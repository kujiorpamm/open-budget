<?php

namespace frontend\controllers;

use common\models\DocumentsTypes;

class DocumentsController extends \yii\web\Controller
{
    public function actionIndex()
    {

        $types = DocumentsTypes::find()->all();
        // echo "<pre>"; print_r($types[0]->documentsByLanguage); echo "</pre>"; exit;

        return $this->render('index', ['types' => $types]);
    }

}
