<?php

namespace frontend\controllers;

use Yii;
use common\models\Reports;
use common\models\search\ReportsSearch;
use common\models\helpers\FilterIncome;
use common\models\helpers\FilterOutcome;
use common\models\helpers\FilterTransferts;
use common\models\ReportTypes;
use common\models\ReportItemCategories;
use common\models\Cities;

class AnalyticsController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionIncome() {

        // Ищем новейший отчет по Мангистауской области и проставляем данные в модель генерирующую графики
        $model = new FilterIncome();
        $city = Cities::findOne(['name_ru' => 'Мангистауская область']);
        // $report = Reports::find()->where(['city_id' => $city->id, 'type_id' => 2])->orderBy('date_fact DESC')->one();
        $model->city = $city->id;
        $model->year = date('Y');
        $model->month = date('m');
        $cities = \common\models\Cities::find()->select('name_ru')->indexBy('id')->column();
        return $this->render('income', [
            'model' => $model,
            'cities' => $cities,
            'years' => $model->getYears()
        ]);
    }

    public function actionIncomePie() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if(isset($_POST['FilterOutcome'])) {
            $model = new FilterOutcome();
            $model->load(Yii::$app->request->post());
        }
        else if(isset($_POST['FilterIncome'])) {
            $model = new FilterIncome();
            $model->load(Yii::$app->request->post());
        } else {
            $model = new FilterTransferts();
            $model->load(Yii::$app->request->post());
        }
        return $model->getPieChart();
    }

    public function actionOutcome() {
        // Ищем новейший отчет по Мангистауской области и проставляем данные в модель генерирующую графики
        $model = new FilterOutcome();
        $city = Cities::findOne(['name_ru' => 'Мангистауская область']);
        // $report = Reports::find()->where(['city_id' => $city->id, 'type_id' => 2])->orderBy('date_fact DESC')->one();
        $model->city = $city->id;
        $model->year = date('Y');
        $model->month = date('m');

        $cities = \common\models\Cities::find()->select('name_ru')->indexBy('id')->column();
        return $this->render('outcome', [
            'model' => $model,
            'cities' => $cities,
            'years' => $model->getYears()
        ]);
    }

    public function actionTransferts() {
        // Ищем новейший отчет по Мангистауской области и проставляем данные в модель генерирующую графики
        $model = new FilterTransferts();
        $city = Cities::findOne(['name_ru' => 'Мангистауская область']);
        $model->city = $city->id;
        $model->year = date('Y');
        $model->month = date('m');

        $cities = \common\models\Cities::find()->select('name_ru')->indexBy('id')->column();
        return $this->render('transferts', [
            'model' => $model,
            'cities' => $cities,
            'years' => $model->getYears()
        ]);
    }

    public function actionDynamicByMonth() {
        return $this->render('dynamic-by-month');
    }

    public function actionGovPrograms() {
        $model = Reports::find()->where('city_id = 2')->andWhere('type_id = 2')->orderBy('date_fact DESC')->one();
        return $this->render('gov-programs', ['model'=>$model]);
    }

    public function actionTest() {
        $model = Reports::find()->one();
        echo "<pre>"; print_r($model->shortTree()); echo "</pre>"; exit;
    }

    public function actionInvest() {
        $this->layout = "main_with_pic";
        return $this->render('invest');
    }

}
