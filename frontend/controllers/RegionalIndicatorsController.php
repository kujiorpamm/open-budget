<?php

namespace frontend\controllers;

use Yii;
use common\models\helpers\RegionalIndicators;
use common\models\Cities;
use common\models\Settings;

class RegionalIndicatorsController extends \yii\web\Controller
{
    public function actionIndex()
    {

        $settings = Settings::findOne(['name' => 'index_cats']);
        $settings = JSON_DECODE($settings->value, true);

        $model = new RegionalIndicators();
        $model->to = date('m');

        $categories = [
            $settings['income_category_id'][0]['id'] => Yii::t('app', 'Доходы'),
            $settings['outcome_category_id'][0]['id'] => Yii::t('app', 'Расходы')
        ];

        $cities = \common\models\Cities::find()->select('name_ru')->indexBy('id')->column();
        return $this->render('index', ['model' => $model,  'categories' => $categories]);
    }

    public function actionReport()
    {
        $last_reports = \common\models\Reports::find()->where('type_id = 1')->orderBy('date DESC')->limit(18)->all();
        return $this->render('report', [
            'last_reports' => $last_reports
        ]);
    }

    public function actionGetData() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $model = new RegionalIndicators();
        $model->load(Yii::$app->request->post());
        return $model->getData();
    }

}
