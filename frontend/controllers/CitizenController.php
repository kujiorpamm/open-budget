<?php

namespace frontend\controllers;

class CitizenController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionChildrens() {
        return $this->render('childrens');
    }

}
