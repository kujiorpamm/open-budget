<?php

namespace frontend\controllers;

class PagesController extends \yii\web\Controller
{

    // public $layout = 'column-2';

    public function actionAboutBudget()
    {
        return $this->render('about-budget');
    }

    public function actionIncome() {
        return $this->render('income');
    }

    public function actionBudgetProcess() {
        return $this->render('budget-process');
    }

    public function actionBudgetSystem() {
        return $this->render('budget-system');
    }

    public function actionInterbudget() {
        return $this->render('interbudget');
    }

    public function actionGlossary() {
        return $this->render('glossary_' . \Yii::$app->language);
    }



}
