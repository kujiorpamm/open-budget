<?php

// yii migrate --migrationPath=@yii/i18n/migrations/
// yii message/extract @backend/config/message_config.php

$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-frontend',
    'name' => 'Budget',
    'basePath' => dirname(__DIR__),
    'homeUrl' => '/',
    'bootstrap' => ['languages'],
    'controllerNamespace' => 'frontend\controllers',
    'sourceLanguage' => 'ru',
    'components' => [
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\DbMessageSource',
                    'messageTable' => '{{%message}}',
                    'sourceMessageTable' => '{{%source_message}}',
                    'on missingTranslation' => ['common\components\TranslationEventHandler', 'handleMissingTranslation']
                ],
                'app' => [
                    'class' => 'yii\i18n\DbMessageSource',
                    'messageTable' => '{{%message}}',
                    'sourceMessageTable' => '{{%source_message}}',
                    'on missingTranslation' => ['common\components\TranslationEventHandler', 'handleMissingTranslation']
                ]
            ],
        ],
        'helper' => [
            'class' => 'common\components\Helper',
        ],
        'request' => [
            'baseUrl' => '',
            'class' => 'common\components\Request',
            'csrfParam' => '_csrf-application_fr',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'class' => 'common\components\UrlManager',
            'baseUrl' => '/',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'languages' => 'languages/default/index', //для модуля мультиязычности
                '/' => 'site/index',
                '<controller>/<action>' => '<controller>/<action>',
                '<_m:[\w\-]+>/<_c:[\w\-]+>/<id:\d+>' => '<_m>/<_c>/view',
                '<_m:[\w\-]+>/<_c:[\w\-]+>/<_a:[\w\-]+>/<id:\d+>' => '<_m>/<_c>/<_a>',
                '<_m:[\w\-]+>' => '<_m>/default/index',
                '<_m:[\w\-]+>/<_c:[\w\-]+>' => '<_m>/<_c>/index'
            ],
        ],
    ],
    'modules' => [
        'languages' => [
            'class' => 'common\modules\languages\Module',
            //Языки используемые в приложении
            'languages' => [
                'Рус' => 'ru',
                'Қаз' => 'kk'
            ],
            'default_language' => 'ru', //основной язык (по-умолчанию)
            'show_default' => false, //true - показывать в URL основной язык, false - нет
        ],
    ],
    'params' => $params,
];
