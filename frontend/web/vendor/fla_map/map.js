(function (lib, img, cjs, ss, an) {

var p; // shortcut to reference prototypes
lib.webFontTxtInst = {}; 
var loadedTypekitCount = 0;
var loadedGoogleCount = 0;
var gFontsUpdateCacheList = [];
var tFontsUpdateCacheList = [];
lib.ssMetadata = [];



lib.updateListCache = function (cacheList) {		
	for(var i = 0; i < cacheList.length; i++) {		
		if(cacheList[i].cacheCanvas)		
			cacheList[i].updateCache();		
	}		
};		

lib.addElementsToCache = function (textInst, cacheList) {		
	var cur = textInst;		
	while(cur != null && cur != exportRoot) {		
		if(cacheList.indexOf(cur) != -1)		
			break;		
		cur = cur.parent;		
	}		
	if(cur != exportRoot) {		
		var cur2 = textInst;		
		var index = cacheList.indexOf(cur);		
		while(cur2 != null && cur2 != cur) {		
			cacheList.splice(index, 0, cur2);		
			cur2 = cur2.parent;		
			index++;		
		}		
	}		
	else {		
		cur = textInst;		
		while(cur != null && cur != exportRoot) {		
			cacheList.push(cur);		
			cur = cur.parent;		
		}		
	}		
};		

lib.gfontAvailable = function(family, totalGoogleCount) {		
	lib.properties.webfonts[family] = true;		
	var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];		
	for(var f = 0; f < txtInst.length; ++f)		
		lib.addElementsToCache(txtInst[f], gFontsUpdateCacheList);		

	loadedGoogleCount++;		
	if(loadedGoogleCount == totalGoogleCount) {		
		lib.updateListCache(gFontsUpdateCacheList);		
	}		
};		

lib.tfontAvailable = function(family, totalTypekitCount) {		
	lib.properties.webfonts[family] = true;		
	var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];		
	for(var f = 0; f < txtInst.length; ++f)		
		lib.addElementsToCache(txtInst[f], tFontsUpdateCacheList);		

	loadedTypekitCount++;		
	if(loadedTypekitCount == totalTypekitCount) {		
		lib.updateListCache(tFontsUpdateCacheList);		
	}		
};
// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.m_7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#373535").ss(1,1,1).p("ABRCHIitgHIgvimICPhgICIBrg");
	this.shape.setTransform(14,13.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#CC0000").s().p("AhcCAIgvimICPhgICIBrIg8Cig");
	this.shape_1.setTransform(14,13.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.m_7, new cjs.Rectangle(-1,-1,30,29), null);


(lib.m_6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#373535").ss(1,1,1).p("ABlCLIi6AHIhBivICTh0ICaBog");
	this.shape.setTransform(15.1,14.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#CC0000").s().p("AiWgdICTh0ICaBoIgyC0Ii6AHg");
	this.shape_1.setTransform(15.1,14.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.m_6, new cjs.Rectangle(-1,-1,32.3,31.2), null);


(lib.m_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FEDC5E").s().p("AjLJ8QgKgWgGgpQgHgqgIgVQgNghgcgSQgngaAEgzQACgcgHgWQgPgqgtgeQgsgegvgBQgwgBg0gEQgngEgggjQgggjAAgnQAAgTgCgeIgDgwQAPgEAIgSQAHgYAGgKQALgUAFgCQAEgBAXAEQATAEAeACIAxAGIATABQALABAHADQAZALAcAAQAXgBAdgIQAzgPAiAnIAdAgQASATAPAKQAPALAWAIQAQAHAZAGQAIACALgDQAJgEAJgFQAHgEAKgLIAPgQQANgNALAAQAMAAAOAKQAFAEATAMIAYgOQAdALASAAQARACARgJQgfgmgrgRQgsgSghAPQgEgSgNgfQgPghgEgOQgHgagTgZQgIgLgbgeIALglQAIgZADgGIAVgzIAVAAIAAgdIAAgVQABgLgCgHQgThXAuhSQAPgaAQgPQATgQAagDQBNgIA3grIAEgDQD0BKCZCUQDUDijUBTQhdApk7hkQiKAMBWBHQDVBGArB4QhIDqjug/QjChXAOBXQABCKCwAAQAFCBASBJQizgmhOCMQgOgagHgRg");
	this.shape.setTransform(67.5,68);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.m_5, new cjs.Rectangle(0,0,135,135.9), null);


(lib.m_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FEDC5E").s().p("AlKEbQgMgLAEgWQADgQANgtQAQg3AAguQAAg1gWgtIgPghQgJgSgLgGQgVgLgPgPQgOgPgKgVIgYguIgBgEQBOiMC0AlQAhCHBSgxQCOhbDVAgQBMAXBOAPQA/Ebi+gxQiQhMi+CaQhPBGgIBfIgvAGIiHAYIgKABQgPAAgJgIg");
	this.shape.setTransform(44.9,29.1);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.m_4, new cjs.Rectangle(0,0,89.9,58.2), null);


(lib.m_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FEDC5E").s().p("AP0SrIgUAQQgpgLgfgnQgKgMgRgQIgcgZQgsgpgkhEIgtheQgcg3gbghIgDgGIgDgFQgRgMgGgUQgGgQADgXQACgPgGgQQgDgKgLgSQgYgogwgmIhVg9Qgrgfg+gpIhqhHQgqgcgwABQg9ABhMAKQgoAFheAPQhVANhkAmQguARiCA5QgdAMgjAdIg7A0IhdBRQg7AygkAdQgRAOgHAIQgNAMgEAPQAAgGAMgeIAOgkQAIggAAgQQAAgbgQgUQgMgRgEgPQgJgjgDgTQgDgfAPgXIAKgSQAGgKAGgGQAggiAZg0QAMgbAZhGQALgcAEgcIAKhKIARANQATgJADgTQABgHgBgbQgBgYgNgPQgNgOgZgJQgKgDgJgHQgKgJgBgHQgCgLgIgHQgEgDgMgFQgxgRgkABQguACglAiIgLgNIgIgMQgagzhAAKQgYAEgiAJIg8AQQgagSgUgwIgXgzIgYgxQgPgIgIgLQgIgMAGgOQAHgRgGgOQgDgKgNgOQAIheBPhHQC/iaCQBNQC+Awg+kbQDYApDpgWIEbhfQDQg8DRA8QBMhnhMguIj8hiQg9geCUhjQG5gqg3B8QgmCuAmDVQAFD1ECgYQDohGBoBQIAEaCQgqgEgTACQghAEgQAbQghgQgiAJQgRAEgqAcg");
	this.shape.setTransform(126.2,121.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.m_3, new cjs.Rectangle(0,0,252.4,243.1), null);


(lib.m_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FEDC5E").s().p("AQnMVQkDAXgDj1QgnjVAnitQA2h8m4AqQiVBiA9AeID8BiQBMAuhMBnQjRg8jQA8IkbBfQjpAXjZgqQhOgPhMgXQjUggiQBbQhRAxgiiHQgThJgEiAQixAAgBiKQgOhXDDBXQDuA+BIjoQgsh5jUhGQhWhICJgLQE8BjBdgoQDUhUjUjhQiZiUj0hKQAqggAzAFIAqADIAoADQAiAEAVAKQAVAJATACQATACAVgFQAVgFAagDIAwgDQAHgBARAEQAQADAIgCQASgHASAFQANADARANQATAPAQAIQAVAJAVADIAdACQASADAKAGQAXAOAgAEIA5ADIAdACQDlHhXACXIADNcQhohRjoBHg");
	this.shape.setTransform(139.9,79.9);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.m_2, new cjs.Rectangle(0,0,279.8,159.8), null);


(lib.m_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FEDC5E").s().p("AwdDDIANABIArAGQAKABAYgGIAVgHIAQgEQAHAIAKACQAKACAQgGQANgEAUgDIAggFQAPgBAdgHQAYgEAOAFIApAOQAXAGAQgBIArAcQALgKgBgIQgBgHgJgJIgOgNQgIgJgFgEQgNgJgVgJQgEgCgLgCQgKgCgEgDQgtgbg6ATQgxARgOgBQgQgBgogXQgIgFgJgLIgQgTIgtgQQgbgKgMgWIABgIQAAgFgBgEIgSg5QgJgdAGgSQAFgSBSgZQgUgPgcAGQggALgRAEIgNgRIAugrIgFgSIgFgPQgLgaAJgzQAJg0AVgNQAOgJAKgNQAIgLAJgSIBPAAIBIgCQAqAAAdADQAsAGApgTQAegOAngjIAZgWQAPgNAJgKQAQgRAIgPQAJgTAAgUQgBgEADgFIAFgIIAvAGIATgFQALgCAIAAQArAAAZgDQAlgFAegNQAhgOA7gHQAkgEArgNQATgFA6gUQAUgHAagEIAtgGQA2gFA0AAQAOAAAXAEIAnAFICHh8IA4A+QAVAWAWAHQAYAHAdgJQAMgEALABQANAAAGAHIAQBJIAGgBIgUAwIAfATIgDAHIAygWIALApIAGgGIAWAGQAMADAIAFQAIAFAJAJIAPARQAPAPARADQAQADAVgHQAKgDAPABIAZABIA4AIQAcAEAPAQQAMAOAFAbQAKA3AYAmQAKAPAEAdQACAPABAaIACApIAQCPIAVCeIAbCxIAEAJIAGANIgOALQgIAGgHACImPCFQgDAEgIABIABGOQ3AiYjmngg");
	this.shape.setTransform(108.7,82.7);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.m_1, new cjs.Rectangle(0,0,217.5,165.4), null);


// stage content:
(lib.map = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		window.current_m = null;
		
		var objects = [
			this.m_1, this.m_2, this.m_3, this.m_4, this.m_5, this.m_6, this.m_7
		]
		
		var names = [
			"Бейнеуский район", "Мангистауский район", 
			"Тупкараганский район", "Мунайлинский район" , 
			"Каракиянский район", "Актау", "Жанаозен"
		]
		
		var infos = [
		    'Информация на: 01.07.2018 года<br/> Факт исполнения поступлений 5 046,7 <br/>Факт исполнения расходов 4 986,9 <br/><span>Подробнее</span><br/>Численность населения: 68 281человек<br/>Официальный сайт<br/><a href="http://beineu.mangystau.gov.kz/ru">http://beineu.mangystau.gov.kz/ru</a>',
		    'Информация на: 01.07.2018 года<br/>Факт исполнения поступлений 4 180,8<br/>Факт исполнения расходов 3 963,9<br/><span>Подробнее</span><br/>Численность населения: 38 531 человек<br/>Официальный сайт<br/><a href="http://audan.mangystau.gov.kz/ru">http://audan.mangystau.gov.kz/ru</a>',
		    'Информация на: 01.07.2018 года<br/>Факт исполнения поступлений 3 212,2<br/>Факт исполнения расходов 3 149,1<br/><span>Подробнее</span><br/>Численность населения: 29 327 человек<br/>Официальный сайт<br/><a href="http://tupkaragan.mangystau.gov.kz/">http://tupkaragan.mangystau.gov.kz/</a>',
		    'Информация на: 01.07.2018 года<br/>Факт исполнения поступлений 9 342,2<br/>Факт исполнения расходов 9 013,8<br/><span>Подробнее</span><br/>Численность населения: 152 826 человек<br/>Официальный сайт<br/><a href="http://munaily.mangystau.gov.kz/ru/">http://munaily.mangystau.gov.kz/ru/</a>',
		    'Информация на: 01.07.2018 года<br/>Факт исполнения поступлений 4 597,3<br/>Факт исполнения расходов 4 616,4<br/><span>Подробнее</span><br/>Численность населения: 37 204 человек<br/>Официальный сайт<br/><a href="http://karakia.mangystau.gov.kz/ru">http://karakia.mangystau.gov.kz/ru</a>',
		    'Информация на: 01.07.2018 года<br/>Факт исполнения поступлений 13 891,4<br/>Факт исполнения расходов 13 546,6<br/><span>Подробнее</span><br/>Численность населения: 183 187 человек<br/>Официальный сайт<br/><a href="https://aktau.gov.kz/kz/">https://aktau.gov.kz/kz/</a>',
		    'Информация на: 01.07.2018 года<br/>Факт исполнения поступлений 7 982,8<br/>Факт исполнения расходов 7 909,6<br/><span>Подробнее</span><br/>Численность населения: 113 014 человек<br/>Официальный сайт<br/><a href="http://zhanaozen.gov.kz/ru">http://zhanaozen.gov.kz/ru</a>'
		]
		
		for (var i = 0; i < objects.length; i++) {
			objects[i].addEventListener('click', clicker);
		}
		
		objects[1].alpha = 0.7;
		document.getElementById("target_name").innerHTML = names[1];
		document.getElementById("target_descr").innerHTML = infos[1];
		if(typeof createTable == 'function') createTable(1);
		
		function clicker(event) {
			
			// console.log(event.currentTarget);
			
			for (var i = 0; i < objects.length; i++) {
				objects[i].alpha = 1;
				if(objects[i] == event.currentTarget) {
					
					if(typeof createTable == 'function') createTable(i);
					
					document.getElementById("target_name").innerHTML = names[i];
					document.getElementById("target_descr").innerHTML = infos[i];
					// console.log(names[i]);
					
				}
				// console.log(objects[i]);
			}
			tgt = event.currentTarget;
			tgt.alpha = 0.7;
			current_m = tgt;
			// console.log(tgt.id);
			
		}
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// names
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#666666").s().p("AAUAyIAAgsIgnAAIAAAsIgUAAIAAhjIAUAAIAAAnIAnAAIAAgnIAUAAIAABjg");
	this.shape.setTransform(273.7,276.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#666666").s().p("AglAyIAAhjIBJAAIAAAQIg1AAIAAAXIAxAAIAAAQIgxAAIAAAbIA3AAIAAARg");
	this.shape_1.setTransform(264.1,276.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#666666").s().p("AgYAvQgJgFgFgPIATgGQADAJAEADQAFACAHAAQAJAAAEgEQAEgEAAgFQABgGgFgEQgFgEgJAAIgFAAIAAgOIADAAQAIAAAFgEQAEgEAAgHQAAgGgDgDQgFgEgGAAQgNAAgDAMIgUgEQAHgZAdAAQARAAAJAIQAJAIAAAKQAAAHgEAGQgEAGgIAEQAJADAFAGQAGAGAAAJQAAAOgKAIQgJAIgUAAQgOAAgKgFg");
	this.shape_2.setTransform(254.7,276);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#666666").s().p("AgiAmQgOgNAAgYQAAgPAFgKQADgIAGgGQAGgGAHgDQAKgEALAAQAWAAANAOQAOAOAAAXQAAAZgOANQgNAOgWAAQgVAAgNgOgAgTgZQgIAJAAAQQAAARAIAJQAIAJALAAQAMAAAIgJQAIgJAAgRQAAgQgIgJQgHgIgNAAQgMAAgHAIg");
	this.shape_3.setTransform(245.1,276);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#666666").s().p("AAcAyIgIgXIgoAAIgIAXIgVAAIAnhjIAUAAIAoBjgAgNALIAaAAIgNglg");
	this.shape_4.setTransform(234.8,276.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#666666").s().p("AATAyIAAgsIgmAAIAAAsIgUAAIAAhjIAUAAIAAAnIAmAAIAAgnIAVAAIAABjg");
	this.shape_5.setTransform(224.7,276.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#666666").s().p("AAcAyIgIgXIgoAAIgIAXIgVAAIAnhjIAUAAIAoBjgAgNALIAaAAIgNglg");
	this.shape_6.setTransform(214.6,276.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#666666").s().p("AAmAzIgMgaIgBgCIgDgFQgEgIgCgCQgCgBgEAAIAAAsIgTAAIAAgsQgDAAgDABQgCACgEAIIgDAFIgBACIgMAaIgYAAIAOgcIAKgQQAEgGAHgBQgGgDgCgEIgEgJIgFgMQgCgEgDgBQgCgBgHgBIgDAAIAAgOIADAAQAPgBAGAFQAGAFAGAPQAFAMADADQACADAFABIAAgqIATAAIAAAqQAFAAADgEQACgDAFgMQAGgQAGgEQAGgFAPABIADAAIAAAOIgDAAQgGABgDABQgDACgCADIgFAMIgEAJQgCAEgGADQAHABAEAGIAKAQIAOAcg");
	this.shape_7.setTransform(203.2,276);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#666666").s().p("AgeAxIAAgPIAIABQAIAAACgDQAEgCADgJIglhGIAWAAIAXAzIATgzIAVAAIghBKQgGANgFAGQgGAHgIgBIgPgBg");
	this.shape_8.setTransform(118.4,254.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#666666").s().p("AAcAyIgIgXIgoAAIgIAXIgVAAIAnhjIAUAAIAoBjgAgNAKIAaAAIgNgkg");
	this.shape_9.setTransform(109.5,254.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#666666").s().p("AgJAyIAAhTIgeAAIAAgQIBPAAIAAAQIgeAAIAABTg");
	this.shape_10.setTransform(101.1,254.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#666666").s().p("AANAyIgMgaIgBgBIgBgFQgEgIgDgCQgCgCgFAAIAAAsIgVAAIAAhiIAVAAIAAAqQAGgBADgDQACgCAEgNQAGgQAHgFQAGgDAOAAIADAAIAAAOIgDAAQgGAAgDACIgFAEIgFAOIgEAJQgCADgGADQAHABAFAFQAEAHAFAKIAOAbg");
	this.shape_11.setTransform(92.6,254.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#666666").s().p("AAcAyIgIgXIgoAAIgIAXIgVAAIAnhjIAUAAIAoBjgAgNAKIAaAAIgNgkg");
	this.shape_12.setTransform(82.8,254.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#666666").s().p("AANAkIAAgeIgaAAIAAAeIgTAAIAAhHIATAAIAAAaIAaAAIAAgaIAUAAIAABHg");
	this.shape_13.setTransform(108.5,295);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#666666").s().p("AgSAhQgJgEgFgJQgEgJgBgLQABgKAEgJQAFgJAIgEQAJgFAKAAQAQAAALALQALAKAAAQQAAAQgLALQgLALgQAAQgJAAgJgFgAgMgQQgEAGAAAKQAAALAEAFQAGAGAGAAQAIAAAFgGQAFgFAAgLQAAgKgFgGQgFgFgIAAQgGAAgGAFg");
	this.shape_14.setTransform(100,295);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#666666").s().p("AAPAzIAAguIgdAuIgTAAIAAhHIATAAIAAAsIAdgsIATAAIAABHgAgNgkQgGgFAAgJIAJAAQAAAFADACQADADAEAAQAGAAADgDQADgCAAgFIAKAAQgBAJgFAFQgHAGgJAAQgIAAgFgGg");
	this.shape_15.setTransform(91.4,293.6);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#666666").s().p("AgaAgQgHgGAAgKQAAgGADgFQADgEAFgCQAFgCAKgCQANgDAFgCIAAgCQAAgGgCgCQgDgCgHAAQgFAAgDACQgDACgCAFIgRgDQADgLAHgFQAIgFANAAQANAAAFADQAHADACAFQADAEAAANIAAAVIAAAPIAEAJIgTAAIgCgFIgBgDQgFAFgFACQgFADgGAAQgLAAgGgGgAAAAEIgKAEQgEACAAAEQAAAEADADQADADAFAAQAEAAAFgDQADgDABgDIABgKIAAgEIgLADg");
	this.shape_16.setTransform(83.2,295);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#666666").s().p("AgjAzIAAhjIASAAIAAAKQAEgFAGgEQAGgDAGAAQAMAAAJAKQAKAKgBASQABARgKAKQgJAKgMAAQgFAAgFgDQgFgCgGgGIAAAlgAgLgdQgFAFAAALQABAMAFAFQAEAGAGAAQAHAAAEgGQAFgEAAgMQAAgMgFgFQgEgGgHAAQgGAAgFAGg");
	this.shape_17.setTransform(75.3,296.3);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#666666").s().p("AAPAzIAAgvIgdAvIgTAAIAAhIIATAAIAAAtIAdgtIATAAIAABIgAgNgjQgGgGAAgJIAJAAQAAAFADADQADACAEAAQAGAAADgCQADgDAAgFIAKAAQgBAJgFAGQgGAFgKAAQgIAAgFgFg");
	this.shape_18.setTransform(138.6,276);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#666666").s().p("AAPAkIAAgtIgdAtIgSAAIAAhHIASAAIAAAsIAcgsIATAAIAABHg");
	this.shape_19.setTransform(130,277.4);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#666666").s().p("AAKAlIgKgWQgCgGgCgBQgCgDgEAAIAAAgIgTAAIAAhIIATAAIAAAeQAEgBACgCIAEgLQAEgLAEgDQAFgCAHAAIAHAAIAAAMQgHABgCABQgCABgDAIQgDAKgGACQAJACAGANIABACIAKAUg");
	this.shape_20.setTransform(122.6,277.4);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#666666").s().p("AgYAcQgJgKAAgSQAAgRAJgKQAKgKAPAAQANAAAIAGQAIAFADAMIgTAEQgBgGgDgDQgEgDgFAAQgGAAgEAFQgFAFAAAMQAAAMAFAFQAEAFAGAAQAGAAADgDQAEgDABgIIATAEQgDANgIAGQgJAHgOAAQgPAAgJgKg");
	this.shape_21.setTransform(114.9,277.4);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#666666").s().p("AANAkIAAgeIgaAAIAAAeIgTAAIAAhHIATAAIAAAaIAaAAIAAgaIAUAAIAABHg");
	this.shape_22.setTransform(106.7,277.4);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#666666").s().p("AAPAkIAAgtIgdAtIgSAAIAAhHIASAAIAAAsIAcgsIATAAIAABHg");
	this.shape_23.setTransform(98.1,277.4);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#666666").s().p("AgmAkIAAgOIAEAAIAEAAQAEAAABgDIABgOIAAgpIA/AAIAABIIgTAAIAAg4IgaAAIAAAfQABANgDAFQgDAFgEABQgDACgHAAIgNgBg");
	this.shape_24.setTransform(89,277.5);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#666666").s().p("AAPAzIAAgvIgdAvIgTAAIAAhIIATAAIAAAtIAdgtIATAAIAABIgAgNgjQgGgGAAgJIAJAAQAAAFADADQADACAEAAQAGAAADgCQADgDAAgFIAKAAQgBAJgFAGQgHAFgJAAQgIAAgFgFg");
	this.shape_25.setTransform(80.6,276);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#666666").s().p("AgaAgQgHgGAAgKQAAgGADgFQADgEAFgCQAFgCAKgCQANgDAFgCIAAgCQAAgGgCgCQgDgCgHAAQgFAAgDACQgDACgCAFIgRgDQADgLAHgFQAIgFANAAQANAAAFADQAHADADAFQACAEAAANIAAAVIAAAPIAEAJIgTAAIgCgFIgBgDQgFAFgFACQgFADgGAAQgLAAgGgGgAAAAEIgKAEQgEACAAAEQAAAEADADQADADAFAAQAEAAAFgDQADgDABgDIABgKIAAgEIgLADg");
	this.shape_26.setTransform(72.4,277.4);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#666666").s().p("AANAkIAAgeIgaAAIAAAeIgTAAIAAhHIATAAIAAAaIAaAAIAAgaIAUAAIAABHg");
	this.shape_27.setTransform(64.3,277.4);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#666666").s().p("AgeAyIgCgPIAIAAQAHABADgEQAEgEABgGIgbhIIAUAAIARAyIAQgyIAUAAIgZBFIgFANQgCAGgDADQgCADgCADIgHADIgKABIgLgBg");
	this.shape_28.setTransform(56.1,278.9);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#666666").s().p("AAeAyIAAhOIgUBOIgTAAIgUhOIAABOIgSAAIAAhjIAeAAIARBDIAShDIAeAAIAABjg");
	this.shape_29.setTransform(46.4,276.1);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#666666").s().p("AANAkIAAgeIgaAAIAAAeIgTAAIAAhHIATAAIAAAaIAaAAIAAgaIAUAAIAABHg");
	this.shape_30.setTransform(109.1,161.5);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#666666").s().p("AgSAhQgJgEgFgJQgEgJAAgLQAAgKAEgJQAFgJAJgEQAJgFAJAAQARAAAKALQAKAKABAQQgBAQgKALQgKALgRAAQgJAAgJgFgAgMgQQgEAGAAAKQAAALAEAFQAGAGAGAAQAIAAAEgGQAGgFAAgLQAAgKgGgGQgEgFgIAAQgGAAgGAFg");
	this.shape_31.setTransform(100.7,161.5);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#666666").s().p("AAPAzIAAgvIgdAvIgSAAIAAhIIASAAIAAAtIAdgtIATAAIAABIgAgNgjQgGgGgBgJIAKAAQAAAFADACQADADAEAAQAGAAADgDQADgCABgFIAJAAQgBAJgGAGQgFAFgKAAQgHAAgGgFg");
	this.shape_32.setTransform(92.1,160.1);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#666666").s().p("AgbAgQgGgGAAgKQAAgGADgFQADgEAFgCQAGgCAJgCQANgDAFgCIAAgCQAAgGgDgCQgCgCgHAAQgFAAgDACQgDACgBAFIgSgDQADgLAHgFQAIgFANAAQANAAAFADQAHADACAFQADAEAAANIAAAVIABAPIADAJIgTAAIgBgFIgBgDQgGAFgFACQgFADgGAAQgLAAgHgGgAAAAEIgKAEQgEACAAAEQAAAEADADQADADAFAAQAEAAAFgDQAEgDAAgDIABgKIAAgEIgLADg");
	this.shape_33.setTransform(83.9,161.5);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#666666").s().p("AgjAzIAAhjIASAAIAAAKQAEgFAGgEQAGgDAGAAQANAAAIAKQAJAKAAASQAAARgJAKQgIAKgNAAQgFAAgFgDQgFgCgFgGIAAAlgAgLgdQgFAFABALQAAAMAEAFQAFAGAGAAQAHAAAFgGQAEgEAAgMQAAgMgFgFQgFgGgGAAQgGAAgFAGg");
	this.shape_34.setTransform(76,162.8);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#666666").s().p("AAPAzIAAguIgdAuIgTAAIAAhHIATAAIAAAsIAdgsIATAAIAABHgAgNgkQgGgFAAgJIAJAAQAAAFADACQADADAEAAQAGAAADgDQADgCABgFIAJAAQgBAJgFAFQgHAGgJAAQgHAAgGgGg");
	this.shape_35.setTransform(142.8,142.5);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#666666").s().p("AAPAkIAAgtIgdAtIgSAAIAAhHIASAAIAAAsIAcgsIATAAIAABHg");
	this.shape_36.setTransform(134.2,143.9);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#666666").s().p("AAKAkIgKgVQgCgGgCgCQgCgBgEgBIAAAfIgTAAIAAhHIATAAIAAAeQAEgBACgDIAEgKQAEgLAEgCQAFgEAHAAIAHABIAAANQgHgBgCACQgCACgDAHQgDAKgGACQAJACAGANIABABIAKAUg");
	this.shape_37.setTransform(126.9,143.9);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#666666").s().p("AgYAcQgJgKAAgSQAAgRAJgKQAKgKAPAAQANAAAIAGQAIAFADAMIgTAEQgBgGgDgDQgEgDgFAAQgGAAgEAFQgFAFAAAMQAAAMAFAFQAEAFAGAAQAGAAADgDQAEgDABgIIATAEQgDANgIAGQgJAHgOAAQgPAAgJgKg");
	this.shape_38.setTransform(119.1,143.9);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#666666").s().p("AANAkIAAgeIgaAAIAAAeIgTAAIAAhHIATAAIAAAaIAaAAIAAgaIAUAAIAABHg");
	this.shape_39.setTransform(110.9,143.9);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#666666").s().p("AgbAgQgGgGAAgKQAAgGADgFQADgEAFgCQAGgCAJgCQANgDAFgCIAAgCQAAgGgDgCQgCgCgHAAQgFAAgDACQgDACgBAFIgSgDQADgLAHgFQAIgFANAAQANAAAFADQAHADACAFQADAEAAANIAAAVIABAPIADAJIgTAAIgBgFIgBgDQgGAFgFACQgFADgGAAQgLAAgHgGgAAAAEIgKAEQgEACAAAEQAAAEADADQADADAFAAQAEAAAFgDQAEgDAAgDIABgKIAAgEIgLADg");
	this.shape_40.setTransform(102.8,143.9);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#666666").s().p("AgXAkIAAhHIAvAAIAAAPIgdAAIAAA4g");
	this.shape_41.setTransform(96.4,143.9);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#666666").s().p("AgbAgQgGgGAAgKQAAgGADgFQADgEAFgCQAGgCAKgCQAMgDAFgCIAAgCQAAgGgDgCQgDgCgGAAQgFAAgDACQgDACgBAFIgSgDQADgLAHgFQAHgFAOAAQAMAAAHADQAGADADAFQACAEAAANIAAAVIABAPIADAJIgTAAIgCgFIAAgDQgFAFgGACQgFADgGAAQgLAAgHgGgAAAAEIgKAEQgEACAAAEQAAAEADADQADADAFAAQAEAAAFgDQAEgDABgDIAAgKIAAgEIgLADg");
	this.shape_42.setTransform(89.1,143.9);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#666666").s().p("AgiAzIAAhjIASAAIAAAKQADgFAGgEQAGgDAGAAQANAAAJAKQAIAKABASQgBARgIAKQgJAKgNAAQgFAAgFgDQgFgCgFgGIAAAlgAgLgdQgEAFAAALQAAAMAEAFQAFAGAGAAQAHAAAEgGQAFgEAAgMQAAgMgFgFQgEgGgHAAQgGAAgFAGg");
	this.shape_43.setTransform(81.2,145.2);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#666666").s().p("AgbAgQgGgGAAgKQAAgGADgFQADgEAFgCQAGgCAKgCQAMgDAFgCIAAgCQAAgGgDgCQgDgCgGAAQgFAAgDACQgDACgBAFIgSgDQADgLAHgFQAHgFAOAAQAMAAAGADQAHADACAFQADAEAAANIAAAVIABAPIADAJIgTAAIgBgFIgBgDQgFAFgGACQgFADgGAAQgLAAgHgGgAAAAEIgKAEQgEACAAAEQAAAEADADQADADAFAAQAEAAAFgDQADgDACgDIAAgKIAAgEIgLADg");
	this.shape_44.setTransform(72.8,143.9);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#666666").s().p("AAKAkIgKgVQgCgGgCgCQgCgBgEgBIAAAfIgTAAIAAhHIATAAIAAAeQAEgBACgDIAEgKQAEgLAEgCQAFgEAHAAIAHABIAAANQgHgBgCACQgCACgDAHQgDAKgGACQAJACAGANIABABIAKAUg");
	this.shape_45.setTransform(65.9,143.9);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#666666").s().p("AANAkIAAg4IgZAAIAAA4IgTAAIAAhHIA/AAIAABHg");
	this.shape_46.setTransform(57.6,143.9);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#666666").s().p("AgeAyIgCgPIAIAAQAHAAADgDQAEgEABgHIgbhHIAUAAIARAyIAQgyIAUAAIgZBFIgFANQgCAGgDADQgCADgCACIgHAEIgKABIgLgBg");
	this.shape_47.setTransform(49.5,145.4);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#666666").s().p("AgJAyIAAhSIgeAAIAAgRIBPAAIAAARIgeAAIAABSg");
	this.shape_48.setTransform(41.9,142.6);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#666666").s().p("AANAkIAAgeIgaAAIAAAeIgTAAIAAhHIATAAIAAAaIAaAAIAAgaIAUAAIAABHg");
	this.shape_49.setTransform(325,307.8);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#666666").s().p("AgSAhQgJgEgFgJQgEgJAAgLQAAgKAEgJQAFgJAJgEQAIgFAKAAQAQAAALALQALAKAAAQQAAAQgLALQgLALgQAAQgJAAgJgFgAgMgQQgEAGAAAKQAAALAEAFQAGAGAGAAQAIAAAEgGQAGgFAAgLQAAgKgGgGQgEgFgIAAQgGAAgGAFg");
	this.shape_50.setTransform(316.6,307.8);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#666666").s().p("AAPAzIAAgvIgdAvIgTAAIAAhIIATAAIAAAtIAdgtIATAAIAABIgAgNgjQgGgGAAgJIAJAAQAAAFADACQADADAEAAQAGAAADgDQADgCABgFIAJAAQgBAJgFAGQgHAFgJAAQgHAAgGgFg");
	this.shape_51.setTransform(308,306.3);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#666666").s().p("AgbAgQgGgGAAgKQAAgGADgFQADgEAFgCQAFgCAKgCQANgDAFgCIAAgCQAAgGgCgCQgDgCgHAAQgFAAgDACQgDACgCAFIgRgDQADgLAHgFQAIgFANAAQANAAAFADQAHADACAFQADAEAAANIAAAVIAAAPIAEAJIgTAAIgBgFIgCgDQgFAFgFACQgFADgGAAQgLAAgHgGgAAAAEIgKAEQgEACAAAEQAAAEADADQADADAFAAQAEAAAFgDQADgDABgDIABgKIAAgEIgLADg");
	this.shape_52.setTransform(299.8,307.8);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#666666").s().p("AgjAzIAAhjIASAAIAAAKQAEgFAGgEQAGgDAGAAQANAAAIAKQAKAKgBASQABARgKAKQgIAKgNAAQgFAAgFgDQgFgCgGgGIAAAlgAgLgdQgFAFAAALQABAMAEAFQAFAGAGAAQAHAAAFgGQAEgEAAgMQAAgMgFgFQgFgGgGAAQgGAAgFAGg");
	this.shape_53.setTransform(291.9,309.1);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#666666").s().p("AAPAzIAAgvIgdAvIgTAAIAAhIIATAAIAAAtIAdgtIATAAIAABIgAgNgjQgGgGAAgJIAJAAQAAAFADACQADADAEAAQAGAAADgDQADgCABgFIAJAAQgBAJgFAGQgHAFgJAAQgHAAgGgFg");
	this.shape_54.setTransform(279.2,306.3);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#666666").s().p("AAPAkIAAgtIgdAtIgSAAIAAhHIASAAIAAAsIAcgsIATAAIAABHg");
	this.shape_55.setTransform(270.6,307.8);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#666666").s().p("AAKAkIgKgUQgCgHgCgBQgCgCgEAAIAAAeIgTAAIAAhHIATAAIAAAdQAEAAACgCIAEgLQAEgLAEgDQAFgCAHAAIAHAAIAAAMQgHAAgCACQgCABgDAIQgDAKgGACQAJACAGANIABACIAKATg");
	this.shape_56.setTransform(263.2,307.8);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#666666").s().p("AgYAcQgJgKAAgSQAAgRAJgKQAKgKAPAAQANAAAIAGQAIAFADAMIgTAEQgBgGgDgDQgEgDgFAAQgGAAgEAFQgFAFAAAMQAAAMAFAFQAEAFAGAAQAGAAADgDQAEgDABgIIATAEQgDANgIAGQgJAHgOAAQgPAAgJgKg");
	this.shape_57.setTransform(255.5,307.8);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#666666").s().p("AANAkIAAgeIgaAAIAAAeIgTAAIAAhHIATAAIAAAaIAaAAIAAgaIAUAAIAABHg");
	this.shape_58.setTransform(247.3,307.8);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#666666").s().p("AARAkIAAgeIgBAAQgHAAgCACQgEABgDAGIAAABIgNAUIgWAAIANgVQAHgJAEgBQgIgBgFgEQgFgGAAgJQAAgHAEgFQACgFAGgCQAGgBALAAIAkAAIAABHgAgGgVQgEACABAFQgBAFAEADQAEACAGAAIANAAIAAgTIgOAAQgGAAgDACg");
	this.shape_59.setTransform(238.5,307.8);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#666666").s().p("AAPAkIAAgtIgdAtIgTAAIAAhHIATAAIAAAsIAdgsIATAAIAABHg");
	this.shape_60.setTransform(230.6,307.8);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#666666").s().p("AAKAkIgKgUQgCgHgCgBQgCgCgEAAIAAAeIgTAAIAAhHIATAAIAAAdQAEAAACgCIAEgLQAEgLAEgDQAFgCAHAAIAHAAIAAAMQgHAAgCACQgCABgDAIQgDAKgGACQAJACAGANIABACIAKATg");
	this.shape_61.setTransform(223.2,307.8);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#666666").s().p("AgbAgQgGgGAAgKQAAgGADgFQADgEAFgCQAGgCAJgCQANgDAFgCIAAgCQAAgGgDgCQgDgCgGAAQgFAAgDACQgDACgBAFIgSgDQADgLAHgFQAIgFANAAQANAAAFADQAHADACAFQADAEAAANIAAAVIABAPIADAJIgTAAIgBgFIgBgDQgGAFgFACQgEADgHAAQgLAAgHgGgAAAAEIgKAEQgEACAAAEQAAAEADADQADADAFAAQAEAAAFgDQAEgDAAgDIABgKIAAgEIgLADg");
	this.shape_62.setTransform(215.4,307.8);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#666666").s().p("AgiAzIAAhjIASAAIAAAKQADgFAGgEQAGgDAGAAQAMAAAJAKQAJAKAAASQAAARgJAKQgJAKgMAAQgFAAgFgDQgFgCgFgGIAAAlgAgLgdQgFAFABALQAAAMAEAFQAFAGAGAAQAHAAAFgGQAEgEAAgMQAAgMgFgFQgFgGgGAAQgGAAgFAGg");
	this.shape_63.setTransform(207.5,309.1);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#666666").s().p("AgbAgQgGgGAAgKQAAgGADgFQADgEAFgCQAGgCAJgCQANgDAFgCIAAgCQAAgGgDgCQgCgCgHAAQgFAAgDACQgDACgBAFIgSgDQADgLAHgFQAIgFANAAQANAAAFADQAHADACAFQADAEAAANIAAAVIABAPIADAJIgTAAIgBgFIgBgDQgGAFgFACQgFADgGAAQgLAAgHgGgAAAAEIgKAEQgEACAAAEQAAAEADADQADADAFAAQAEAAAFgDQAEgDAAgDIABgKIAAgEIgLADg");
	this.shape_64.setTransform(199.1,307.8);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#666666").s().p("AANAyIgMgaIgBgBIgBgFQgEgIgDgCQgCgCgFAAIAAAsIgVAAIAAhiIAVAAIAAAqQAGgBADgDQACgCAEgNQAGgQAHgFQAGgDAOAAIADAAIAAAOIgDAAQgGAAgDACIgFAEIgFAOIgEAJQgCADgGADQAHABAFAFQAEAHAFAKIAOAbg");
	this.shape_65.setTransform(191.2,306.4);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#666666").s().p("AANAkIAAgeIgaAAIAAAeIgTAAIAAhHIATAAIAAAaIAaAAIAAgaIAUAAIAABHg");
	this.shape_66.setTransform(296.6,199);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#666666").s().p("AgSAhQgJgEgEgJQgFgJgBgLQABgKAFgJQAEgJAIgEQAJgFAKAAQAQAAALALQALAKAAAQQAAAQgLALQgLALgQAAQgJAAgJgFgAgMgQQgEAGAAAKQAAALAEAFQAGAGAGAAQAIAAAFgGQAFgFAAgLQAAgKgFgGQgFgFgIAAQgGAAgGAFg");
	this.shape_67.setTransform(288.1,199);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#666666").s().p("AAPAzIAAgvIgdAvIgTAAIAAhHIATAAIAAAsIAdgsIATAAIAABHgAgNgjQgGgGAAgJIAJAAQAAAFADACQADADAEAAQAGAAADgDQADgCAAgFIAKAAQgBAJgFAGQgHAFgJAAQgIAAgFgFg");
	this.shape_68.setTransform(279.5,197.5);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#666666").s().p("AgaAgQgHgGAAgKQAAgGADgFQADgEAFgCQAFgCAKgCQANgDAFgCIAAgCQAAgGgCgCQgDgCgHAAQgFAAgDACQgDACgCAFIgRgDQADgLAHgFQAIgFANAAQANAAAFADQAHADACAFQADAEAAANIAAAVIAAAPIAEAJIgTAAIgCgFIgBgDQgFAFgFACQgFADgGAAQgLAAgGgGgAAAAEIgKAEQgEACAAAEQAAAEADADQADADAFAAQAEAAAFgDQADgDABgDIABgKIAAgEIgLADg");
	this.shape_69.setTransform(271.3,199);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#666666").s().p("AgjAzIAAhjIASAAIAAAKQAEgFAGgEQAGgDAGAAQAMAAAJAKQAKAKgBASQABARgKAKQgJAKgMAAQgFAAgFgDQgFgCgGgGIAAAlgAgLgdQgFAFAAALQABAMAFAFQAEAGAGAAQAHAAAEgGQAFgEAAgMQAAgMgFgFQgEgGgHAAQgGAAgFAGg");
	this.shape_70.setTransform(263.4,200.3);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("#666666").s().p("AAPAzIAAgvIgdAvIgTAAIAAhHIATAAIAAAsIAdgsIATAAIAABHgAgNgjQgGgGAAgJIAJAAQAAAFADACQADADAEAAQAGAAADgDQADgCAAgFIAKAAQgBAJgFAGQgHAFgJAAQgIAAgFgFg");
	this.shape_71.setTransform(250.7,197.5);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f("#666666").s().p("AAPAkIAAgtIgdAtIgSAAIAAhHIASAAIAAAsIAcgsIATAAIAABHg");
	this.shape_72.setTransform(242.1,199);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f("#666666").s().p("AAKAkIgKgUQgCgHgCgBQgCgCgEAAIAAAeIgTAAIAAhHIATAAIAAAdQAEAAACgCIAEgLQAEgKAEgEQAFgCAHAAIAHAAIAAAMQgHAAgCACQgCACgDAHQgDAKgGACQAJACAGANIABACIAKATg");
	this.shape_73.setTransform(234.8,199);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f("#666666").s().p("AgYAcQgJgKAAgSQAAgRAJgKQAKgKAPAAQANAAAIAGQAIAFADAMIgTAEQgBgGgDgDQgEgDgFAAQgGAAgEAFQgFAFAAAMQAAAMAFAFQAEAFAGAAQAGAAADgDQAEgDABgIIATAEQgDANgIAGQgJAHgOAAQgPAAgJgKg");
	this.shape_74.setTransform(227,199);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f("#666666").s().p("AgeAyIgCgPIAIABQAHAAADgEQAEgEABgHIgbhHIAUAAIARAzIAQgzIAUAAIgZBFIgFAMQgCAHgDADQgCAEgCACIgHADIgKABIgLgBg");
	this.shape_75.setTransform(219.4,200.5);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f("#666666").s().p("AgbAgQgGgGAAgKQAAgGADgFQADgEAFgCQAGgCAJgCQANgDAFgCIAAgCQAAgGgDgCQgCgCgHAAQgFAAgDACQgDACgBAFIgSgDQADgLAHgFQAIgFANAAQANAAAFADQAHADACAFQADAEAAANIAAAVIABAPIADAJIgTAAIgBgFIgBgDQgGAFgFACQgFADgGAAQgLAAgHgGgAAAAEIgKAEQgEACAAAEQAAAEADADQADADAFAAQAEAAAFgDQAEgDAAgDIABgKIAAgEIgLADg");
	this.shape_76.setTransform(211.7,199);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f("#666666").s().p("AgJAkIAAg4IgXAAIAAgPIBBAAIAAAPIgXAAIAAA4g");
	this.shape_77.setTransform(204.3,199);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f("#666666").s().p("AgYAcQgJgKAAgSQAAgRAJgKQAKgKAPAAQANAAAIAGQAIAFADAMIgTAEQgBgGgDgDQgEgDgFAAQgGAAgEAFQgFAFAAAMQAAAMAFAFQAEAFAGAAQAGAAADgDQAEgDABgIIATAEQgDANgIAGQgJAHgOAAQgPAAgJgKg");
	this.shape_78.setTransform(197.1,199);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f("#666666").s().p("AAPAkIAAgtIgdAtIgSAAIAAhHIASAAIAAAsIAdgsIASAAIAABHg");
	this.shape_79.setTransform(188.8,199);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f("#666666").s().p("AgXAkIAAhHIAvAAIAAAPIgcAAIAAA4g");
	this.shape_80.setTransform(182.1,199);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f("#666666").s().p("AANAkIAAgeIgaAAIAAAeIgTAAIAAhHIATAAIAAAaIAaAAIAAgaIAUAAIAABHg");
	this.shape_81.setTransform(174.5,199);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f("#666666").s().p("AgbAgQgGgGAAgKQAAgGADgFQADgEAFgCQAGgCAJgCQANgDAFgCIAAgCQAAgGgDgCQgDgCgGAAQgFAAgDACQgDACgBAFIgSgDQADgLAHgFQAIgFANAAQANAAAFADQAHADACAFQADAEAAANIAAAVIABAPIADAJIgTAAIgBgFIgBgDQgGAFgFACQgEADgHAAQgLAAgHgGgAAAAEIgKAEQgEACAAAEQAAAEADADQADADAFAAQAEAAAFgDQAEgDAAgDIABgKIAAgEIgLADg");
	this.shape_82.setTransform(166.3,199);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f("#666666").s().p("AAeAyIAAhOIgUBOIgTAAIgUhOIAABOIgSAAIAAhjIAeAAIARBDIAShDIAeAAIAABjg");
	this.shape_83.setTransform(156.4,197.6);

	this.shape_84 = new cjs.Shape();
	this.shape_84.graphics.f("#666666").s().p("AAOAkIAAgeIgbAAIAAAeIgTAAIAAhHIATAAIAAAaIAbAAIAAgaIATAAIAABHg");
	this.shape_84.setTransform(358.6,94.9);

	this.shape_85 = new cjs.Shape();
	this.shape_85.graphics.f("#666666").s().p("AgSAhQgJgEgEgJQgFgJgBgLQABgKAFgJQAEgJAIgEQAJgFAKAAQAQAAALALQALAKAAAQQAAAQgLALQgLALgQAAQgJAAgJgFgAgMgQQgEAGAAAKQAAALAEAFQAFAGAHAAQAIAAAFgGQAFgFAAgLQAAgKgFgGQgFgFgIAAQgHAAgFAFg");
	this.shape_85.setTransform(350.1,94.9);

	this.shape_86 = new cjs.Shape();
	this.shape_86.graphics.f("#666666").s().p("AAPAzIAAgvIgdAvIgTAAIAAhIIATAAIAAAtIAdgtIATAAIAABIgAgNgjQgGgGAAgJIAJAAQAAAFADACQADADAEAAQAGAAADgDQADgCAAgFIAKAAQgBAJgFAGQgHAFgJAAQgIAAgFgFg");
	this.shape_86.setTransform(341.5,93.5);

	this.shape_87 = new cjs.Shape();
	this.shape_87.graphics.f("#666666").s().p("AgaAgQgHgGAAgKQAAgGADgFQADgEAFgCQAFgCAKgCQANgDAFgCIAAgCQAAgGgCgCQgDgCgHAAQgFAAgDACQgDACgCAFIgRgDQADgLAHgFQAHgFAOAAQAMAAAHADQAGADADAFQACAEAAANIAAAVIAAAPIAEAJIgTAAIgCgFIgBgDQgEAFgGACQgEADgHAAQgLAAgGgGgAAAAEIgKAEQgEACAAAEQAAAEADADQADADAFAAQAEAAAFgDQADgDACgDIAAgKIAAgEIgLADg");
	this.shape_87.setTransform(333.3,94.9);

	this.shape_88 = new cjs.Shape();
	this.shape_88.graphics.f("#666666").s().p("AgjAzIAAhjIASAAIAAAKQAEgFAGgEQAGgDAGAAQAMAAAJAKQAKAKgBASQABARgKAKQgJAKgMAAQgFAAgFgDQgFgCgGgGIAAAlgAgLgdQgEAFgBALQAAAMAGAFQAEAGAGAAQAHAAAEgGQAFgEAAgMQAAgMgFgFQgFgGgGAAQgGAAgFAGg");
	this.shape_88.setTransform(325.4,96.2);

	this.shape_89 = new cjs.Shape();
	this.shape_89.graphics.f("#666666").s().p("AAPAzIAAgvIgdAvIgTAAIAAhIIATAAIAAAtIAdgtIATAAIAABIgAgNgjQgGgGAAgJIAJAAQAAAFADACQADADAEAAQAGAAADgDQADgCAAgFIAKAAQgBAJgFAGQgHAFgJAAQgIAAgFgFg");
	this.shape_89.setTransform(312.7,93.5);

	this.shape_90 = new cjs.Shape();
	this.shape_90.graphics.f("#666666").s().p("AAPAkIAAgtIgdAtIgSAAIAAhHIASAAIAAAsIAcgsIATAAIAABHg");
	this.shape_90.setTransform(304.1,94.9);

	this.shape_91 = new cjs.Shape();
	this.shape_91.graphics.f("#666666").s().p("AAKAkIgKgUQgCgHgCgBQgCgCgEAAIAAAeIgTAAIAAhHIATAAIAAAdQAEAAACgCIAEgLQAEgLAEgDQAFgCAHAAIAHAAIAAAMQgHAAgCACQgCABgDAIQgDAKgGACQAJACAGANIABACIAKATg");
	this.shape_91.setTransform(296.8,94.9);

	this.shape_92 = new cjs.Shape();
	this.shape_92.graphics.f("#666666").s().p("AgYAcQgJgKAAgSQAAgRAJgKQAKgKAPAAQANAAAIAGQAIAFADAMIgTAEQgBgGgDgDQgEgDgFAAQgGAAgEAFQgFAFAAAMQAAAMAFAFQAEAFAGAAQAGAAADgDQAEgDABgIIATAEQgDANgIAGQgJAHgOAAQgPAAgJgKg");
	this.shape_92.setTransform(289,94.9);

	this.shape_93 = new cjs.Shape();
	this.shape_93.graphics.f("#666666").s().p("AgeAyIgCgPIAIABQAHAAADgEQAEgEABgHIgbhHIAUAAIARAzIAQgzIAUAAIgZBFIgFAMQgCAHgDADQgCAEgCACIgHADIgKABIgLgBg");
	this.shape_93.setTransform(281.4,96.4);

	this.shape_94 = new cjs.Shape();
	this.shape_94.graphics.f("#666666").s().p("AgaAaQgHgKAAgQQAAgRAKgKQAJgKAOAAQAPAAAKALQAJAKAAAVIgwAAQABAJAFAFQADAFAHAAQAEAAADgDQADgCABgGIATADQgDALgIAFQgIAGgMAAQgSAAgJgMgAgJgSQgEAFAAAIIAbAAQAAgJgDgEQgEgEgGAAQgGAAgEAEg");
	this.shape_94.setTransform(273.8,94.9);

	this.shape_95 = new cjs.Shape();
	this.shape_95.graphics.f("#666666").s().p("AAOAkIAAgeIgbAAIAAAeIgTAAIAAhHIATAAIAAAaIAbAAIAAgaIATAAIAABHg");
	this.shape_95.setTransform(265.8,94.9);

	this.shape_96 = new cjs.Shape();
	this.shape_96.graphics.f("#666666").s().p("AAPAzIAAgvIgdAvIgSAAIAAhIIASAAIAAAtIAdgtIASAAIAABIgAgNgjQgGgGgBgJIAJAAQABAFADACQADADAEAAQAGAAADgDQADgCABgFIAJAAQgBAJgFAGQgGAFgKAAQgHAAgGgFg");
	this.shape_96.setTransform(257.2,93.5);

	this.shape_97 = new cjs.Shape();
	this.shape_97.graphics.f("#666666").s().p("AgaAaQgHgKAAgQQAAgRAKgKQAJgKAOAAQAPAAAKALQAJAKAAAVIgvAAQAAAJAFAFQADAFAHAAQAEAAADgDQADgCABgGIATADQgDALgIAFQgIAGgMAAQgSAAgJgMgAgJgSQgEAFAAAIIAbAAQAAgJgDgEQgFgEgFAAQgGAAgEAEg");
	this.shape_97.setTransform(249,94.9);

	this.shape_98 = new cjs.Shape();
	this.shape_98.graphics.f("#666666").s().p("AgpAyIAAhjIBKAAIAAAQIg2AAIAAAYIAaAAQALAAAIACQAHAEAGAFQAFAIAAAKQAAAKgFAHQgFAIgIACQgGADgNAAgAgVAhIAUAAIANAAQADgCADgDQADgDAAgFQAAgHgFgDQgFgEgLABIgVAAg");
	this.shape_98.setTransform(240.3,93.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_98},{t:this.shape_97},{t:this.shape_96},{t:this.shape_95},{t:this.shape_94},{t:this.shape_93},{t:this.shape_92},{t:this.shape_91},{t:this.shape_90},{t:this.shape_89},{t:this.shape_88},{t:this.shape_87},{t:this.shape_86},{t:this.shape_85},{t:this.shape_84},{t:this.shape_83},{t:this.shape_82},{t:this.shape_81},{t:this.shape_80},{t:this.shape_79},{t:this.shape_78},{t:this.shape_77},{t:this.shape_76},{t:this.shape_75},{t:this.shape_74},{t:this.shape_73},{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// cities
	this.m_7 = new lib.m_7();
	this.m_7.parent = this;
	this.m_7.setTransform(179,276.9,1,1,0,0,0,14,13.5);

	this.m_6 = new lib.m_6();
	this.m_6.parent = this;
	this.m_6.setTransform(60,255.1,1,1,0,0,0,15.1,14.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.m_6},{t:this.m_7}]}).wait(1));

	// contour
	this.shape_99 = new cjs.Shape();
	this.shape_99.graphics.f().s("#373535").ss(1,1,1).p("EAE/gtlMAgUAAAMAAABbLMgwfgC7EAC2gkQIAuAHQAHgCANgDQALgDAIAAQAqABAagEQAlgEAegNQAhgPA7gHQAkgEArgNQATgFA6gUQAUgGAZgFQAPgCAggDQA2gGA0ABQAOAAAXADQAMACAbAEICHh8QAnArARASQAVAWAVAHQAZAIAcgJQAMgEAMAAQANABAFAGIARBJIAGAAIgUAvIAfATIgEAHIAygVIAMApIAGgGQAHACAPADQAMAEAIAFQAIAEAJAKQAFAFAKALQAPAPARADQAQADAVgGQAKgDAOAAQADAAAXACQATACAlAGQAcADAPARQAMAOAFAbQAKA3AYAlQAKAQAEAdQACAPABAaQABAdABAMQAIBHAIBHQAJBHAMBYQALBOAQBjQABAEADAGQAGAMAAAAQgJAIgFADQgIAHgHACQg5ATlWByQgDADgIACIAAGNIADNdAjk35QAIAIAJACQALACAQgFQANgFAUgDQALgBAVgDQAPgCAcgGQAZgFAOAFQAeAMAKADQAWAGARgCIAqAdQAMgKgBgJQgBgHgKgIQgDgDgKgLQgIgIgFgEQgOgKgVgJQgEgCgLgCQgIgCgFgCQgtgcg5AUQgxAQgPgBQgPgBgpgXQgIgEgJgLQgKgOgGgGQgBAAgrgQQgcgKgLgVQABgGAAgCQAAgGgBgDQgNgngGgUQgIgcAFgSQAFgSBTgZQgUgQgdAHQgfALgSADIgMgRIAtgrQAAAAgEgRQgDgKgCgGQgLgaAJgyQAJg0AUgNQAOgJAKgOQAJgKAJgTIBPAAQAYAAAwgBQAqgBAcAEQAtAFAngTQAfgOAngiQAHgIASgPQAPgNAJgKQAQgRAHgOQAKgTgBgVQAAgEADgFQADgGACgCICJpVMgqRAAAMACCBZTIYEhDAjk35QgKADgFACQgNAEgJACQgXAGgLgBQgMgBgegEQgHgBgGgBQgSgCgMAAQgugCgKgBQgggEgXgOQgKgGgSgDQgUgBgKgBQgUgDgVgJQgQgIgTgPQgRgNgOgDQgRgFgTAHQgHACgQgDQgSgEgGABQgiACgOABQgbADgUAFQgVAFgUgCQgSgCgVgJQgWgKghgEQgOgBgbgCQgcgBgNgCQgygFgrAgQgCABgCACQg4ArhNAHQgZADgUARQgRAPgOAaQgvBSAUBXQABAHAAALQAAAHgBAOIAAAcIgVAAQgHASgOAiQgDAGgIAZIgKAlQAbAeAIAKQASAZAIAbQAEANAOAiQAOAeAEATQAhgPAtASQAqARAfAmQgQAJgSgBQgRAAgegLIgXANQgUgLgFgEQgPgLgLABQgMAAgNANQgFAFgKALQgJAKgIAFQgJAFgJADQgLAEgHgCQgZgHgQgGQgWgIgQgLQgPgKgRgTQgTgWgKgLQgigogzAPQgdAJgYABQgcAAgZgLQgHgDgLgBQgMgBgHgBQgggDgRgCQgdgCgTgEQgXgFgFACQgEACgLATQgGALgIAYQgIATgPAEQABAQACAgQACAdAAATQABAoAgAjQAgAjAnAEQA0AEAwABQAvABAsAeQAsAeAPAqQAIAWgDAcQgEAyAoAbQAbASANAhQAJAUAGArQAGAoAKAXQAIARAOAaQABACAAACQATAkAFAKQAKAVAOAPQAPAPAVALQALAGAJASQAGALAJAWQAWAuAAA0QAAAugQA3QgNAtgDAQQgEAWAMALQAMAKAWgDICHgYQAJgBAmgFQAIhfBPhGQC/iaCQBNQC+Awg/kbQDYAqDqgXIEbhfQDRg8DQA8QBMhnhMguIj8hiQg8geCThiQG4gqg1B8QgnCtAnDVQADD1EDgXQDphHBnBRIAEaCQgqgEgTACQghADgQAcQgggRgjAKQgRAEgrAbIgMgUIgVAPQgpgLgfgnQgKgMgRgPQgTgRgJgJQgsgpgkhDQgeg/gPgfQgbg4gcghQgCgBgBgEQgCgEAAgBQgSgNgHgTQgFgRADgXQACgPgGgPQgEgLgKgRQgZgogvgmQgBgBhUg8Qgrgfg+gqQhHgvgjgYQgpgbgxAAQg9AChLAJQgpAFheAPQhVAOhkAlQguARiCA5QgdANgjAdQgUARgnAjQgVAShIA+Qg7AygkAeQgRANgIAIQgMANgFAOIAAWWAjk35QgCgCgCgDAx849QD0BKCZCUQDUDhjUBUQhdAok8hjQiJALBWBIQDVBGArB5QhIDpjug+QjDhXAOBXQABCKCxAAQAECAATBJQAiCHBRgxQCPhbDVAgQBMAXBOAPArMUVQABgFAMgeQANggABgEQAIgggBgRQAAgagPgVQgNgRgDgOQgKgkgCgSQgEgfAQgXQABgCAJgQQAGgLAFgFQAhgjAYg0QANgaAZhGQALgcAEgcQAEgdAGguIARAOQASgKAEgTQABgHgBgbQgBgYgNgPQgNgOgZgIQgKgEgJgHQgKgIgBgHQgCgMgIgGQgEgEgMgEQgygTgkACQgtACglAiQgHgJgEgEQgFgHgDgFQgagzhAAKQgYADgiAKQgnALgVAFQgagSgUgxQgJgVgPgdQgPghgIgRQgQgIgHgLQgIgMAGgOQAGgRgFgOQgDgJgNgOAzdlVQi0glhOCMAli3wQDmHhW/CX");
	this.shape_99.setTransform(210.7,280.3);

	this.shape_100 = new cjs.Shape();
	this.shape_100.graphics.f("#3399FF").s().p("EgVIgspMAqRAAAIiKJVIgFAIQgDAFABAEQAAAVgJATQgHAOgRARQgIAKgQANIgZAXQgnAigeAOQgoATgtgFQgdgEgpABIhIABIhQAAQgJATgIAKQgKAOgOAJQgVANgJA0QgIAyAKAaIAFAQIAFARIguArIANARQARgDAggLQAcgHAVAQQhTAZgFASQgGASAJAcIASA7QABADAAAGIgBAIQAMAVAcAKIAsAQIAQAUQAJALAIAEQApAXAPABQAOABAxgQQA6gUAtAcQAFACAJACQALACAEACQAVAJANAKQAGAEAHAIIAOAOQAJAIABAHQABAJgLAKIgrgdQgQACgWgGIgqgPQgOgFgYAFQgdAGgPACIgfAEQgVADgNAFQgQAFgKgCQgJgCgIgIIgEgFIAEAFIgQAFIgVAGQgXAGgLgBIgqgFIgOgCIgdgCIg4gDQghgEgXgOQgJgGgSgDIgegCQgUgDgVgJQgQgIgTgPQgSgNgNgDQgSgFgSAHQgIACgPgDQgSgEgGABIgwADQgbADgUAFQgWAFgTgCQgSgCgVgJQgWgKghgEIgpgDIgogDQgzgFgrAgIgEADQg3ArhNAHQgaADgTARQgRAPgPAaQguBSATBXQACAHgBALIAAAVIAAAcIgVAAIgVA0QgDAGgIAZIgLAlQAcAeAHAKQATAZAHAbQAEANAPAiQAOAeAEATQAhgPAtASQAqARAfAmQgRAJgRgBQgRAAgegLIgYANQgTgLgGgEQgOgLgLABQgMAAgNANIgPAQQgKAKgHAFQgJAFgJADQgLAEgIgCQgZgHgPgGQgXgIgPgLQgPgKgRgTIgdghQgjgogzAPQgcAJgYABQgcAAgZgLQgHgDgLgBIgTgCIgxgFQgegCgSgEQgYgFgEACQgEACgLATQgHALgHAYQgIATgPAEIADAwQACAdAAATQABAoAgAjQAfAjAoAEQAzAEAxABQAuABAtAeQAsAeAPAqQAIAWgDAcQgEAyAnAbQAcASANAhQAIAUAHArQAGAoAKAXIAVArIACAEIAYAuQAKAVAOAPQAPAPAVALQALAGAJASIAPAhQAWAtgBA1QABAugRA3QgNAtgCAQQgEAWAMALQAMAKAWgDICGgYIAwgGQANAOADAJQAFAOgHARQgFAOAIAMQAHALAPAIIAYAyIAXAyQAVAxAZASIA7gQQAjgKAXgDQBBgKAZAzIAJAMIALANQAkgiAugCQAkgCAyATQAMAEAEAEQAIAGABAMQABAHALAIQAJAHAKAEQAZAIANAOQANAPABAYQABAbgBAHQgEATgSAKIgRgOIgKBLQgFAcgKAcQgZBGgNAaQgYA0ghAjQgFAFgHALIgKASQgPAXAEAfQACASAKAkQADAOAMARQAQAVAAAaQAAARgHAgIgOAkQgNAeAAAFIAAWWI4DBDgAMl29IAAAAg");
	this.shape_100.setTransform(107.3,274.3);

	this.shape_101 = new cjs.Shape();
	this.shape_101.graphics.f("#FFFFCC").s().p("EgYPAqrIAA2WQAFgOAMgNQAIgIARgNQAkgeA7gyIBehQIA7g0QAigdAdgNQCCg5AugRQBkglBVgOQBfgPApgFQBLgJA9gCQAwAAAqAbIBqBHQA+AqArAfIBWA9QAvAmAYAoQALARADALQAGAPgCAPQgDAXAFARQAGATATANIACAFIADAFQAcAhAbA4IAsBeQAkBDArApIAdAaQARAPAJAMQAgAnApALIAVgPIAMAUQArgbARgEQAjgKAfARQARgcAhgDQATgCAqAEIgE6CIgCtdIgBmNQAHgCAEgDIGPiFQAGgCAJgHIAOgLIgGgMIgEgKIgbixIgVifIgQiOIgCgpQgBgagCgPQgEgdgKgQQgYglgKg3QgFgbgNgOQgOgRgcgDIg4gIIgZgCQgPAAgKADQgWAGgPgDQgRgDgPgPIgPgQQgKgKgHgEQgIgFgNgEIgVgFIgGAGIgMgpIgxAVIADgHIgggTIAVgvIgGAAIgRhJQgFgGgNgBQgMAAgLAEQgdAJgYgIQgWgHgVgWIg4g9IiHB8IgogGQgWgDgOAAQgzgBg2AGIgvAFQgaAFgTAGQg7AUgSAFQgrANgkAEQg7AHghAPQgeANglAEQgZAEgrgBQgIAAgLADIgUAFIgvgHICLpVMAgRAAAMAAABbLg");
	this.shape_101.setTransform(294.2,280.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_101},{t:this.shape_100},{t:this.shape_99}]}).wait(1));

	// m_1
	this.m_1 = new lib.m_1();
	this.m_1.parent = this;
	this.m_1.setTransform(280.5,108.8,1,1,0,0,0,108.7,82.7);

	this.timeline.addTween(cjs.Tween.get(this.m_1).wait(1));

	// m_3
	this.m_3 = new lib.m_5();
	this.m_3.parent = this;
	this.m_3.setTransform(78.5,188.6,1,1,0,0,0,67.5,68);

	this.timeline.addTween(cjs.Tween.get(this.m_3).wait(1));

	// m_4
	this.m_4 = new lib.m_4();
	this.m_4.parent = this;
	this.m_4.setTransform(105.3,274.5,1,1,0,0,0,45,29.1);

	this.timeline.addTween(cjs.Tween.get(this.m_4).wait(1));

	// m_5
	this.m_5 = new lib.m_3();
	this.m_5.parent = this;
	this.m_5.setTransform(219.9,328.1,1,1,0,0,0,126.2,121.6);

	this.timeline.addTween(cjs.Tween.get(this.m_5).wait(1));

	// m_2
	this.m_2 = new lib.m_2();
	this.m_2.parent = this;
	this.m_2.setTransform(205.8,197.8,1,1,0,0,0,139.9,79.9);

	this.timeline.addTween(cjs.Tween.get(this.m_2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(171,262.5,479.3,585.7);
// library properties:
lib.properties = {
	width: 400,
	height: 550,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	webfonts: {},
	manifest: [],
	preloads: []
};




})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{}, AdobeAn = AdobeAn||{});
var lib, images, createjs, ss, AdobeAn;