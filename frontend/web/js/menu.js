$('#main_menu .menu a').click(function() {
    if(!$(this).hasClass('opened')) {
        $('#main_menu').addClass('opened');
    }

    if( $(this).attr('toggle') ) {

        $('#main_submenu > .group').each(function(e) {
            $(this).hide();
        });

        $('#' + $(this).attr('toggle')).show();

    } else {
        $('#main_menu').removeClass('opened');
        return true;
    }

    return false;
});


$('.principe-info').click(function(){

    $('.principes > div').each(function(itm){
        $(this).hide();
    });
    $($(this).attr('data-show')).show();
    $('#principe-modal').modal('show');

});


$('ul.ul-clicker span').click(function(){

    $(this).parent().toggleClass('active');

});

$('.alpgabit span').click(function(e) {
    var ch = $(e.target).html();
    var found = false;

    $('li > span').each(function(e) {

        if(found) return;

        _ch = $(this).html();
        // console.log(_ch[0]);

        if( ch == _ch[0]) {
            found = true;
            $([document.documentElement, document.body]).animate({
                scrollTop: $(this).offset().top
            }, 1000);
        }

    });

    console.log(  );
});
