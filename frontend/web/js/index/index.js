var chart1;

$(document).ready(function(e) {

    getData();

    $("#btn-chart-1").on("click", function(e) {
        getData();
    });

});

function createIndexChart(data) {

    if(data.report) $("#link1").attr("href", data.report);

    var height = 360,
        width = 180,
        parts = ["#left-top", "#left-bot", "#right-top", "#right-bot", ".datetime"],
        bars = [".bar.left .fill", ".bar.right .fill"];

    $(parts[0]).html(appendCoef(data.income.plan_payments, data.coeficient));
    $(parts[1]).html(appendCoef(data.income.budget_execution, data.coeficient) + "<br/>" + data.income.budget_execution_percent + "%");
    $(parts[2]).html(appendCoef(data.outcome.plan_payments, data.coeficient));
    $(parts[3]).html(appendCoef(data.outcome.budget_execution, data.coeficient) + "<br/>" + data.outcome.budget_execution_percent + "%");

    $(parts[4]).html(data.date);

    $(bars[0]).css({
        height: scaleData(data.income.budget_execution_percent, height)
    });
    $(bars[1]).css({
        height: scaleData(data.outcome.budget_execution_percent, height)
    });

}

function appendCoef(value, coef) {
    var val = (value * +(coef)).toFixed(2);
    val = isNaN(val) ? 0 : val.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
    return val;
}

function scaleData(value, max) {
    if (value / 100 * max > max) return max;
    return value / 100 * max + "px";
}

function createTables(data) {
    returnTables("t_income", data.income);
    returnTables("t_outcome", data.outcome);
    if(data.report1) $("#link2").attr("href", data.report1);
    if(data.report2) $("#link3").attr("href", data.report2);
    if(data.report3) $("#link4").attr("href", data.report3);
}

function returnTables(tableid, data) {
    $("#" + tableid + " thead tr").html("");
    $("#" + tableid + " tbody").html("");
    data.forEach( function(row, i) {
        if(i == 0) {
            row.forEach(function(cell, j) {
                $("#"+tableid+" thead tr").append("<th>" + cell + "</th>")
            })
        } else {
            var _row = "";
            row.forEach(function(cell, j) {
                if(!isNaN(+(cell))) {
                    cell = +cell;
                    cell = cell.toFixed(2).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
                }
                _row += "<td>" + cell + "</td>";
            })
            $("#"+tableid+" tbody").append("<tr>" + _row + "</tr>")
        }
    });
}

function getData() {
    $.ajax({
        url: '/' + language + '/ajax/index-chart-1',
        type: 'POST',
        data: $('#form-chart-1').serialize(),
        dataType: 'JSON',
        success: function(data) {
            if(typeof data !== 'undefined') {
                createIndexChart(data);
            } else {
                console.log(data);
            }
        }
    });

    $.ajax({
        url: '/' + language + '/ajax/index-chart-2',
        type: 'POST',
        data: $('#form-chart-1').serialize(),
        dataType: 'JSON',
        success: function(data) {
            if(typeof data !== 'undefined') {
                createTables(data);
            } else {
                console.log(data);
            }
        }
    });
}
