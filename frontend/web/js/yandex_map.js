ymaps.ready(init);

var items = [
    {
        hintContent: 'ТОО "Қараойқұрылыс"',
        balloonContent: '«Привязка типового проекта «Средняя общеобразовательная школа на 600 учащихся для IVA, IVГ климатических подрайонов с обычными геологическими условиями»'
    },
    {
        hintContent: 'ТОО "Альянс ЛТД"',
        balloonContent: 'Строительство сетей водоснабжения'
    },
    {
        hintContent: 'ТОО "Ақ жол құрылыс"',
        balloonContent: 'Реконструкция автомобильной дороги "Киякты-Тущыкудук"'
    }
];

var marks = false;

function openMark(i) {
    marks[i].balloon.open();
}

function createItemRow(container, index, name) {
    var container = document.getElementById(container);

    var newitem = document.createElement('div');
    var txt = document.createTextNode(name);
    newitem.appendChild(txt);
    newitem.onclick  = function() {
        marks[index].balloon.open();
    }

    container.appendChild(newitem);

}

function init(){

    var myMap = new ymaps.Map("map", {
        center: [43.915464, 52.692646],
        zoom: 7
    });

    marks = [
        new ymaps.Placemark([43.756414, 51.116108], {
            hintContent: 'ТОО "Қараойқұрылыс"',
            balloonContent: '«Привязка типового проекта «Средняя общеобразовательная школа на 600 учащихся для IVA, IVГ климатических подрайонов с обычными геологическими условиями»'
        }),
        new ymaps.Placemark([43.720569, 51.511616], {
            hintContent: 'ТОО "Альянс ЛТД"',
            balloonContent: 'Строительство сетей водоснабжения'
        }),
        new ymaps.Placemark([44.358533, 54.093403], {
            hintContent: 'ТОО "Ақ жол құрылыс"',
            balloonContent: 'Реконструкция автомобильной дороги "Киякты-Тущыкудук"'
        }),
        new ymaps.Placemark([43.683157, 51.154571], {
            hintContent: 'ТОО "АктауСаулетКурылыс"',
            balloonContent: '«Привязка типового проекта «ТП РК 900 СОШ (IVA, IVГ) -2.2-2012 средняя общеобразовательная школа на 900 учащихся для IVA и IVГ климатических подрайонов с обычными геологическими условиями» в 32 «А» "'
        }),
        new ymaps.Placemark([43.680790, 51.210189], {
            hintContent: 'ТОО "АктауСаулетКурылыс"',
            balloonContent: '«Привязка типового проекта «ТП РК 900 СОШ (IVA, IVГ) -2.2-2012 средняя общеобразовательная школа на 900 учащихся для IVA и IVГ климатических подрайонов с обычными геологическими условиями» в 32 «А» "'
        }),
        new ymaps.Placemark([44.358533, 54.093403], {
            hintContent: 'ТОО "Шебер курылыс"',
            balloonContent: 'Строительство внутримирорайонных инженерных сетей (водоснабжение, канализация) и автомобильных дорог, проездных зон, тротуаров, благоустройство придорожных территорий в 17 мкр. - 2 очередь'
        }),
        new ymaps.Placemark([44.845092, 53.511141], {
            hintContent: 'ТОО "Шебер курылыс"',
            balloonContent: 'Строительство водоочистного сооружения и внутрипоселкового водопровода '
        }),
        new ymaps.Placemark([43.248523, 52.236727], {
            hintContent: ' ТОО "АС Құрылыс"',
            balloonContent: 'Строительство 4-х этажного арендного жилого дома             1-очередь'
        }),
    ];

    for(var i = 0; i < marks.length; i++) {
        myMap.geoObjects.add(marks[i]);
        createItemRow('mapitems', i, marks[i].properties._data.balloonContent);
    }

}
