var colorset = ["#A7C6DA", "#EEFCCE", "#9EB25D", "#F1DB4B", "#EDFF71"];

var chart1, chart2;
isArray = Array.isArray ?
    function (obj) {
      return Array.isArray(obj);
    } :
    function (obj) {
      return Object.prototype.toString.call(obj) === '[object Array]';
    };

function getValueAtIndexOrDefault(value, index, defaultValue) {
    if (value === undefined || value === null) {
      return defaultValue;
    }

    if (this.isArray(value)) {
      return index < value.length ? value[index] : defaultValue;
    }

    return value;
  };
$(document).ready(function(e) {

    var ctx = document.getElementById("pie");
    var ctx2 = document.getElementById("stacked");
    ctx2.height = 300;
    chart1 = new Chart(ctx, {
        type: 'outlabeledPie',
        options: {
            legend: {
                display: true,
                position: 'right',
                font: {
                    size: 17
                }
            },
            plugins: {
                // Change options for ALL labels of THIS CHART
                colorschemes: {
                    scheme: 'brewer.Paired12'
                },
                outlabels: {
                    text: '%l %p',
                    color: 'black',
                    stretch: 45,
                    font: {
                        resizable: true,
                        minSize: 12,
                        maxSize: 18
                    }
                }
            }
        },
        gridLines: {
            display: false
        }
    });

    chart2 = new Chart(ctx2, {
        type: 'bar',
        data: {
            labels: [],
            datasets: []
        },
        options: {
            plugins: {
                colorschemes: {
                    scheme: 'brewer.Paired12'
                }
            },
            legend: {
                display: true,
                position: 'bottom',
                font: {
                    size: 17
                }
            },
            scales: {
                xAxes: [{ stacked: true }],
                yAxes: [{ stacked: true }]
            }
        }
    });

    getData();

    $('#btn-chart').click(function(e) {
        getData();
    });

});

function getData() {
    $.ajax({
        url: '/analytics/income-pie',
        type: 'POST',
        dataType: 'JSON',
        data: $('#form-chart').serializeArray(),
        success: function(data) {

            chart1.data = data.data;
            chart1.labels = data.labels;
            chart1.data.datasets[0].backgroundColor = colorset;
            chart1.data.datasets[0].fill = true;
            chart1.update();

            // console.log(data.data2);
            chart2.data.labels = data.data2.labels;
            chart2.data.datasets = data.data2.datasets;
            // chart2.data = data.data2;
            // chart2.data.datasets[0].backgroundColor = colorset;
            // chart2.data.datasets[0].fill = true;
            chart2.update();

            $('#income-total').html(data.total_income);

            $('.datetime').html(data.date);



        }
    });
}
