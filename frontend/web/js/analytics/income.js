// var height = 400;
var ctx1 = document.getElementById("pie");
var ctx2 = document.getElementById("stacked");

var chartpie = new chartpie(ctx1);
var stackedBar = new stackedBar(ctx2);

$(document).ready(function(e) {
    getData();
    $('#btn-chart').click(function(e) {
        getData();
    });

    $('[name="tempo[]"]').click(function(e) {
        calc_table();
    });

});

function getData() {
    $.ajax({
        url: '/' + language + '/analytics/income-pie',
        type: 'POST',
        dataType: 'JSON',
        data: $('#form-chart').serializeArray(),
        success: function(data) {

            // chart_pie(data.data_pie, ctx1);
            chartpie.setData(data.data_pie);
            chartpie.update();
            stackedBar.setData(data.data_bars);
            stackedBar.update();
            // chart_bars(data.data_bars, ctx2);
            chart_table(data.data_tables, $('#table'), $('#table-head'));
            $('#income-total').html(formatNumber(data.total_income));
            $('.datetime').html(data.date);
            calc_table();
        }
    });
}

function calc_table() {
    var status = $('[name="tempo[]"]')[0].checked;
    if(status != true) {
        $('.period').attr('colspan', '2');
        $('.total').hide();
    } else {
        $('.period').attr('colspan', '1');
        $('.total').show();
    }
}

function chart_table(data, container, header) {
    container.html("");
    header.html("");

    data.forEach(function(d, i) {

        if(i >= 2) {
            var _class = d.header ? "class='header'" : '';
            var tempo1 = (d.data[3] / d.data[1] * 100 - 100).toFixed(2),
                tempo2 = (d.data[6] / d.data[3] * 100 - 100).toFixed(2);

            console.log(d.data[1], d.data[3], d.data[6]);

            tempo1 = isNaN(tempo1) ? 0 : tempo1;
            tempo2 = isNaN(tempo2) ? 0 : tempo2;

            var class1 = (tempo1 > 0) ? "plus" : "minus",
                class2 = (tempo2 > 0) ? "plus" : "minus";

            container.append(`<tr ${_class}>
                                <td>${d.data[0]}</td>
                                <td colspan="2">${formatNumber(d.data[1])}</td>
                                <td class="period">${formatNumber(d.data[3])}</td>
                                <td class="total ${class1}"> ${tempo1} </td>                       // Темп роста 1
                                <td>${formatNumber(d.data[6])}</td>
                                <td class="total ${class2}">${tempo2}</td>                      // Темп роста 2
                                <td>${formatNumber(d.data[5])}</td>
                                <td>${d.data[7]}</td>
                            </tr>`);
        } else {
            if(i == 0) {
                header.append(`<tr>
                                    <th rowspan="2">${d.data[0]}</th>
                                    <th colspan="2">${d.data[1]}</th>
                                    <th colspan="2">${d.data[2]}</th>
                                    <th colspan="4">${d.data[3]}</th>
                                </tr>`);
            }
            if(i ==1 ) {
                header.append(`<tr>
                                    <th colspan="2">${d.data[0]}</th>

                                    <th class="period">${d.data[2]}</th>
                                    <th class="total"> </th>                    // Темп роста 1
                                    <th>${d.data[4]}</th>
                                    <th class="total"></th>                     // Темп роста 2
                                    <th>${d.data[5]}</th>
                                    <th>${d.data[6]}</th>
                                </tr>`);
            }
        }


    });
}

function chartpie(element) {
    this.data;
    this.element = element;

    this.setData = function(data) {
        this.data = data;
        total = 0;
        for(var elem in this.data) {
            total += +data[elem].value;
        }
        for(var i in this.data) {
            this.data[i].visible = true;
            this.data[i].percent = (data[i].value / total * 100).toFixed(2);
        }
    }

    this.update = function() {
        $(this.element).html("");
        var data = this.data;
        var width = $(this.element).parent().width();
        var height = 500;
        var thickness = 40;
        var duration = 750;
        var padding = 0;
        var opacity = .1;
        var opacityHover = 1;
        var otherOpacityOnHover = .8;
        var tooltipMargin = 13;

        var radius = Math.min(width-padding, height-padding) / 1.8;

        var outerArc = d3.arc()
                   .outerRadius(radius * 0.7)
                   .innerRadius(radius * 0.7);

        var color = d3.scaleOrdinal(d3.schemeCategory10);


        var tip = d3.tip()
            .attr('class', 'd3-tip')
            .html(function(d) {
                return `Наименование: ${d.data.name} <br/>
                        Объем показателя: ${formatNumber(d.data.value)} <br/>
                        Доля в общем объеме: ${d.data.percent} %`;
            });



        var svg = d3.select(this.element)
            .append('svg')
            .attr('class', 'pie')
            .attr('style', 'overflow:visible')
            .attr('width', width)
            .attr('height', height);

        svg.call(tip);

        var g = svg.append('g')
            .attr('transform', 'translate(' + (width/2) + ',' + (height/2) + ')');

        var arc = d3.arc()
            .innerRadius(0)
            .outerRadius(radius * 0.6);

        var pie = d3.pie()
            .value(function(d) {
                if(d.visible) return +(d.value);
                return 0;
            })
            .sort(null);
        var path = g.selectAll('path')
          .data(pie(data))
          .enter()
          .append("g")

          .attr('transform', 'translate(-' + width/6 + ',0)')
          .append('path')
          .attr('d', arc)
          .attr('fill', function(d,i) {
              return color(i);
          })
          .style('opacity', 1)
          .style('stroke', 'white')
          .style('stroke-width', '3')
          .on('mouseover', function(d) {
              $(this).attr("style", "opacity: 0.8; stroke: white; stroke-width: 3;");
              tip.show(d);
          })
          .on('mouseout', function(d){
              $(this).attr("style", "opacity: 1; stroke: white; stroke-width: 3;");
              tip.hide(d);
          })
          .each(function(d, i) { this._current = i; });

          if(window.location.pathname == "/analytics/income") {
              var polyline =
                    g.selectAll('polyline')
                    .data(pie(data))
                  .enter().append('polyline')
                    .attr('points', function(d) {
                        var pos = outerArc.centroid(d);
                        pos[0] = radius * 0.5 * (midAngle(d) < Math.PI ? 1 : -1);
                        return [arc.centroid(d), outerArc.centroid(d), pos]
                    })
                    .attr('transform', 'translate(-' + width/6 + ',0)');
                var label = g.selectAll('text')
                    .data(pie(data))
                    .enter().append('text')
                        .attr('dy', '.35em')
                    .html(function(d) {
                        return d.data.name + ": " + (d.data.value / total * 100).toFixed(2) + '%' ;
                    })
                    .attr('transform', function(d) {
                            var pos = outerArc.centroid(d);
                            pos[0] = radius * 0.5 * (midAngle(d) < Math.PI ? 1 : -1);
                            pos[0] -= width / 6;
                            // pos[1] -= 5;
                            return 'translate(' + pos + ')';
                        })
                    .style('text-anchor', function(d) {
                        return (midAngle(d)) < Math.PI ? 'start' : 'end';
                 });
          }



          var legend = d3.select(element).append('div')
          			.attr('class', 'legend');


          var keys = legend.selectAll('.key')
          			.data(data)
          			.enter().append('div')
          			.attr('class', 'key')
          			.style('display', 'block')
          			.style('margin-right', '20px')
          			.style('cursor', 'pointer')
                    .style('opacity', function(d) {
                        if(d.visible) return 1;
                        return 0.5;
                    })
                    .on('click', function(d) {
                        d.visible = !d.visible;
                        chartpie.update();
                    });

      	keys.append('div')
      		.attr('class', 'symbol')
      		.style('height', '10px')
      		.style('display', 'inline-block')
      		.style('vertical-align', 'top')
      		.style('width', '10px')
      		.style('margin', '5px 5px')
      		.style('background-color', (d, i) => color(i));

      	keys.append('div')
      		.attr('class', 'name')
      		.style('display', 'inline')
      		.style('vertical-align', 'top')
      		.text(d => d.name + ': ' + formatNumber(d.value));

      	keys.exit().remove();

        function midAngle(d) { return d.startAngle + (d.endAngle - d.startAngle) / 2 ; }

    }
}


function stackedBar(element) {
    this.data;
    this.groups = [];
    this.element = element;

    this.setData = function(data) {
        this.data = data;
        this.groups = [];
        for(var i in this.data.groups) {
            this.groups.push({
                visible: true,
                value: this.data.groups[i]
            });
        }
    }


    this.getGroups = function() {
        var _new_arr = [];
        for (var i in this.groups) _new_arr.push(this.groups[i].value);
        return _new_arr;
    }

    this.hide_group = function(name) {
        for(var i in this.groups) {
            if(this.groups[i].value == name) this.groups[i].visible = !this.groups[i].visible;
            console.log(this.groups[i].visible);
        }
    }

    this.createChartLegend = function (group) {
        var z = d3.scaleOrdinal(d3.schemeCategory10);
        // $(mainDiv).append("<div id='Legend_" + mainDivName + "' class='pmd-card-body' style='margin-top:0; margin-bottom:0;'></div>");
        var container = document.createElement("ul");
        $(container).addClass('legend');

        var nexElem = $(element).next();

        if($(nexElem).hasClass('legend')) {
            container = nexElem;
            $(container).html("");
        }

        var keys = group;
        keys.forEach(function(d) {
            var cloloCode = z(d.value);
            var span = document.createElement("li");
            if(d.visible) {
                span.setAttribute('class', 'item');
            } else {
                span.setAttribute('class', 'item hdn');
            }
            span.setAttribute('style', 'list-style: none; margin-right:10px;');

            $(span).on('click', function(e) {
                stackedBar.hide_group(d.value);
                stackedBar.update();
            })

            var span2 = document.createElement("span");
            span2.setAttribute('class', 'square');
            span2.setAttribute('style', 'background: ' + cloloCode + ';');

            var span3 = document.createElement("span");
            span3.setAttribute('class', 'text');
            span3.textContent = d.value;

            span.append(span2);
            span.append(span3);
            container.append(span);
        });

        $(element).after(container);

    }

    this.isVisible = function(name) {
        for(var i in this.groups) {
            // console.log(this.groups[i].value, name);
            if(this.groups[i].value == name) return this.groups[i].visible;
        }
        return false;
    }

    this.update = function() {
        $(element).html("<svg></svg>");
        // var group = this.data.groups;
        var group = this.getGroups();
        var parseDate = d3.timeFormat("%Y");
        this.createChartLegend(this.groups);
        var salesData = this.data.data;
        var new_arra = [];

        for (var i in salesData) {
            var __ar = [];
            for(var k in salesData[i]) {
                if(k == 'date') {
                    __ar[k] = salesData[i][k];
                    continue;
                };
                if(this.isVisible(k)) {
                    __ar[k] = +salesData[i][k];
                } else {
                    __ar[k] = 0;
                }
            }
            new_arra[i] = __ar;
        }

        salesData = new_arra;

        var tip = d3.tip()
            .attr('class', 'd3-tip')
            .html(function(d) {
                // console.log(d);
                return `${d.key} : ${formatNumber(d.data[d.key])}`;
            });

        var layers = d3.stack()
            .keys(group)
            .offset(d3.stackOffsetDiverging)
            (salesData);

        var svg = d3.select("#stacked svg"),
            margin = {
                top: 20,
                right: 30,
                bottom: 60,
                left: 100
            },
        width = $(element).parent().width() * 1,
        height = 420;

        if(width > 800) width = 800;


            svg.attr('width', width)
            .attr('height', height);

        svg.call(tip);
        var x = d3.scaleBand()
                .rangeRound([margin.left, width - margin.right])
                .padding(0.1)
                .paddingInner(0.1);;

            x.domain(salesData.map(function(d) {
                return d.date;
            }))

            var y = d3.scaleLinear()
                .rangeRound([height - margin.bottom, margin.top]);

            y.domain([d3.min(layers, stackMin), d3.max(layers, stackMax)])

            function stackMin(layers) {
                return d3.min(layers, function(d) {
                    return d[0];
                });
            }

            function stackMax(layers) {
                return d3.max(layers, function(d) {
                    return d[1];
                });
            }

            var z = d3.scaleOrdinal(d3.schemeCategory10);

            var maing = svg.append("g")
                .selectAll("g")
                .data(layers);
            var g = maing.enter().append("g")
                .attr("fill", function(d) {
                    return z(d.key);
                });

            var rect = g.selectAll("rect")
                .data(function(d) {
                    d.forEach(function(d1) {
                        d1.key = d.key;
                        return d1;
                    });
                    return d;
                })
                .enter().append("rect")
                .attr("data", function(d) {
                    var data = {};
                    data["key"] = d.key;
                    data["value"] = d.data[d.key];
                    var total = 0;
                    group.map(function(d1) {
                        total = total + d.data[d1]
                    });
                    data["total"] = total;
                    return JSON.stringify(data);
                })
                // .attr("width", (x.bandwidth() > 100) ? 100 : x.bandwidth )
                .attr("width", x.bandwidth )
                .attr("x", function(d) {
                    return x(d.data.date);
                })
                .attr("y", function(d) {
                    return y(d[1]);
                })
                .attr("height", function(d) {
                    return y(d[0]) - y(d[1]);
                });


                rect.on('mouseover', function(e) {
                    $(this).attr("style","opacity: 0.7");
                    return tip.show(e);
                })
                    .on('mouseout', function(e) {
                        $(this).attr("style","opacity: 1");
                        return tip.hide(e);
                    })

                svg.append("g")
                    .attr("transform", "translate(0," + y(0) + ")")
                    .style("font", "14px sans")
                    .call(d3.axisBottom(x))
                    .append("text")
                    .attr("x", width / 2)
                    .attr("y", margin.bottom * 0.5)
                    .attr("dx", "0.32em")
                    .attr("fill", "#000")
                    .attr("font-weight", "bold")
                    .attr("text-anchor", "start")
                    // .text("Год");

                svg.append("g")
                    .attr("transform", "translate(" + margin.left + ",0)")
                    .style("font", "14px sans")
                    .call(d3.axisLeft(y))
                    .append("text")
                    .attr("transform", "rotate(-90)")
                    .attr("x", 0 - (height / 2))
                    .attr("y", 15 - (margin.left))
                    .attr("dy", "0.32em")
                    .attr("fill", "#000")
                    .attr("font-weight", "bold")
                    .attr("text-anchor", "middle")
                    // .text("тыс. тг.");

                var rectTooltipg = svg.append("g")
                    .attr("font-family", "sans-serif")
                    .attr("font-size", 10)
                    .attr("text-anchor", "end")
                    .attr("id", "recttooltip_")
                    .attr("style", "opacity:0")
                    .attr("transform", "translate(-500,-500)");

                rectTooltipg.append("rect")
                    .attr("id", "recttooltipRect_")
                    .attr("x", 0)
                    .attr("width", 120)
                    .attr("height", 80)
                    .attr("opacity", 0.71)
                    .style("fill", "#000000");

                rectTooltipg
                    .append("text")
                    .attr("id", "recttooltipText_")
                    .attr("x", 30)
                    .attr("y", 15)
                    .attr("fill", function() {
                        return "#fff"
                    })
                    .style("font-size", function(d) {
                        return 10;
                    })
                    .style("font-family", function(d) {
                        return "arial";
                    })
                    .text(function(d, i) {
                        return "";
                    });

                    var helpers = {
                            getDimensions: function(id) {
                                var el = document.getElementById(id);
                                var w = 0,
                                    h = 0;
                                if (el) {
                                    var dimensions = el.getBBox();
                                    w = dimensions.width;
                                    h = dimensions.height;
                                } else {
                                    console.log("error: getDimensions() " + id + " not found.");
                                }
                                return {
                                    w: w,
                                    h: h
                                };
                            }
                        };
    }

}

function formatNumber(x) {
    var x;
    try {
        x = x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
    } catch (e) {
        x = 0;
    } finally {
        return x;
    }
}
