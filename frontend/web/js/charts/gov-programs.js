var nodes = null;
var _nodes = [];
var _edges = [];
var edges = null;
var network = null;
var data = null;


var DIR = '/images/style/';

function rec(object, index, imageindex) {

    if (index > 2) return;
    if(index == 0) imageindex = 2;
    if(index == 1) imageindex = object.id;

    if(object.children) {
        for (var _object in object.children) {
            // console.log(object.children[_object]);
            rec(object.children[_object], index + 1, imageindex);
        }

        _nodes.push({
            id: parseInt(object.id),
            label: object.name,
            shape: 'circularImage',
            image: DIR + 'circle-' + imageindex + '.png',
            title: object.name + "<hr/>Запланировано: " + addCommas((object.model.budget_corrected * 0.001).toFixed(1)) + " млн. тг" + "<br/>Исполнено: " + addCommas((object.model.budget_execution * 0.001).toFixed(1)) + " млн. тг"
        });

        _edges.push({
            from:parseInt(object.id),
            to: parseInt(object.parentid)
        });
    }

}

function draw() {
      // create people.
      // value corresponds with the age of the person
      // var DIR = '/images/style/';
      nodes = [
        {id: 1,  shape: 'circularImage', image: DIR + 'process-1-1.png'},
        {id: 2,  shape: 'circularImage', image: DIR + 'process-2-2.png'},
        {id: 3,  shape: 'circularImage', image: DIR + 'process-3-3.png'},
        {id: 4,  shape: 'circularImage', image: DIR + 'process-4-4.png', label:"pictures by this guy!"},
        {id: 5,  shape: 'circularImage', image: DIR + 'process-5-5.png'},
        {id: 6,  shape: 'circularImage', image: DIR + 'process-6-6.png'},
        {id: 7,  shape: 'circularImage', image: DIR + 'process-7-7.png'},
        {id: 8,  shape: 'circularImage', image: DIR + 'process-8-8.png'},
        {id: 9,  shape: 'circularImage', image: DIR + 'process-9-9.png'},
        {id: 10, shape: 'circularImage', image: DIR + 'process-10-10.png'},
        {id: 11, shape: 'circularImage', image: DIR + 'process-11-11.png'},
        {id: 12, shape: 'circularImage', image: DIR + 'process-12-12.png'},
        {id: 13, shape: 'circularImage', image: DIR + 'process-13-13.png'},
        {id: 14, shape: 'circularImage', image: DIR + 'process-14-14.png'}
      ];

      // create connections between people
      // value corresponds with the amount of contact between two people
      edges = [
        {from: 1, to: 2},
        {from: 2, to: 3},
        {from: 2, to: 4},
        {from: 4, to: 5},
        {from: 4, to: 10},
        {from: 4, to: 6},
        {from: 6, to: 7},
        {from: 7, to: 8},
        {from: 8, to: 9},
        {from: 8, to: 10},
        {from: 10, to: 11},
        {from: 11, to: 12},
        {from: 12, to: 13},
        {from: 13, to: 14},
        {from: 9, to: 16}
      ];

      // create a network
      var container = document.getElementById('mynetwork');
      var data = {
        nodes: _nodes,
        edges: _edges
      };
      var options = {
          locale: 'ru',
        layout: {
            improvedLayout: true
        },
        nodes: {
          borderWidth:4,
          size:20,
	      color: {
            border: '#222222',
            background: '#666666'
          },
          font:{
              color:'#000',
              size: 10
          },
          widthConstraint: {
            maximum: 150
          }
        },
        edges: {
          color: {
              color: '#ccc'
          }
      }

      };
      network = new vis.Network(container, data, options);
    //
    //   network.on("click", function (params) {
    //     params.event = "[original event]";
    //     // document.getElementById('eventSpan').innerHTML = '<h2>Click event:</h2>' + JSON.stringify(params, null, 4);
    //     console.log('click event, getNodeAt returns: ' + this.getNodeAt(params.pointer.DOM));
    // });

    //   network.on("stabilized", function (params) {
    //     this.focus(2, {scale: 2});
    // });


}

document.addEventListener("DOMContentLoaded", function() {
    console.log(chart_data);
    rec(chart_data, 0, 0);
    draw() ;
    network.focus(2, {scale: 2});
});
