var ctx = document.getElementById("dynamic-income");
var chart;
var months = [];
var datasets = [];
var colors = ['#749abd', '#00cc66', '#ffcc00', '#ff5050', '#ff00ff'];

function chart_dynamic() {
    chart = new Chart(ctx, {
        type: 'line',
        data: {
            // labels: months,
            labels: [],
            datasets: datasets
        },
        options: {
            responsive: true,
            tooltips: {
                mode: 'y',
                callbacks: {
                    label: function(tooltipItem, data) {
                        var _label = data.datasets[tooltipItem.datasetIndex].label;
                        return _label + " : " + addCommas(tooltipItem.yLabel.toString());
                    }
                }
            },
            scales: {
                xAxes: [{
                    type: 'category',
                    display: true,
                    scaleLabel: {
                        display: true
                    },
                    ticks: {
                        fontStyle: 'bold'
                    }
                }],
                yAxes: [{
                    ticks: {
                        callback: function(value) {
                            return addCommas(value);
                        }
                    }
                    // stacked: true
                }]
            },
            plugins: {
                datalabels: {
                    align: 'top',
					color: 'black',
                    rotation: -45,
					font: {
						weight: 'bold'
					},
                    formatter: function (value) {
                        return addCommas(value.y);
                    },
                    display: function (context) {
                        return context.chart.isDatasetVisible(context.datasetIndex);
                    }
                }
            },
        }
    });
}


function update_chart() {
    $.ajax({
        url: '/ajax/dynamics-income',
        type: 'POST',
        dataType: 'JSON',
        data: {
            'city_id' : $('#city_id').val(),
            'type_id' : 2,
            'multiplier' : $('#multiplier').val(),
            'date_from' : $('#date_from').val(),
            'date_to' : $('#date_to').val(),
            'report' : $('#report_id').val()
        },
        success: function(data) {
            months = data.months;
            datasets = [];
            var _i = 0;
            for( var key in data.values) {
                // console.log(k);
                datasets.push({
                    label: key,
                    borderColor: colors[_i],
                    fill: false,
                    data: data.values[key]
                });
                _i ++;
            }
            chart.config.data.labels = months;
            chart.config.data.datasets = datasets;
            chart.update();
            console.log(datasets);
        }
    });
}


$(document).ready(function() {

    chart_dynamic();
    update_chart();
    $('#show').click(function(e) {
        update_chart();
    });

});
