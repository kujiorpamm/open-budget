$(document).ready(function(e){
    sendForm();

    $('#btn-chart').click(function(e) {
        sendForm();
    });

})

function sendForm() {
    $.ajax({
        url: 'get-data',
        type: 'POST',
        dataType: 'JSON',
        data: $('#form-chart').serializeArray(),
        success: function(data) {
            map_simple('#chart', data);
        }
    });
}


function map_simple(elem, data) {
    console.log(data);

    $('#date_1').html(data.date1);
    $('#date_2').html(data.date2);

    var width = parseInt($(elem).parent().width());
    var height = 500;
    var margin = {
	  top: 0,
	  right: 0,
	  bottom: 0,
	  left: 0
	};

    var svg = d3.select(elem)
        .attr("class", "chart map_simple")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.bottom + margin.top);

        // Map and projection
    var path = d3.geoPath();
    var projection ;
    // TODO: скейл сделать динамичным

    var symbolGenerator = d3.symbol()
    	.type(d3.symbolStar)
    	.size(80);
    var pathData = symbolGenerator();

    var domain_data = [];

    for(var index in data.datasets) {
        var value = data.datasets[index].data[0].x;
        domain_data.push(value);
    }
    function sortNumber(a,b) {
        return a - b;
    }
    domain_data.sort(sortNumber);

    var colorScale = d3.scaleThreshold()
        .domain(domain_data)
        .range(d3.schemeBlues[7]);
    d3.queue()
      .defer(d3.json, "/js/d3/map.geojson")
      .await(ready);

      var tool_tip = d3.tip()
        .attr("class", "d3-tip")
        .offset([-8, 0])
        .html(function(d) {
                // console.log(d)
                d = d.region_data;
                $('#region-data').html(`<h3>${d.z}</h3> <hr/> ${(d.n ? d.n : "")}`);
                // return d.label + "<br/>" + numberWithSpaces(d.x) + " " + d.measure;
                return ` ${d.label} <br/> ${d.y} <br/> ${numberWithSpaces(d.x)} ${d.measure}`;
            }
        );
      svg.call(tool_tip);

      function numberWithSpaces(x) {
            var parts = x.toString().split(".");
            parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, " ");
            return parts.join(".");
        }

    function getData(id) {
        for(var i = 0; i < data.datasets.length; i++) {
            if(data.datasets[i].data[0].z == id) {
                var region_data = data.datasets[i].data[0];
                region_data.label = data.datasets[i].label;
                region_data.measure = data.measure_y;
                return region_data;
            };
        }
        return {
            x :0,
            y: 0,
            z: id
        };
    }

    function appendRow(row) {
        return `<tr>
                    <td>${row.name}</td>
                    <td> 0 </td>
                    <td>${formatNumber(row.income_year)}</td>
                    <td>${formatNumber(row.income_month)}</td>
                    <td>${formatNumber(row.income_actual)}</td>
                    <td>${formatNumber(row.income_actual_percent)}</td>

                    <td>${formatNumber(row.sub_year)}</td>
                    <td>${formatNumber(row.sub_month)}</td>
                    <td>${formatNumber(row.sub_kasse)}</td>
                    <td>0</td>

                    <td>${formatNumber(row.outcome_year)}</td>
                    <td>${formatNumber(row.outcome_month)}</td>
                    <td>${formatNumber(row.outcome_actual)}</td>
                    <td>${formatNumber(row.outcome_actual_percent)}</td>

                </tr>`
    }

    function updateTable(d, data) {
        console.log(d, data);
        var _data = data.subreports;
        var selected = _data[d.properties.id];
        var last_row = "";
        $('#region-table').html("");
        for(var row in selected) {
            if(row == 0) {
                last_row = appendRow(selected[row]);
                continue;
            }
            $('#region-table').append(appendRow(selected[row]));
        }
        $('#region-table').append(last_row);
    }

    function ready(error, topo) {
        // var states = topojson.feature(topo, topo.features[0]);
        var tt = topo.features[0];
        topo.features.shift();
        var projection = d3.geoConicConformal()
            .parallels([0,0])
            .rotate([0, 0])
            .fitSize([width, height], tt);
        var path = d3.geoPath()
          .projection(projection);
        svg.append("g")
            .selectAll("path")
            .data(topo.features)
            .enter()
            .filter(function(d) { return d.geometry.type == "MultiPolygon" || d.geometry.type == "Polygon" ; })
                .append("path")
                .on('mouseover', function(d, b) {
                    d3.select(this).attr("fill", "rgb(107, 174, 214)");
                    return tool_tip.show(d);
                })
                .on('mouseout', function(d) {
                    d3.select(this).attr("fill", "rgb(158, 202, 225)");
                    return tool_tip.hide(d);
                })
                .on('click', function(d) { updateTable(d, data); })
                .attr("stroke-width", 1)
                .attr("stroke", "white")
                .attr("d", path)
                // set the color of each country
                .attr("fill", "rgb(158, 202, 225)");

            svg.append("g")
                .selectAll("text")
                .data(topo.features)
                .enter()
                .filter(function(d) {
                    var region_data = getData(d.properties.id) || {};
                    d.region_data = region_data;
                    return (d.geometry.type != "Point");
                  })
                .append("svg:text")
                .text(function(d){
                    return d.region_data.z;
                })
                .attr("x", function(d){
                    return path.centroid(d)[0];
                    // return projection(d.geometry.coordinates)[0];
                })
                .attr("y", function(d){
                    return  path.centroid(d)[1];
                    // return projection(d.geometry.coordinates)[1];
                })
                .attr("text-anchor","middle")
                .attr('font-size','9pt');

          svg.append("g")
              .selectAll("path")
              .data(topo.features)
              .enter()
              .filter(function(d) {
                  var region_data = getData(d.properties.id) || {};
                  d.region_data = region_data;
                  return (d.geometry.type == "Point");
                })
                .append('path')
                .attr('transform', function(d) {
            		return 'translate(' + projection(d.geometry.coordinates) + ')';
            	})
            	.attr('d', pathData)
                // .append("circle")
                .on('mouseover', tool_tip.show)
                .on('mouseout', tool_tip.hide)
                // .on('click', function(d) { updateTable(d, data); })
                // .attr("r", 8)
                // .attr("cx", function(d) { return projection(d.geometry.coordinates)[0]; })
                // .attr("cy", function(d) { return projection(d.geometry.coordinates)[1]; })
                .attr("fill", "red");

            svg.append("g")
                .selectAll("texts")
                .data(topo.features)
                .enter()
                .filter(function(d) {
                    var region_data = getData(d.properties.id) || {};
                    d.region_data = region_data;
                    return (d.geometry.type == "Point");
                  })
                .append("text")
                    .text(function(d) {
                        return d.region_data.z;
                    })
                    .attr("x", function(d) { return projection(d.geometry.coordinates)[0] + 16;  })
                    .attr("y", function(d) { return projection(d.geometry.coordinates)[1] + 2;  })
                    .attr("font-family", "sans-serif")
                    .attr("font-weight", "bold")
                    .attr("font-size", "12px")
                    .attr("fill", "red");
    }

}

function formatNumber(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
}
