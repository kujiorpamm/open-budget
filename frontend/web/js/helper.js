var backgroundColor = [
    'rgba(255, 99, 132, 0.4)',
    'rgba(255, 206, 86, 0.4)',
    'rgba(75, 192, 192, 0.4)',
    'rgba(153, 102, 255, 0.4)',
    'rgba(255, 159, 64, 0.4)',
    'rgba(54, 162, 235, 0.4)'
];

function addCommas(nStr)
{
	return nStr.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
}
