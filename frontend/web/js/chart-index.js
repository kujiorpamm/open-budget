var data = [];

$(document).ready(function(e) {
	// initData();
});

$(".btn-group > .btn").click(function(){
    $(".btn-group > .btn").removeClass("active");
    $(this).addClass("active");
});

function initData() {
	$.ajax({
		url: '/ajax/get-index-data',
		type: 'POST',
		data: {
			city_id: $('#form-city').val()
		},
		dataType: 'JSON',
		success: function(_data) {
			data = _data;
			setData();
			var col1 = $('.same-cols-height .data-container')[0], col2 = $('.same-cols-height .data-container')[1];
			if($(col1).height() > $(col2).height()) {
				$(col2).height($(col1).height());
			} else {
				$(col1).height($(col2).height());
			}
		}
	});

	// $('.data-wrapper').each(function(item) {
	// 	$(this).click(function(event) {
	// 		$(this).parent().toggleClass('active');
	// 	});
	// });

	$('#chart-selector').submit(function(event){
		initData();
		return false;
	});

}

//	max : 400px
function setData() {
	if(data.length > 0) {
		// heights
		$('#chart-1').css('min-height',  (400 * parseFloat(data[0][1]) / 100) + 'px' );
		$('#chart-2').css('min-height',  (400 * parseFloat(data[0][2]) / 100) + 'px' );
		$('#chart-3').css('min-height',  (400 * parseFloat(data[1][1]) / 100) + 'px' );
		$('#chart-4').css('min-height',  (400 * parseFloat(data[1][2]) / 100) + 'px' );

		//labesl
		$('#chart-1 .value').html(data[0][0]);
		$('#chart-1 .percent').html(data[0][1] + ' % <br> <span>Отчетный период</span>');
		$('#chart-2 .value').html(data[0][3]);
		$('#chart-2 .percent').html(data[0][2] + ' % <br> <span>Финансовый год</span>');
		//labesl
		$('#chart-3 .value').html(data[1][0]);
		$('#chart-3 .percent').html(data[1][1] + ' % <br> <span>Отчетный период</span>');
		$('#chart-4 .value').html(data[1][3]);
		$('#chart-4 .percent').html(data[1][2] + ' % <br> <span>Финансовый год</span>');

		$('#chart-date').html(data[2].date);
		$('#data-1 .d-container').html("");
		$('#data-2 .d-container').html("");
		// $('#chart-additional').html("");
		// console.log(  Object.keys(data[2].incomes_details).length);
		for(var i = 0; i < data[2].incomes_details.length; i++) {
			createItemRow('#data-1 .d-container', data[2].incomes_details[i]);
		}

		for(item in data[2].incomes_details) {
			createItemRow('#data-1 .d-container', data[2].incomes_details[item]);
		}
		for(item in data[2].outcomes_details) {
			createItemRow('#data-2 .d-container', data[2].outcomes_details[item]);
		}

	}

	// $('#chart-additional').html("");
	// for(var item in data[2].additional) {
	// 	createAdditionalRow('#chart-additional', data[2].additional[item]);
	// }

}

function createAdditionalRow(container, data) {
	if(!data) return;
	var html = [
			'<div class="item">',
				'<div class="row">',
					'<div class="col-xs-6 text">' + data.name + "</div>",
					'<div class="col-xs-6 value">' + data.model[0].budget_execution + " тыс. тг. </div>",
				'</div',
			'</div>'
	].join("\n");
	$(container).append(html);
}

function createItemRow(container, data) {
	var html = [
			'<div class="item">',
				'<div class="row">',
					'<div class="col-xs-6 text">' + data.name + "</div>",
					'<div class="col-xs-6 value">' + data.model[0].budget_execution_percent + "%, " + data.model[0].budget_execution + " тыс. тг </div>",
					'<div class="col-xs-12 line" style="width: ' + data.model[0].budget_execution_percent + '%"> </div>',
				'</div',
			'</div>'
	].join("\n");
	$(container).append(html);
}

// window.onload = function() {
// 	window.myBar = new Chart(document.getElementById('chart-1').getContext('2d'), {
// 		type: 'bar',
// 		data: {
//             labels: ['Доходы', 'Расходы'],
//             datasets: [
//                 {
//                     label: 'Доходы',
//                     data: [
//                         22, 43
//                     ]
//                 }
//             ]
//         },
// 		options: {
// 			responsive: true,
//             maintainAspectRatio: false,
// 			legend: false,
//             scales: {                                                           // zero - based
//                 yAxes: [{
//                     ticks: {
//                         beginAtZero: true
//                     }
//                 }]
//             },
//             tooltips: {                                                         // label at top
//                 enabled: true
//             },
//             hover: {
//                 animationDuration: 1
//             },
//             animation: {
//                     duration: 1,
//                     onComplete: function () {
//                         var chartInstance = this.chart,
//                             ctx = chartInstance.ctx;
//                         ctx.textAlign = 'center';
//                         ctx.fillStyle = "rgba(0, 0, 0, 1)";
//                         ctx.textBaseline = 'bottom';
//
//                         this.data.datasets.forEach(function (dataset, i) {
//                             var meta = chartInstance.controller.getDatasetMeta(i);
//                             meta.data.forEach(function (bar, index) {
//                                 var data = dataset.data[index] + ' млн';
//                                 ctx.fillText(data, bar._model.x, bar._model.y - 5);
//
//                             });
//                         });
//                     }
//                 }
// 		}
// 	});
//
//     window.myDoughnut = new Chart(document.getElementById('chart-2').getContext('2d'), {
// 			type: 'doughnut',
// 			data: {
// 				datasets: [{
// 					data: [
// 						22,
// 						240
// 					],
// 					backgroundColor: [
// 						'#ccc',
// 						'#999'
// 					],
// 					label: 'Dataset 1'
// 				}],
// 				labels: false
// 			},
// 			options: {
// 				responsive: true,
//                 maintainAspectRatio: false,
// 				legend: false,
// 				title: {
//                     display: true,
//                     text: ["Доходы", "224 тыс. тг"]
//                 },
// 				animation: {
// 					animateScale: true,
// 					animateRotate: true
// 				},
//                 elements: {
//                       center: {
//                           text: '18%',
//                           color: '#36A2EB', //Default black
//                           fontStyle: 'Helvetica', //Default Arial
//                           sidePadding: 15 //Default 20 (as a percentage)
//                       }
//                 }
// 			}
// 		}
//     );
//
//     window.myDoughnut = new Chart(document.getElementById('chart-3').getContext('2d'), {
// 			type: 'doughnut',
// 			data: {
// 				datasets: [{
// 					data: [
// 						24,
// 						210
// 					],
// 					backgroundColor: [
// 						'#ccc',
// 						'#999'
// 					],
// 					label: 'Dataset 1'
// 				}],
// 				labels: false
// 			},
// 			options: {
// 				responsive: true,
//                 maintainAspectRatio: false,
// 				legend: false,
// 				title: {
//                     display: true,
//                     text: ["Расходы", "224 тыс. тг"]
//                 },
// 				animation: {
// 					animateScale: true,
// 					animateRotate: true
// 				},
//                 elements: {
//                       center: {
//                           text: '28%',
//                           color: '#36A2EB', //Default black
//                           fontStyle: 'Helvetica', //Default Arial
//                           sidePadding: 15 //Default 20 (as a percentage)
//                       }
//                 }
// 			}
// 		}
//     );
//
// };
//
// Chart.pluginService.register({
//   beforeDraw: function (chart) {
//     if (chart.config.options.elements.center) {
//       //Get ctx from string
//       var ctx = chart.chart.ctx;
//
//       //Get options from the center object in options
//       var centerConfig = chart.config.options.elements.center;
//       var fontStyle = centerConfig.fontStyle || 'Arial';
//       var txt = centerConfig.text;
//       var color = centerConfig.color || '#000';
//       var sidePadding = centerConfig.sidePadding || 20;
//       var sidePaddingCalculated = (sidePadding/100) * (chart.innerRadius * 2)
//       //Start with a base font of 30px
//       ctx.font = "30px " + fontStyle;
//
//       //Get the width of the string and also the width of the element minus 10 to give it 5px side padding
//       var stringWidth = ctx.measureText(txt).width;
//       var elementWidth = (chart.innerRadius * 2) - sidePaddingCalculated;
//
//       // Find out how much the font can grow in width.
//       var widthRatio = elementWidth / stringWidth;
//       var newFontSize = Math.floor(30 * widthRatio);
//       var elementHeight = (chart.innerRadius * 2);
//
//       // Pick a new font size so it will not be larger than the height of label.
//       var fontSizeToUse = Math.min(newFontSize, elementHeight);
//
//       //Set font settings to draw it correctly.
//       ctx.textAlign = 'center';
//       ctx.textBaseline = 'middle';
//       var centerX = ((chart.chartArea.left + chart.chartArea.right) / 2);
//       var centerY = ((chart.chartArea.top + chart.chartArea.bottom) / 2);
//       ctx.font = fontSizeToUse+"px " + fontStyle;
//       ctx.fillStyle = color;
//
//       //Draw text in center
//       ctx.fillText(txt, centerX, centerY);
//     }
//   }
// });
