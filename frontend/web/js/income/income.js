var chart1;

$(document).ready(function(e) {

    // $('#btn-chart-1').on('click', function(e){
    //     getData();
    // });

    var ctx = document.getElementById("chart1");
    chart1 = new Chart(ctx, {
          type: 'pie',
          data: {
            labels: _labels,
            datasets: [{
                backgroundColor: backgroundColor,
                data: _values,
            }]
          },
          options: {
              legend: {
                  display: true,
                  position: 'right'
              },
              responsive: false
          }
        });
});
