<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class IndexAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/base.css',
        'css/main.less',
        'css/pages.less',
        '/vendor/layerslider/css/slider-pro.min.css',
    ];
    public $js = [
        '/vendor/layerslider/js/jquery.sliderPro.min.js',
        'vendor/chartsjs/Chart.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset'
    ];
}
