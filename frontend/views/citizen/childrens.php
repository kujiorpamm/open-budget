<?php

    use yii\helpers\Html;
    $this->registerJsFile('/vendor/video/video.min.js', ['depends' => frontend\assets\AppAsset::className()]);
    $this->registerCssFile('/vendor/video/video-js.css');

    $this->title = Yii::t('app', 'Бюджет для детей');
?>


<div class="page childrens">


    <div class="heading"><?=$this->title?></div>

    <video id="my-video" class="video-js vjs-default-skin vjs-16-9 vjs-big-play-centered" controls preload="auto" width="100%" height="264"
      data-setup="{}">
        <source src="/uploads/videos/childrens.mp4" type='video/mp4'>
        <p class="vjs-no-js">
          Чтобы посмотреть ролик, пожалуйста включите Javascrtipt в вашем браузере
          <a href="https://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
        </p>
      </video>


</div>
