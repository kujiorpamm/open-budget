<?php

    use yii\helpers\Html;

    $this->title = Yii::t('app', 'Бюджет для граждан');
    // $this->params['breadcrumbs'][] = $this->title;
?>


<div class="page citizen">


    <div class="heading"><?=$this->title?></div>

    <p>
        Расчет является примерным. При расчете не указываются льготы, установленные для отдельных категорий налогоплательщиков. Результат расчета может отличаться от данных налогового уведомления.
    </p> <br />

    <ul class="nav nav-tabs">
      <li class="active tab1"><a data-toggle="tab" href="#menu1">
          <center>
              <img src="/images/style/circle-77.png" /> <br /> <br /> Налоги на доходы физических лиц
          </center>
      </a></li>
      <li class="tab2" ><a data-toggle="tab" href="#menu2">
          <center>
              <img src="/images/style/circle-46.png" /> <br /> <br /> Семейные расходы
          </center>
      </a></li>
      <li class="tab3" ><a data-toggle="tab" href="#menu3">
          <center>
              <img src="/images/style/circle-ber.png" /> <br /> <br /> Пособие по беременности и родам
          </center>
      </a></li>
      <li class="tab4" ><a data-toggle="tab" href="#menu4">
          <center>
              <img src="/images/style/circle-car.png" /> <br /> <br /> Транспортный налог
          </center>
      </a></li>
    </ul>
    <br />
    <br />

    <div class="tab-content">

        <div id="menu1" class="tab-pane fade in active">

            <div class="row">
                <div class="col-sm-3">
                    <p>Метод расчета</p>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <?=Html::dropDownList('tt', null, ['Прямой', 'Обратный'], ['class'=>'form-control'])?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <p>Заработная плата</p>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <?=Html::textInput('tt', null,  ['class'=>'form-control', 'type' => 'number'])?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <p>Резидент РК</p>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <?=Html::checkBox("asd", true)?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <p>Вычет МЗП</p>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <?=Html::checkBox("asd", false)?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <p>Пенсионер</p>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <?=Html::checkBox("asd", false)?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <p>Инвалид</p>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <?=Html::checkBox("asd", false)?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <p>Суммка к уплате</p>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        0 тг.
                    </div>
                </div>
            </div>

        </div>

        <div id="menu2" class="tab-pane fade">

            <p>ДОХОДЫ</p> <hr />
            <div class="row">
                <div class="col-sm-3">
                    <p>Зарплата без налогов</p>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <?=Html::textInput('tt', null,  ['class'=>'form-control', 'type' => 'number'])?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <p>Пособие</p>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <?=Html::textInput('tt', null,  ['class'=>'form-control', 'type' => 'number'])?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <p>Итого в месяц</p>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <?=Html::textInput('tt', null,  ['class'=>'form-control', 'type' => 'number'])?>
                    </div>
                </div>
            </div>

            <p>РАСХОДЫ</p> <hr />
            <div class="row">
                <div class="col-sm-3">
                    <p>Продукты питания</p>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <?=Html::textInput('tt', null,  ['class'=>'form-control', 'type' => 'number'])?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <p>Коммунальные услуги</p>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <?=Html::textInput('tt', null,  ['class'=>'form-control', 'type' => 'number'])?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <p>Общественный транспорт</p>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <?=Html::textInput('tt', null,  ['class'=>'form-control', 'type' => 'number'])?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <p>Медицинские услуги</p>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <?=Html::textInput('tt', null,  ['class'=>'form-control', 'type' => 'number'])?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <p>Кредит на авто</p>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <?=Html::textInput('tt', null,  ['class'=>'form-control', 'type' => 'number'])?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <p>Другие кредиты</p>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <?=Html::textInput('tt', null,  ['class'=>'form-control', 'type' => 'number'])?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <p>Расходы на авто</p>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <?=Html::textInput('tt', null,  ['class'=>'form-control', 'type' => 'number'])?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <p>Страхование здоровья и жизни</p>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <?=Html::textInput('tt', null,  ['class'=>'form-control', 'type' => 'number'])?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <p>Занятия спортом</p>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <?=Html::textInput('tt', null,  ['class'=>'form-control', 'type' => 'number'])?>
                    </div>
                </div>
            </div>
            <hr />
            <div class="row">
                <div class="col-sm-3">
                    <p>Ваши расходы:</p>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        0 тг.
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <p>Ваши накопления:</p>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        0 тг.
                    </div>
                </div>
            </div>
        </div>

        <div id="menu3" class="tab-pane fade">
            <div class="row">
                <div class="col-sm-3">
                    <p>Занятость женщины</p>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <?=Html::checkBox("asd", false)?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <p>Какой по счету ребенок</p>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <?=Html::textInput('tt', null,  ['class'=>'form-control', 'type' => 'number'])?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <p>Среднемесячный доход за последние 24 месяца</p>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <?=Html::textInput('tt', null,  ['class'=>'form-control', 'type' => 'number'])?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <p>Среднемесячный доход за последние 12 месяцев</p>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <?=Html::textInput('tt', null,  ['class'=>'form-control', 'type' => 'number'])?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <p>Льготы для женщин и условия родов:</p>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <?=Html::dropDownList('tt', null, ['для нормально протекающих родов'], ['class'=>'form-control'])?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <p>Кол-во дней отпуска по беременности:</p>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <?=Html::textInput('tt', null,  ['class'=>'form-control', 'type' => 'number'])?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <p>Ваше пособие:</p>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        0 тг.
                    </div>
                </div>
            </div>
        </div>

        <div id="menu4" class="tab-pane fade">

            <div class="row">
                <div class="col-sm-3">
                    <p>Тип транспорта: </p>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <?=Html::dropDownList('tt', null, ['Легковой', 'Грузовой'], ['class'=>'form-control'])?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <p>Год выпуска: </p>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <?=Html::dropDownList('tt', null, ['2017', '2018'], ['class'=>'form-control'])?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <p>Месяц владения в налоговом периоде: </p>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <?=Html::dropDownList('tt', null, ['Янвварь', 'Февраль'], ['class'=>'form-control'])?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <p>Объем двигателя: </p>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <?=Html::dropDownList('tt', null, ['0,8', '1,5'], ['class'=>'form-control'])?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <p>Налог на транспорт:</p>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        0 тг.
                    </div>
                </div>
            </div>

        </div>

    </div>


</div>
