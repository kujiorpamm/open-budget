<?php
use yii\bootstrap\NavBar;
use yii\bootstrap\Nav;

$langs = Yii::$app->params['languages'];

$language = Yii::$app->language;
$js = <<<JS
    var language = '$language';
JS;

$this->registerJs($js, \yii\web\View::POS_HEAD);

?>

<?php if(Yii::$app->controller->id == 'site' && Yii::$app->controller->action->id == 'index'): ?>
    <!-- <div class="header-new"></div> -->
    <div class="slider-pro" id="index-slider">
        <div class="sp-slides">
        	<div class="sp-slide slide-1">
        		<img class="sp-image" src="/images/style/slide-1.jpg"/>
                <h3 class="sp-layer"
                    data-horizontal="50" data-vertical="150"
                    data-show-transition="right" data-show-delay="800">
                    <div class="slider-text">
                        <h4><?=Yii::t("app", "ОТ ИМЕНИ УПРАВЛЕНИЯ ФИНАНСОВ МАНГИСТАУСКОЙ ОБЛАСТИ");?></h4>
                        <p> <?=Yii::t("app", "Рад приветствовать Вас на новом информационном Портале «Открытый бюджет
                            Мангистауской области». Интернет-ресурс предназначен для обеспечения
                            прозрачности и открытости бюджетного процесса в нашем регионе. ");?></p>
                    </div>
                </h3>
        	</div>

            <div class="sp-slide">
                <img class="sp-image" src="/images/style/slide-2.jpg" />
                <div class="sp-layer"
                    data-horizontal="0" data-vertical="50"
                    data-show-transition="right" data-show-delay="200">
                    <a class="link" href="#"><?=Yii::t("app", "Общая информация");?></a>
                </div>
                <div class="sp-layer"
                    data-horizontal="0" data-vertical="100"
                    data-show-transition="right" data-show-delay="400">
                    <a class="link" href="#"><?=Yii::t("app", "«Понятие Бюджет»");?></a>
                </div>
                <div class="sp-layer"
                    data-horizontal="0" data-vertical="150"
                    data-show-transition="right" data-show-delay="600">
                    <a class="link" href="#"><?=Yii::t("app", "Бюджетная система РК");?></a>
                </div>
                <div class="sp-layer"
                    data-horizontal="0" data-vertical="200"
                    data-show-transition="right" data-show-delay="800">
                    <a class="link" href="#"><?=Yii::t("app", "Бюджетный процесс");?></a>
                </div>
            </div>

        </div>
    </div>
<?php endif; ?>
<div class="header">
    <?php
    NavBar::begin([ // отрываем виджет
      'brandLabel' => '<div class="logo">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <div class="image"><img src="/images/style/mangystau.png" /></div>
                                </div>
                                <div class="pull-right">
                                    <div class="text">
                                        <p>' . Yii::t('app', 'ОТКРЫТЫЙ БЮДЖЕТ') . '</p>
                                        <p>' . Yii::t('app', 'МАНГИСТАУСКОЙ ОБЛАСТИ') . '</p>
                                    </div>
                                </div>
                            </div>
                        </div>', // название организации
      'brandUrl' => Yii::$app->homeUrl, // ссылка на главную страницу сайта
      'options' => [
          'class' => 'navbar-inverse', // стили главной панели
      ],
  ]);
      echo Nav::widget([
          'options' => ['class' => 'navbar-nav navbar-right'], // стили ul
          'items' => [
              ['label' => Yii::t('app', 'О Бюджете'), 'items' => [
                    ['label' => Yii::t('app', 'Бюджет'), 'url' => ['/pages/about-budget']],
                    ['label' => Yii::t('app', 'Доходы'), 'url' => ['/pages/income']],
                    ['label' => Yii::t('app', 'Бюджетный процесс'), 'url' => ['/pages/budget-process']],
                    ['label' => Yii::t('app', 'Принципы бюджетной системы'), 'url' => ['/pages/budget-system']],
                    ['label' => Yii::t('app', 'Документы'), 'url' => ['/documents/index']],
                  ]],
              ['label' => 'Аналитика', 'items' => [
                    ['label' => Yii::t('app', 'Исполнение доходов'), 'url' => ['/analytics/income']],
                    ['label' => Yii::t('app', 'Исполнение расходов'), 'url' => ['/analytics/outcome']],
                    ['label' => Yii::t('app', 'Межбюджетные трансферты'), 'url' => ['/analytics/transferts']],
                    // ['label' => 'Помесячная динамика исполнения', 'url' => ['/analytics/dynamic-by-month']],
                    // ['label' => 'Государственные программы', 'url' => ['/analytics/gov-programs']],
                    ['label' => Yii::t('app', 'Бюджетно инвестиционные проекты'), 'url' => ['/analytics/invest']],
                  ]],
              // ['label' => 'Межбюджетные отношения', 'url' => ['/pages/interbudget']],
              ['label' => Yii::t('app', 'Областные показатели'), 'items' => [
                    ['label' => Yii::t('app', 'Межбюджетные отношения'), 'url' => ['/pages/interbudget']],
                    ['label' => Yii::t('app', 'Административно территориальные деления'), 'url' => ['/regional-indicators/index']],
                    ['label' => Yii::t('app', 'Областной бюджет'), 'url' => ['/regional-indicators/report']],
                  ]],
              ['label' => Yii::t('app', 'Бюджет для граждан'), 'items' => [
                    // ['label' => 'Калькулятор', 'url' => ['/citizen/index']],
                    ['label' => Yii::t('app', 'Для детей'), 'url' => ['/citizen/childrens']],
                    ['label' => Yii::t('app', 'Глоссарий'), 'url' => ['/pages/glossary']],
                  ]],
            ['label' => array_search(Yii::$app->language, Yii::$app->params['languages']), 'items' => [
                    ['label' => 'Қаз', 'url' => ['/languages?lang=kk']],
                    ['label' => 'Рус', 'url' => ['/languages?lang=ru']]
                ]]
          ],
      ]);
  NavBar::end(); // закрываем виджет
    ?>

</div>
