<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">

    <?=$this->render('_menu')?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <div class="row">
            <div class="col-sm-3">
                <ul class="nav nav-pills nav-stacked">
                  <li class="<?=(Yii::$app->controller->action->id == 'about-budget')?'active':''?>"><a href="/pages/about-budget"><?=Yii::t("app", "О Бюджете");?></a></li>
                  <li class="<?=(Yii::$app->controller->action->id == 'income')?'active':''?>" ><a href="/pages/income"><?=Yii::t("app", "Доходы");?></a></li>
                  <li class="<?=(Yii::$app->controller->action->id == 'budget-process')?'active':''?>"><a href="/pages/budget-process"><?=Yii::t("app", "Бюджетный процесс");?></a></li>
                </ul>
            </div>
            <div class="col-sm-9"><?= $content ?></div>
        </div>
    </div>

</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; <?= Html::encode(Yii::$app->name) ?> <?= date('Y') ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
