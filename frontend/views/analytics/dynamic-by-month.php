<?php

    use yii\helpers\Html;
    $this->title = Yii::t('app', 'ПОМЕСЯЧНАЯ ДИНАМИКА ИСПОЛНЕНИЯ БЮДЖЕТА ПО ОТРАСЛЯМ');
    // $this->params['breadcrumbs'][] = $this->title;
?>

<div class="page glossary">


    <div class="heading"><?=$this->title?></div>

    <div class="row">
        <div class="col-sm-2">
            <div class="form-group">
                <label for="tt">Год: </label>
                <?=Html::dropDownList('tt', '', [2018, 2017, 2016], ['class' => 'form-control'])?>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="form-group">
                <label for="tt">Период:</label>
                <?=Html::dropDownList('tt', '', ['Январь - Декабрь'], ['class' => 'form-control'])?>
            </div>
        </div>
        <div class="col-sm-3 col-sm-offset-4">
            <div class="form-group">
                <label for="tt">Вид бюджета</label>
                <?=Html::dropDownList('tt', '', ['Местный', 'Республиканский','Местный/Республиканский'], ['class' => 'form-control'])?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-5">
            <div class="form-group">
                <label for="tt">Бюджет: </label>
                <?=Html::dropDownList('tt', '', ['Областной (включая районы и города) '], ['class' => 'form-control'])?>
            </div>
        </div>
        <div class="col-sm-3 col-sm-offset-4">
            <div class="form-group">
                <label for="tt"></label>
                <?=Html::dropDownList('tt', '', ['Тыс. тг.', 'Млн. тг.'], ['class' => 'form-control'])?>
            </div>
        </div>
    </div>

    <center>
        <img class="img img-responsive" src="/images/inc.jpg"/ style="max-width: 900px; padding: 40px 0;">
    </center>
    <center>
        <img class="img img-responsive" src="/images/inc2.jpg"/>
    </center>

</div>
