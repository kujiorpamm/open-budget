<?php
    $this->title = "Отчет об исполнении бюджета: {$model->city->name} ," .  Yii::$app->formatter->asDate($model->date, 'long');
    $this->registerJsFile('/js/treeview.js');
    $this->registerJsFile('/js/barchart.js');
    $this->registerJs("initTreeView({url: '/ajax/get-report?id={$model->id}'}, 'root')");
    $script = <<< JS
        var _bc = new BarChart();
        _bc.init('/ajax/get-report-bar?id={$model->id}', 'bc_root');
JS;
    $this->registerJs($script);
?>

<div class="analytics-view-report">
    <div class="block">
        <h3><?=$this->title?></h3>


        <!-- <h3><?=Yii::t('app', 'Табличные данные')?> <hr /> </h3> -->
        <div id="bc_root"></div>

        <h3><?=Yii::t('app', 'Табличные данные')?> <hr /> </h3>
        <div id="root"></div>

    </div>
</div>
