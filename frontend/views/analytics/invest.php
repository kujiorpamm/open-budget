<?php

    $this->registerJsFile('https://api-maps.yandex.ru/2.1/?lang=ru_RU&load=Map,Placemark,geoObject.addon.balloon', ['depends' => [\yii\web\JqueryAsset::className()]]);
    $this->registerJsFile('/js/yandex_map.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

    $this->title = Yii::t('app', 'Бюджетно инвестиционные проекты');
    // $this->params['breadcrumbs'][] = $this->title;
?>


<div class="page invest">

    <img style="margin-top: -20px;" class="img img-responsive" src="/images/style/top.jpg" />

    <div class="container">

        <div class="heading"><?=$this->title?></div>

        <ul class="nav nav-tabs">
          <li class="active tab1"><a data-toggle="tab" href="#menu1"> <i class="glyphicon glyphicon-th-list"></i> Перечень бюджетно инвестиционных проектов</a></li>
          <li class="tab2" ><a data-toggle="tab" href="#menu2"> <i class="glyphicon glyphicon-map-marker"></i> Карта региона</a></li>
        </ul>

        <div class="tab-content">
            <div id="menu1" class="tab-pane fade in active">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Наименование проекта</th>
                            <th>Мощность проекта</th>
                            <th>План, 2018 год (тыс.тенге)</th>
                            <th>Факт, 2018 год (тыс.тенге)</th>
                            <th>Срок сдачи (год ввода в эксплуатацию)</th>
                            <th>Подрядчик</th>
                            <th>Месторасположение проекта</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>«Привязка типового проекта «Средняя общеобразовательная школа на 600 учащихся для IVA, IVГ климатических подрайонов с обычными геологическими условиями»</td>
                            <td>600 мест</td>
                            <td>476 021</td>
                            <td>425 282</td>
                            <td>2018 год</td>
                            <td>ТОО "Қараойқұрылыс"</td>
                            <td>Мунайлинский район, сельски округ Баскудык</td>
                        </tr>
                        <tr>
                            <td>Строительство сетей водоснабжения</td>
                            <td>протяженность 5,3 км</td>
                            <td>725 000</td>
                            <td>725 000</td>
                            <td>2018 год</td>
                            <td>ТОО "Альянс ЛТД"</td>
                            <td>Мунайлинского района сельский округ Баскудук</td>
                        </tr>
                        <tr>
                            <td>Реконструкция автомобильной дороги "Киякты-Тущыкудук"</td>
                            <td>протяженность 9 км</td>
                            <td>247 094</td>
                            <td>71 262</td>
                            <td>2018 год</td>
                            <td>ТОО "Ақ жол құрылыс"</td>
                            <td>Мангистаукий район</td>
                        </tr>
                        <tr>
                            <td>«Привязка типового проекта «ТП РК 900 СОШ (IVA, IVГ) -2.2-2012 средняя общеобразовательная школа на 900 учащихся для IVA и IVГ климатических подрайонов с обычными геологическими условиями» в 32 «А» </td>
                            <td>900 мест</td>
                            <td>462 866</td>
                            <td>462 865,6</td>
                            <td>2018 год</td>
                            <td>ТОО "АктауСаулетКурылыс"</td>
                            <td>Город Актау</td>
                        </tr>
                        <tr>
                            <td>Строительство внутримирорайонных инженерных сетей (водоснабжение, канализация) и автомобильных дорог, проездных зон, тротуаров, благоустройство придорожных территорий в 17 мкр. - 2 очередь</td>
                            <td>протяженность автодороги - 3832,46м; <br />тепловой сети - 5002м; <br /> питьевого водопровода - 7116м; <br />технического водопровода - 5050м;</td>
                            <td>1 280 578</td>
                            <td>647 184</td>
                            <td>2018 год</td>
                            <td>ТОО "Шебер курылыс"</td>
                            <td>Город Актау</td>
                        </tr>
                        <tr>
                            <td>Строительство водоочистного сооружения и внутрипоселкового водопровода </td>
                            <td>протяженность внутрипоселковой сети - 47005 м</td>
                            <td>110 000</td>
                            <td>10 000</td>
                            <td>2018 год</td>
                            <td>ТОО "Шебер курылыс"</td>
                            <td>Бейнеускмй район</td>
                        </tr>
                        <tr>
                            <td>Строительство 4-х этажного арендного жилого дома             1-очередь</td>
                            <td>одного четырехэтажного 24-квартирного жилого дома</td>
                            <td>151 423</td>
                            <td>52 856</td>
                            <td>2018 год</td>
                            <td>ТОО "АС Құрылыс"</td>
                            <td>Каракиянский район</td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div id="menu2" class="tab-pane fade">

                <div class="row">
                    <div class="col-sm-3">
                        <div id="mapitems">

                        </div>
                    </div>
                    <div class="col-sm-9">
                        <div id="map" style="width: 100%; height: 600px"></div>
                    </div>
                </div>

            </div>
        </div>

    </div>


</div>
