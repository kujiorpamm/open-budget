<?php

use yii\helpers\Html ;
use yii\widgets\ActiveForm ;


$this->registerJsFile('/js/d3/d3.v4.min.js', ['depends' => frontend\assets\AppAsset::className()]);
$this->registerJsFile('/js/d3/d3.tip.js', ['depends' => frontend\assets\AppAsset::className()]);
// $this->registerJsFile('/js/Chart.bundle.min.js', ['depends' => frontend\assets\AppAsset::className()]);
// $this->registerJsFile('/js/chartjs-plugin-piechart-outlabels.js', ['depends' => frontend\assets\AppAsset::className()]);
// $this->registerJsFile('/js/chartjs-plugin-colorschemes.min.js', ['depends' => frontend\assets\AppAsset::className()]);
$this->registerJsFile('/js/analytics/income.js', ['depends' => frontend\assets\AppAsset::className()]);

?>

<div class="page-income block">

    <div class="block block-1">
        <div class="container">

            <h3><?=Yii::t('app', 'ИСПОЛНЕНИE ДОХОДОВ')?></h3>

            <div class="text-block">
                <p><?=Yii::t('app', 'На странице приводятся показатели доходов бюджета за два прошлых года и текущий финансовый год. Динамика приводится в разрезе показателей доходов. На круговой диаграмме доступен просмотр за текущий финансовый год. На гистограмме приводится сравнение показателей за два прошлых года и текущий финансовый год. В таблице более детализировано, представлены показатели за аналогичный гистограмме период.')?></p>
                <p><?=Yii::t('app', 'Для показателей доходов бюджета доступен просмотр уровня бюджета: «Областной бюджет», «Бюджет области», «Бюджет районов и городов». Данные доступны за последние три года.')?></p>
                <p><?=Yii::t('app', 'При наведении на элементы инфографики, в сплывающем «окне» отображается дополнительная информация по выбранному показателю.')?></p>
                <p><?=Yii::t('app', 'При клике на легенду к круговой диаграмме и гистограмме, выбранный показатель отключается на инфографике.')?></p>
                <p><?=Yii::t('app', 'При выборке периода, показатели в аналитическом отчете изменяются на принятые показатели с учетом уточнений данного периода.')?></p>
            </div>

            <h3><?=Yii::t('app', 'ИСПОЛНЕНИЕ ПОКАЗАТЕЛЕЙ ДОХОДОВ БЮДЖЕТА')?></h3>

            <?php $form = ActiveForm::begin([
                'id' => 'form-chart'
                ])?>

                <div class="row">
                    <div class="col-sm-3">
                        <?=$form->field($model, 'city')->dropDownList($cities)->label(Yii::t('app', 'Тип'))?>
                    </div>
                    <div class="col-sm-4">
                        <div class="row">
                            <div class="col-xs-6">
                                <?=$form->field($model, 'year')->dropDownList($years)->label(Yii::t('app', 'Период'))?>
                            </div>
                            <div class="col-xs-6">
                                <?=$form->field($model, 'month')->dropDownList([
                                    '01' => Yii::t('app', 'Январь'),
                                    '02' => Yii::t('app', 'Февраль'),
                                    '03' => Yii::t('app', 'Март'),
                                    '04' => Yii::t('app', 'Апрель'),
                                    '05' => Yii::t('app', 'Май'),
                                    '06' => Yii::t('app', 'Июнь'),
                                    '07' => Yii::t('app', 'Июль'),
                                    '08' => Yii::t('app', 'Август'),
                                    '09' => Yii::t('app', 'Сентябрь'),
                                    '10' => Yii::t('app', 'Октябрь'),
                                    '11' => Yii::t('app', 'Ноябрь'),
                                    '12' => Yii::t('app', 'Декабрь')
                                ])->label('&nbsp;')?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <?=$form->field($model, 'coeficient')->dropDownList(['1' => 'тыс.тг', '0.001'  => 'млн.тг'])->label('&nbsp;')?>
                    </div>
                    <div class="col-sm-3">
                        <label class="control-label">&nbsp;</label> <br />
                        <button id="btn-chart" type="button" class="btn btn-success"><?=Yii::t("app", "Показать");?></button>
                    </div>
                </div>

            <?php ActiveForm::end()?>

            <div class="datetime"></div>
            <h3 class="center"><?=Yii::t('app', 'СТРУКТУРА ДОХОДОВ БЮДЖЕТА')?></h3>
            <div id="income-total" class="center"><?=Yii::t('app', 'Всего доходов: {0}', [0])?></div>
            <div id="pie"></div>


            <h3 class="center"><?=Yii::t('app', 'ДИНАМИКА ИСПОЛНЕНИЯ ПОКАЗАТЕЛЕЙ ДОХОДОВ БЮДЖЕТА')?></h3>
            <div id="stacked">
                <svg></svg>
            </div>

            <h3 class="center"><?=Yii::t('app', 'ДИНАМИКА ИСПОЛНЕНИЯ ПОКАЗАТЕЛЕЙ ДОХОДОВ БЮДЖЕТА')?></h3>

            <?=Html::checkBoxList('tempo', '', [Yii::t('app', 'Темп роста к предыдущему году')])?>

            <table class="table table-bordered">
                <thead id="table-head">

                </thead>
                <tbody id="table">

                </tbody>
            </table>




        </div>
    </div>

</div>
