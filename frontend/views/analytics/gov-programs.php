<?php

    $this->title = "Государственные программы финансирования";

    $this->registerJsFile('/vendor/vis/vis.js', ['depends' => frontend\assets\AppAsset::className()]);
    $this->registerCssFile('/vendor/vis/vis-network.min.css');
    $__data = $model->shortTree();
    $data = json_encode($__data[2]);

        // echo "<pre>"; print_r( ($model->shortTree())[2]); echo "</pre>"; exit;
    $js = <<< JS
        var chart_data = $data;
JS;

    $this->registerJs($js, $this::POS_END);
    $this->registerJsFile('/js/charts/gov-programs.js', ['depends' => frontend\assets\AppAsset::className()]);
?>

<div class="gov-programs">

    <h3><?=$this->title?></h3>
    <h4 style="margin-bottom: 40px;"><?=Yii::t('app', 'По состоянию на {0}, Мангистауская область', [ Yii::$app->formatter->asDate($model->date, 'php:d.m.Y') ])?></h4>

        <div class="btn-group" role="group" aria-label="val">
          <button type="button" class="btn btn-default active"><?=Yii::t('app', 'тыс. тг')?></button>
          <button type="button" class="btn btn-default"><?=Yii::t('app', 'млн. тг')?></button>
        </div>

        <br />
        <br />

        <table class="table">
            <thead>
                <tr>
                    <th colspan="2">Наименование</th>
                    <th>Годовой план</th>
                    <th>План на отчетный период</th>
                    <th>Кассовое исполнение</th>
                    <th>%</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><img src="/images/style/circle-33.png" /></td>
                    <td> Государственная программа развития образования Республики Казахстан на 2011-2020 годы </td>
                    <td>21 520 709</td>
                    <td>6 301 693</td>
                    <td>5 807 227</td>
                    <td>92</td>
                </tr>
                <tr>
                    <td><img src="/images/style/circle-39.png" /></td>
                    <td>Государственная программа развития здравоохранения Республики Казахстан «Денсаулык» на 2016-2020 годы</td>
                    <td>4 037 385</td>
                    <td>1 772 819</td>
                    <td>1 654 945</td>
                    <td>93</td>
                </tr>
                <tr>
                    <td><img src="/images/style/circle-75.png" /></td>
                    <td>Программы по развитию агропромышленного комплекса в Республике Казахстан на 2013 – 2020 годы «Агробизнес – 2020»</td>
                    <td>1 062 557</td>
                    <td>177 077</td>
                    <td>177 074</td>
                    <td>100</td>
                </tr>
                <tr>
                    <td><img src="/images/style/circle-46.png" /></td>
                    <td>Программы развития продуктивной занятости и массового предпринимательства</td>
                    <td>1 585 632</td>
                    <td>398 084</td>
                    <td>383 047</td>
                    <td>96</td>
                </tr>
                <tr>
                    <td><img src="/images/style/circle-71.png" /></td>
                    <td>Дорожная карта бизнеса – 2020</td>
                    <td>1 055 765</td>
                    <td>1 005 765</td>
                    <td>1 005 765</td>
                    <td>100</td>
                </tr>
                <tr>
                    <td><img src="/images/style/circle-2.png" /></td>
                    <td>Программа развития регионов до 2020 года</td>
                    <td>24 116 088</td>
                    <td>5 108 801</td>
                    <td>5 186 718</td>
                    <td>102</td>
                </tr>
                <tr>
                    <td><img src="/images/style/circle-68.png" /></td>
                    <td>Программа развития и интеграции инфраструктуры транспортной системы РК до 2020 года</td>
                    <td>5 952 474</td>
                    <td>1 873 041</td>
                    <td>1 794 106</td>
                    <td>96</td>
                </tr>
                <tr>
                    <td><img src="/images/style/circle-23.png" /></td>
                    <td>Программа развития Мангистауской области на 2016-2020 годы</td>
                    <td>4 837 116</td>
                    <td>1 691 262</td>
                    <td>1 461 570</td>
                    <td>86</td>
                </tr>
            </tbody>
        </table>

    <div style="width: 100%; height: 500px; display: none;" id="mynetwork"></div>

</div>
