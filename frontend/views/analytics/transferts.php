<?php

use yii\helpers\Html ;
use yii\widgets\ActiveForm ;


$this->registerJsFile('/js/d3/d3.v4.min.js', ['depends' => frontend\assets\AppAsset::className()]);
$this->registerJsFile('/js/d3/d3.tip.js', ['depends' => frontend\assets\AppAsset::className()]);
// $this->registerJsFile('/js/Chart.bundle.min.js', ['depends' => frontend\assets\AppAsset::className()]);
// $this->registerJsFile('/js/chartjs-plugin-piechart-outlabels.js', ['depends' => frontend\assets\AppAsset::className()]);
// $this->registerJsFile('/js/chartjs-plugin-colorschemes.min.js', ['depends' => frontend\assets\AppAsset::className()]);
$this->registerJsFile('/js/analytics/income.js', ['depends' => frontend\assets\AppAsset::className()]);

?>

<div class="page-income block">

    <div class="block block-1">
        <div class="container">

            <h3><?=Yii::t('app', 'Межбюджетные трансферты')?></h3>

            <div class="text-block">
                <p><?=Yii::t('app', 'На странице приводятся показатели по межбюджетным трансфертам за текущий финансовый год и за плановый период. Динамика приводится в разрезе показателей по межбюджетным трансфертам. На круговой диаграмме доступен просмотр за текущий период. На гистограмме приводится сравнение показателей за два прошлых года и текущий финансовый год. В таблице более детализировано, представлены показатели за аналогичный гистограмме период.')?></p>
                <p><?=Yii::t('app', 'При наведении на элементы инфографики, в сплывающем «окне» отображается дополнительная информация по выбранному показателю.')?></p>
                <p><?=Yii::t('app', 'При клике на легенду к круговой диаграмме и гистограмме, выбранный показатель отключается на инфографике.')?></p>
                <p><?=Yii::t('app', 'При выборке периода, показатели в аналитическом отчете изменяются на принятые показатели с учетом уточнений данного периода.')?></p>
            </div>

            <h3><?=Yii::t('app', 'ПОКАЗАТЕЛИ МЕЖБЮДЖЕТНЫХ ТРАНСФЕРТОВ')?></h3>

            <?php $form = ActiveForm::begin([
                'id' => 'form-chart'
                ])?>

                <div class="row">
                    <div class="col-sm-3">
                        <?=$form->field($model, 'city')->dropDownList($cities)->label('Тип')?>
                    </div>
                    <div class="col-sm-4">
                        <div class="row">
                            <div class="col-xs-6">
                                <?=$form->field($model, 'year')->dropDownList($years)->label('Период')?>
                            </div>
                            <div class="col-xs-6">
                                <?=$form->field($model, 'month')->dropDownList([
                                    '01' => 'Январь',
                                    '02' => 'Февраль',
                                    '03' => 'Март',
                                    '04' => 'Апрель',
                                    '05' => 'Май',
                                    '06' => 'Июнь',
                                    '07' => 'Июль',
                                    '08' => 'Август',
                                    '09' => 'Сентябрь',
                                    '10' => 'Октябрь',
                                    '11' => 'Ноябрь',
                                    '12' => 'Декабрь'
                                ])->label('&nbsp;')?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <?=$form->field($model, 'coeficient')->dropDownList(['1' => 'тыс.тг', '0.001'  => 'млн.тг'])->label('&nbsp;')?>
                    </div>
                    <div class="col-sm-3">
                        <label class="control-label">&nbsp;</label> <br />
                        <button id="btn-chart" type="button" class="btn btn-success">Показать</button>
                    </div>
                </div>

            <?php ActiveForm::end()?>

            <div class="datetime"></div>
            <h3 class="center"><?=Yii::t('app', 'СТРУКТУРА МЕЖБЮДЖЕТНЫХ ТРАНСФЕРТОВ')?></h3>
            <div id="income-total" class="center"><?=Yii::t('app', 'Итого трансферты: {0}', [0])?></div>
            <div id="pie"></div>


            <h3 class="center"><?=Yii::t('app', 'ДИНАМИКА ПОКАЗАТЕЛЕЙ МЕЖБЮДЖЕТНЫХ ТРАНСФЕРТОВ')?></h3>
            <div id="stacked">
                <svg></svg>
            </div>

            <h3 class="center"><?=Yii::t('app', 'ДИНАМИКА ПОКАЗАТЕЛЕЙ МЕЖБЮДЖЕТНЫХ ТРАНСФЕРТОВ')?></h3>

            <?=Html::checkBoxList('tempo', '', [Yii::t('app', 'Темп роста к предыдущему году')])?>

            <table class="table table-bordered">
                <thead id="table-head">

                </thead>
                <tbody id="table">

                </tbody>
            </table>




        </div>
    </div>

</div>
