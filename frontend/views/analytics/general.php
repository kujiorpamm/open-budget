<?php
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
    use kartik\date\DatePicker;
    use kartik\file\FileInput;

    $this->title = Yii::t('app', 'Основные показатели бюджета');
    $this->params['breadcrumbs'][] = $this->title;

?>


<h1><?=Yii::t('app', '')?></h1>

<div class="page analytics-general">


    <div class="heading center"><?=$this->title?></div>

    <!-- Поиск -->
    <div class="block-1">
        <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>
        <div class="row">
            <div class="col-sm-3">

                <?=$form->field($model, 'date')->widget(DatePicker::className(), [
                    'options' => ['placeholder' => 'Укажите дату'],
                    'language' => 'ru',
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'viewMode' => 'months',
                        'minViewMode' => 'months',
                    ]
                ])->label(false)?>

            </div>

            <div class="col-sm-3 col-sm-offset-1">
                <div class="form-field">
                    <?=$form->field($model, 'city_id')->dropDownList($cities)->label(false)?>
                </div>
            </div>

            <div class="col-sm-3">
                <div class="form-field">
                    <?=$form->field($model, 'type_id')->dropDownList($types)->label(false)?>
                </div>
            </div>

            <div class="col-sm-2">
                <?= Html::submitButton(Yii::t('app', 'Поиск'), ['class' => 'btn btn-success']) ?>
            </div>


        </div>
        <?php ActiveForm::end(); ?>

    </div>

    <?php if($model->id) : ?>

        <div class="row">
            <div class="col-sm-6">
                Установлено: <?=Yii::$app->formatter->asDecimal($model->incomes->budget_corrected)?><br />
                Исполнено: <?=Yii::$app->formatter->asDecimal($model->incomes->budget_execution)?><br />
                Исполнено % месяц: <?=$model->incomes->budget_execution_percent?> <br />
                Исполнено % год: <?=$model->incomes->budget_execution_percent_total?> <br />
            </div>
            <div class="col-sm-6">
                Установлено: <?=Yii::$app->formatter->asDecimal($model->outcomes->budget_corrected)?><br />
                Исполнено: <?=Yii::$app->formatter->asDecimal($model->outcomes->budget_execution)?><br />
                Исполнено % месяц: <?=$model->outcomes->budget_execution_percent?> <br />
                Исполнено % год: <?=$model->outcomes->budget_execution_percent_total?> <br />
            </div>
        </div>

    <?php else: ?>
        <div class="not-found"> Отчет не найден </div>
    <?php endif; ?>

</div>
