<?php

use yii\helpers\Html ;
use yii\widgets\ActiveForm ;
$this->title = Yii::t('app', 'Динамика поступлений в бюджет');
$this->registerJsFile('/js/Chart.bundle.min.js', ['depends' => frontend\assets\AppAsset::className()]);
$this->registerJsFile('/js/chartjs-plugin-datalabels.min.js', ['depends' => frontend\assets\AppAsset::className()]);
$this->registerJsFile('/js/charts/chart-dynamic-incomes.js', ['depends' => frontend\assets\AppAsset::className()]);
$this->registerJsFile('/js/treeview-income.js', ['depends' => frontend\assets\AppAsset::className()]);

$scr = <<< JS
    initTreeView({
        url: '/ajax/treeview-income'
    }, 'root');
    initTreeView({
        url: '/ajax/treeview-outcome'
    }, 'root2');


    $('#root2').hide();

    $(document).ready(function() {
        $('#report_id').change(function(e){
            var val = $(this).val();

            if(val == 1) {
                $('#root').show();
                $('#root2').hide();
            } else {
                $('#root2').show();
                $('#root').hide();
            }

            console.log($(this).val());
        });
    });


JS;

$this->registerJs(
$scr
);
?>

<div class="analytics">
    <div class="dynamics" style="min-height: 500px; width: 100%;">

        <?=Html::beginForm(['/ajax/dynamics-income'], 'post', ['class' => 'form-inline'])?>



            <div class="row" style="padding-bottom: 40px;">
                <div class="col-sm-9">

                    <div>
                        <div class="form-group">
                            <?=Html::dropDownList('report', 0, $reports, ['class'=>'form-control', 'id'=>'report_id'])?>
                        </div>

                        <div class="form-group">
                            <?=Html::dropDownList('city_id', 0, $cities, ['class'=>'form-control', 'id'=>'city_id'])?>
                        </div>

                        <!-- <div class="form-group">
                            <?=Html::dropDownList('type_id', 0, $types, ['class'=>'form-control', 'id'=>'type_id'])?>
                        </div> -->

                        <div class="form-group">
                            <?=Html::dropDownList('multiplier', 0, $multipliers, ['class'=>'form-control', 'id'=>'multiplier'])?>
                        </div>
                    </div>

                    <div>

                        <div class="form-group">
                            <?=Yii::t('app', 'Период: ')?>
                        </div>
                        <div class="form-group">
                            <?=Html::dropDownList('date_from', 0, $months, ['class'=>'form-control', 'id'=>'date_from'])?>
                        </div>
                        <div class="form-group">
                            <?=Yii::t('app', ' - ')?>
                        </div>
                        <div class="form-group">
                            <?=Html::dropDownList('date_to', 12, $months, ['class'=>'form-control', 'id'=>'date_to'])?>
                        </div>
                    </div>

                </div>
                <div class="col-sm-3">
                    <?=Html::button('Показать', ['class' => 'btn btn-success btn-block', 'id' => 'show'])?>
                </div>
            </div>
        <?=Html::endForm()?>

        <canvas id="dynamic-income"></canvas>
    </div>


    <div style="padding-top: 40px; padding-bottom: 90px; min-height: 400px;" id="root"></div>
    <div style="padding-top: 40px; padding-bottom: 90px; min-height: 400px;" id="root2"></div>

</div>
