<?=Yii::t('app', 'По состоянию на {0}, Мангистауская область', [ Yii::$app->formatter->asDate($model->date, 'php:d.m.Y') ])?>

<?php
    $data = $model->shortTree();
    // echo "<pre>"; print_r($data); echo "</pre>"; exit;
    // echo "<pre>"; print_r($data[2]); echo "</pre>"; exit;
    // foreach ($data[2]['children'] as $key => $value) {
    //
    //     echo $value['name'] . " <br />";
    // }

?>

<div class="gov-programs">

<ul class="govprogram-tree">

<?php foreach ($data[2]['children'] as $item): ?>
    <li class="active">
        <img src="/images/style/test.png" />
        <?=$item['name'] . ". Назначено: " . round($item['model']['budget_corrected'] * 0.001, 1) . " млн. тг. / Исполнено: " . round($item['model']['budget_execution'] * 0.001) . " млн. тг." ?>
        <?php if($item['model']): ?>
            <!-- <ul> -->
                <table class="table">
                    <thead>
                        <tr>
                            <th>Наименование</th>
                            <th>Запланировано на конец года</th>
                            <th>Исполнено на текущий период</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($item['children'] as $item2): ?>
                            <!-- <li> -->
                                    <tr>
                                        <td><?=$item2['name']?></td>
                                        <td><?=round($item2['model']['budget_corrected'] * 0.001, 1)?></td>
                                        <td><?=round($item2['model']['budget_execution'] * 0.001)?></td>
                                    </tr>
                            <!-- </li> -->
                        <?php endforeach; ?>
                    </tbody>
                </table>
            <!-- </ul> -->
        <?php endif; ?>
    </li>
<?php endforeach; ?>

</ul>

</div>
