<?php

use yii\helpers\Html;

?>
<h1><?=Yii::t('app', 'Документы')?></h1>

<div class="page documents">
    <?php foreach ($types as $type) : ?>
        <!-- <h3><?=$type->name?></h3> -->
        <div class="panel panel-default">
            <div class="panel-heading"><?=$type->name?></div>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th><?=Yii::t('app', 'Наименование')?></th>
                        <th><?=Yii::t('app', 'Ссылка')?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($type->documentsByLanguage as $item) : ?>
                        <tr>
                            <td><?=$item->name?></td>
                            <td><?=Html::a("<i class='glyphicon glyphicon-link'>Скачать</i>", $item->path, ['target' => '_blank'])?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    <?php endforeach; ?>
</div>
