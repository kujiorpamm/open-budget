<?php

    use yii\helpers\Html ;
    use yii\widgets\ActiveForm ;

    $this->title = Yii::t('app', 'Административно территориальные деления');
    $this->registerJsFile('/js/d3/d3.v4.min.js', ['depends' => frontend\assets\AppAsset::className()]);
    $this->registerJsFile('/js/d3/d3.tip.js', ['depends' => frontend\assets\AppAsset::className()]);
    $this->registerJsFile('/js/d3/d3-scale-chromatic.v1.min.js', ['depends' => frontend\assets\AppAsset::className()]);
    $this->registerJsFile('/js/d3/topojson.v2.min.js', ['depends' => frontend\assets\AppAsset::className()]);
    $this->registerJsFile('/js/regional-indicators/index.js', ['depends' => frontend\assets\AppAsset::className()]);
?>

<div class="regional-indicators block">

    <div class="block block-1">
        <div class="container">
            <h3><?=$this->title?></h3>
            <div class="text-block">
                <p><?=Yii::t('app', 'На странице приводятся показатели поступлений и расходов бюджета за текущий финансовый год. На карте региона доступен просмотр поступлений и расходов бюджета за отчетный период в разрезе районов и городов. В таблице представлены показатели исполнения IV уровня бюджета в селах и сельских округах.')?></p>
                <p><?=Yii::t('app', 'При клике на районный центр в карте, выбранный районный центр или город отражает информацию о поступлениях и расходах, а также исполнения  IV уровня бюджета в селах и сельских округах.')?></p>
                <p><?=Yii::t('app', 'При выборке периода, показатели в аналитическом отчете изменяются на принятые показатели с учетом уточнений данного периода. ')?></p>
            </div>

            <h3><?=Yii::t('app', 'Картограмма')?></h3>
            <?php $form = ActiveForm::begin([
                'id' => 'form-chart'
                ])?>

                <div class="row">
                    <div class="col-sm-3">
                        <?=$form->field($model, 'category_id')->dropDownList($categories)->label('Тип')?>
                    </div>

                    <div class="col-sm-4">
                        <div class="row">
                            <div class="col-xs-6">
                                <?=$form->field($model, 'from')->dropDownList([
                                    '01' => 'Январь'
                                ], ['disabled'=>true])->label('Период')?>
                            </div>
                            <div class="col-xs-6">
                                <?=$form->field($model, 'to')->dropDownList([
                                    '01' => 'Январь',
                                    '02' => 'Февраль',
                                    '03' => 'Март',
                                    '04' => 'Апрель',
                                    '05' => 'Май',
                                    '06' => 'Июнь',
                                    '07' => 'Июль',
                                    '08' => 'Август',
                                    '09' => 'Сентябрь',
                                    '10' => 'Октябрь',
                                    '11' => 'Ноябрь',
                                    '12' => 'Декабрь'
                                ])->label('&nbsp;')?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <?=$form->field($model, 'coeficient')->dropDownList(['1' => 'тыс.тг', '0.001'  => 'млн.тг'])->label('&nbsp;')?>
                    </div>
                    <div class="col-sm-3">
                        <label class="control-label">&nbsp;</label> <br />
                        <button id="btn-chart" type="button" class="btn btn-success">Показать</button>
                    </div>
                </div>

            <?php ActiveForm::end()?>


            <div class="datetime"></div>

            <div id="date_1" class="datetime"></div>
            <div class="row">
                <div class="col-sm-6 col-xs-12">
                    <svg class="chart" id="chart"></svg>
                </div>
                <div class="col-sm-6 col-xs-12">
                    <div id="region-data">

                    </div>
                </div>
            </div>

            <div id="date_2" class="datetime"></div>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th rowspan="2"><?=Yii::t('app', 'Наименование')?></th>
                        <th rowspan="2"><?=Yii::t('app', 'Население')?></th>
                        <th colspan="4"><?=Yii::t('app', 'Доходы')?></th>
                        <th colspan="4"><?=Yii::t('app', 'Субвенция')?></th>
                        <th colspan="4"><?=Yii::t('app', 'Расходы')?></th>
                    </tr>
                    <tr>
                        <th><?=Yii::t('app', 'План на год')?></th>
                        <th><?=Yii::t('app', 'План на период')?></th>
                        <th><?=Yii::t('app', 'Факт. исполн')?></th>
                        <th>%</th>
                        <th><?=Yii::t('app', 'План на год')?></th>
                        <th><?=Yii::t('app', 'План на период')?></th>
                        <th><?=Yii::t('app', 'Факт. исполн')?></th>
                        <th>%</th>
                        <th><?=Yii::t('app', 'План на год')?></th>
                        <th><?=Yii::t('app', 'План на период')?></th>
                        <th><?=Yii::t('app', 'Факт. исполн')?></th>
                        <th>%</th>
                    </tr>
                </thead>
                <tbody id="region-table">

                </tbody>
            </table>



        </div>
    </div>






</div>
