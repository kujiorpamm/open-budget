<?php
    $this->title = "Отчет по областным показателям";
?>


<div class="page obl-budget">


    <div class="heading"><?=$this->title?></div>

    <div class="report-items">
        <?php foreach($last_reports as $report) : ?>
            <div class="item">
                <span class="dwnld"> <a href="<?=$report->filename?>"><i class="glyphicon glyphicon-download-alt"></i></a> </span>
                <a href="/analytics/view-report?id=<?=$report->id?>">Отчет об исполнении бюджета: <?=$report->city->name?> , <?=Yii::$app->formatter->asDate($report->date, 'long')?></a>
            </div>
        <?php endforeach; ?>
    </div>

    <!-- <table class="table">
        <thead>
            <tr>
                <th>Дата</th>
                <th>Город</th>
                <th>Тип</th>
                <th>Ссылка</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($last_reports as $report) : ?>
                <tr>
                    <td><?=Yii::$app->formatter->asDate($report->date, 'php:d.m.Y')?></td>
                    <td><?=$report->city->name?></td>
                    <td><?=$report->type->name?></td>
                    <td><a href="/analytics/view-report?id=<?=$report->id?>"><i class="glyphicon glyphicon-link"> Просмотр </i></a></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table> -->

</div>
