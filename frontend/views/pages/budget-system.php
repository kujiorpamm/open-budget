<?php

    $this->title = Yii::t('app', 'Принципы бюджетной системы');
?>

<div class="page budget-systems">


    <div class="heading"><?=$this->title?></div>
    <?=$this->render('_budget_menu')?>

    <div class="block-1">

        <div class="principe-info" data-show="#principe-1">
            <img class="img img-responsive" src="/images/style/process-1.png" />
            <div class="text"><?=Yii::t('app', 'Единство бюджетной системы')?></div>
            <div class="number">1</div>
        </div>
        <div class="principe-info" data-show="#principe-2">
            <img class="img img-responsive" src="/images/style/process-2.png" />
            <div class="text"><?=Yii::t('app', 'Полнота отражений показателей бюджетов')?></div>
            <div class="number">2</div>
        </div>
        <div class="principe-info" data-show="#principe-3">
            <img class="img img-responsive" src="/images/style/process-3.png" />
            <div class="text"><?=Yii::t('app', 'Достоверность бюджета')?></div>
            <div class="number">3</div>
        </div>
        <div class="principe-info" data-show="#principe-4">
            <img class="img img-responsive" src="/images/style/process-4.png" />
            <div class="text"><?=Yii::t('app', 'Адресность и целевой характер бюджетных средств')?></div>
            <div class="number">4</div>
        </div>
        <div class="principe-info" data-show="#principe-5">
            <img class="img img-responsive" src="/images/style/process-5.png" />
            <div class="text"><?=Yii::t('app', 'Разграничение доходов, расходов и источников финансирования')?></div>
            <div class="number">5</div>
        </div>
        <div class="principe-info" data-show="#principe-6">
            <img class="img img-responsive" src="/images/style/process-6.png" />
            <div class="text"><?=Yii::t('app', 'Результативная разработка и исполнение бюджета')?></div>
            <div class="number">6</div>
        </div>
        <div class="principe-info" data-show="#principe-7">
            <img class="img img-responsive" src="/images/style/process-7.png" />
            <div class="text"><?=Yii::t('app', 'Самостоятельность бюджетов')?></div>
            <div class="number">7</div>
        </div>
        <div class="principe-info" data-show="#principe-8">
            <img class="img img-responsive" src="/images/style/process-8.png" />
            <div class="text"><?=Yii::t('app', 'Подведомственность расходов бюджетов')?></div>
            <div class="number">8</div>
        </div>
        <div class="principe-info" data-show="#principe-9">
            <img class="img img-responsive" src="/images/style/process-9.png" />
            <div class="text"><?=Yii::t('app', 'Сбалансированность бюджета')?></div>
            <div class="number">9</div>
        </div>
        <div class="principe-info" data-show="#principe-10">
            <img class="img img-responsive" src="/images/style/process-10.png" />
            <div class="text"><?=Yii::t('app', 'Равенство бюджетных прав')?></div>
            <div class="number">10</div>
        </div>
        <div class="principe-info" data-show="#principe-11">
            <img class="img img-responsive" src="/images/style/process-11.png" />
            <div class="text"><?=Yii::t('app', 'Единство кассы')?></div>
            <div class="number">11</div>
        </div>
        <div class="principe-info" data-show="#principe-12">
            <img class="img img-responsive" src="/images/style/process-12.png" />
            <div class="text"><?=Yii::t('app', 'Результативность и эффективность бюджетных средств')?></div>
            <div class="number">12</div>
        </div>
        <div class="principe-info" data-show="#principe-13">
            <img class="img img-responsive" src="/images/style/process-13.png" />
            <div class="text"><?=Yii::t('app', 'Общее покрытие расходов бюджета')?></div>
            <div class="number">13</div>
        </div>
        <div class="principe-info" data-show="#principe-14">
            <img class="img img-responsive" src="/images/style/process-14.png" />
            <div class="text"><?=Yii::t('app', 'Адресность и целевой характер бюджетных средств')?></div>
            <div class="number">14</div>
        </div>
    </div>



<?php
    yii\bootstrap\Modal::begin([
        'header' => "<b>$this->title</b>",
        'id' => 'principe-modal',
        'size' => 'modal-lg'
    ]);
?>

<div class="principes">

    <div id="principe-1">
        <div class="row">
            <div class="col-xs-2"><img src="/images/style/process-1-1k.png" /></div>
            <div class="col-xs-10">
                <?=Yii::t('app', 'Принцип единства означает применение единых принципов организации и функционирования бюджетной системы, использование единой бюджетной классификации, единых процедур и технических решений бюджетного процесса в Республике Казахстан')?>
            </div>
        </div>
    </div>
    <div id="principe-2">
        <div class="row">
            <div class="col-xs-2"><img src="/images/style/process-2-2k.png" /></div>
            <div class="col-xs-10">
                <?=Yii::t('app', 'Принцип полноты отражение в бюджете и Национальном фонде Республики Казахстан всех поступлений и расходов, предусмотренных законодательством Республики Казахстан, недопущение зачетов взаимных требований с использованием бюджетных средств, равно как и уступок прав требований по бюджетным средствам')?>
            </div>
        </div>
    </div>
    <div id="principe-3">
        <div class="row">
            <div class="col-xs-2"><img src="/images/style/process-3-3k.png" /></div>
            <div class="col-xs-10">
                <?=Yii::t('app', 'Принцип реалистичности соответствие утвержденных (уточненных, скорректированных) показателей бюджета утвержденным (уточненным, скорректированным) параметрам, направлениям прогнозов социально-экономического развития, стратегических планов государственных органов, программ развития территорий')?>
            </div>
        </div>
    </div>
    <div id="principe-4">
        <div class="row">
            <div class="col-xs-2"><img src="/images/style/process-4-4k.png" /></div>
            <div class="col-xs-10">
                <?=Yii::t('app', 'Принцип транспарентности обязательное опубликование нормативных правовых актов в области бюджетного законодательства Республики Казахстан, утвержденных (уточненных, скорректированных) бюджетов и отчетов об их исполнении, гражданского бюджета, стратегических планов и отчетов об их реализации, о формировании и об использовании Национального фонда Республики Казахстан, за исключением сведений, составляющих государственную или иную охраняемую законом тайну, а также обязательная открытость бюджетного процесса для общества и средств массовой информации')?>
            </div>
        </div>
    </div>
    <div id="principe-5">
        <div class="row">
            <div class="col-xs-2"><img src="/images/style/process-5-5k.png" /></div>
            <div class="col-xs-10">
                <?=Yii::t('app', 'Принцип последовательности  соблюдение государственными органами ранее принятых решений в сфере бюджетных отношений')?>
            </div>
        </div>
    </div>
    <div id="principe-6">
        <div class="row">
            <div class="col-xs-2"><img src="/images/style/process-6-6k.png" /></div>
            <div class="col-xs-10">
                <?=Yii::t('app', 'Принцип результативности разработка и исполнение бюджета, ориентированного на достижение показателей результатов, предусмотренных стратегическими планами, программами развития территорий и (или) бюджетными программами государственных органов')?>
            </div>
        </div>
    </div>
    <div id="principe-7">
        <div class="row">
            <div class="col-xs-2"><img src="/images/style/process-7-7k.png" /></div>
            <div class="col-xs-10">
                <?=Yii::t('app', 'Принцип самостоятельности бюджета установление стабильного распределения поступлений между бюджетами разных уровней и определение направлений их расходования в соответствии с настоящим Кодексом, право всех уровней государственного управления самостоятельно осуществлять бюджетный процесс в соответствии с настоящим Кодексом, недопустимость изъятия доходов, дополнительно полученных в ходе исполнения местных бюджетов, и остатков бюджетных средств местных бюджетов в вышестоящие бюджеты, недопустимость возложения на нижестоящие бюджеты дополнительных расходов без соответствующей их компенсации')?>
            </div>
        </div>
    </div>
    <div id="principe-8">
        <div class="row">
            <div class="col-xs-2"><img src="/images/style/process-8-8k.png" /></div>
            <div class="col-xs-10">
                <?=Yii::t('app', 'Принцип преемственности планирование республиканского и местных бюджетов, основанное на прогнозах социально-экономического развития, базовых расходах, утвержденных в предыдущие периоды, итогах бюджетного мониторинга, оценке результатов')?>
            </div>
        </div>
    </div>
    <div id="principe-9">
        <div class="row">
            <div class="col-xs-2"><img src="/images/style/process-9-9k.png" /></div>
            <div class="col-xs-10">
                <?=Yii::t('app', 'Принцип обоснованности планирование бюджета на основе нормативных правовых актов и других документов, определяющих необходимость включения в проект бюджета тех или иных поступлений или расходов и обоснованность их объемов, а также использование бюджетных средств и активов государства в соответствии с законодательством Республики Казахстан')?>
            </div>
        </div>
    </div>
    <div id="principe-10">
        <div class="row">
            <div class="col-xs-2"><img src="/images/style/process-10-10k.png" /></div>
            <div class="col-xs-10">
                <?=Yii::t('app', 'Принцип своевременности зачисление поступлений в республиканский и местные бюджеты, на контрольный счет наличности Национального фонда Республики Казахстан и перевод их на счета Правительства в Национальном Банке Республики Казахстан, принятие обязательств государственными учреждениями в соответствии с индивидуальными планами финансирования по обязательствам, проведение платежей в соответствии с индивидуальными планами финансирования по платежам и перечисление бюджетных средств на счета получателей бюджетных средств в сроки с соблюдением порядка, установленного соответствующими нормативными правовыми актами')?>
            </div>
        </div>
    </div>
    <div id="principe-11">
        <div class="row">
            <div class="col-xs-2"><img src="/images/style/process-11-11k.png" /></div>
            <div class="col-xs-10">
                <?=Yii::t('app', 'Принцип единства кассы зачисление всех поступлений в бюджет на единый казначейский счет и осуществление всех предусмотренных расходов с единого казначейского счета в национальной валюте')?>
            </div>
        </div>
    </div>
    <div id="principe-12">
        <div class="row">
            <div class="col-xs-2"><img src="/images/style/process-12-12k.png" /></div>
            <div class="col-xs-10">
                <?=Yii::t('app', 'Принцип эффективности разработка и исполнение бюджета исходя из необходимости достижения наилучшего прямого и конечного результата с использованием утвержденного объема бюджетных средств или достижения прямого и конечного результата с использованием меньшего объема бюджетных средств')?>
            </div>
        </div>
    </div>
    <div id="principe-13">
        <div class="row">
            <div class="col-xs-2"><img src="/images/style/process-13-13.png" /></div>
            <div class="col-xs-10">
                <?=Yii::t('app', 'Принцип ответственности принятие необходимых административных и управленческих решений, направленных на достижение прямых и конечных результатов и обеспечение ответственности администраторов бюджетных программ и руководителей государственных учреждений и субъектов квазигосударственного сектора за принятие решений, не соответствующих законодательству Республики Казахстан')?>
            </div>
        </div>
    </div>
    <div id="principe-14">
        <div class="row">
            <div class="col-xs-2"><img src="/images/style/process-14-14k.png" /></div>
            <div class="col-xs-10">
                <?=Yii::t('app', 'Принцип адресности и целевого характера бюджетных средств направление и использование бюджетных средств администраторами бюджетных программ, субъектами квазигосударственного сектора для достижения показателей результатов, предусмотренных стратегическими планами, программами развития территорий и (или) бюджетными программами государственных органов, финансово-экономическими обоснованиями бюджетных инвестиций, посредством участия в уставном капитале субъектов квазигосударственного сектора с соблюдением законодательства Республики Казахстан')?>
            </div>
        </div>
    </div>
</div>


<?php
    yii\bootstrap\Modal::end();
?>

</div>
