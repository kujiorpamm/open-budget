<?php
    use yii\helpers\Html;
?>
<ul class="subheading_menu">
  <li class="<?=(Yii::$app->controller->action->id == 'about-budget')?'active':''?>">
      <?=Html::a(Yii::t("app", "Бюджет"), ["/pages/about-budget"])?></li>
  <li class="<?=(Yii::$app->controller->action->id == 'income')?'active':''?>" >
       <?=Html::a(Yii::t("app", "Доходы"), ["/pages/income"])?></li>
  <li class="<?=(Yii::$app->controller->action->id == 'budget-process')?'active':''?>">
      <?=Html::a(Yii::t("app", "Бюджетный процесс"), ["/pages/budget-process"])?></li>
  <li class="<?=(Yii::$app->controller->action->id == 'budget-system')?'active':''?>">
      <?=Html::a(Yii::t("app", "Принципы бюджетной системы"), ["/pages/budget-system"])?></li>
</ul>
