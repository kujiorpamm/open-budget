<?php

    $this->title = Yii::t('app', 'Доходы');
    // $this->params['breadcrumbs'][] = $this->title;
?>


<div class="page about-budget">


    <div class="heading"><?=$this->title?></div>
    <?=$this->render('_budget_menu')?>

    <div class="block-2">
        <div class="center"><img class="image image-responsive" src="/images/money-2.png" /></div>
        <h4 class="center"></h4>
        <p class="center subhead"><?=Yii::t('app', 'Безвозмездные и безвозвратные<br />поступления денежных средств в бюджет')?></p>
        <br />
        <br />
        <div class="row">
            <div class="col-sm-4">
                <div class="schema-container">
                    <div class="schema">
                        <div class="image-container type1"><img class="img img-responsive" src="/images/money-1.png" /></div>
                        <center><img width="180px" src="/images/style/tree-1.png" /></center>
                        <p class="i-heading"><?=Yii::t('app', 'НАЛОГОВЫЕ ПОСТУПЛЕНИЯ')?></p>
                        <p class="i-content"><?=Yii::t('app', 'Поступление уплаты от физических и юридических лиц установленных Законодательством РК')?></p>
                    </div>

                    <center><img width="60px" style="padding-bottom: 40px;" src="/images/style/Document_icon.png" /></center>
                    <p class="example-header"><span><?=Yii::t('app', 'ПРИМЕРЫ')?></span></p>
                    <ul class="styled green">
                        <li><?=Yii::t('app', 'Индивидуальный подоходный налог')?></li>
                        <li><?=Yii::t('app', 'Социальный налог')?></li>
                        <li><?=Yii::t('app', 'Налоги на собственность')?></li>
                        <li><?=Yii::t('app', 'Земельный налог')?></li>
                        <li><?=Yii::t('app', 'Транспортный налог')?></li>
                        <li><?=Yii::t('app', 'Прочие налоги')?></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="schema-container">
                    <div class="schema">
                        <div class="image-container type1"><img class="img img-responsive" src="/images/money-1.png" /></div>
                        <center><img width="180px" src="/images/style/tree-2.png" /></center>
                        <p class="i-heading"><?=Yii::t('app', 'НЕНАЛОГОВЫЕ ПОСТУПЛЕНИЯ')?></p>
                        <p class="i-content"><?=Yii::t('app', 'Поступление уплаты от физических, юридических лиц и других платежей установленных Законодательством РК')?></p>
                    </div>

                    <center><img width="60px" style="padding-bottom: 40px;" src="/images/style/Document_icon.png" /></center>
                    <p class="example-header"><span><?=Yii::t('app', 'ПРИМЕРЫ')?></span></p>
                    <ul class="styled green">
                        <li><?=Yii::t('app', 'Доходы от использования имущества, назодящегося в гос. собственности')?></li>
                        <li><?=Yii::t('app', 'Отчисления недропользователей на социально - экономическое развитие региона и инфраструктуры')?></li>
                        <li><?=Yii::t('app', 'Прочие неналоговые поступления')?></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="schema-container">
                    <div class="schema">
                        <div class="image-container type1"><img class="img img-responsive" src="/images/money-1.png" /></div>
                        <center><img width="180px" src="/images/style/tree-3.png" /></center>
                        <p class="i-heading"><?=Yii::t('app', 'ПОСТУПЛЕНИЯ ОТ ПРОДАЖИ ОСНОВНОГО КАПИТАЛА')?></p>
                    </div>

                    <center><img width="60px" style="padding-bottom: 40px;" src="/images/style/Document_icon.png" /></center>
                    <p class="example-header"><span><?=Yii::t('app', 'ПРИМЕРЫ')?></span></p>
                    <ul class="styled green">
                        <li><?=Yii::t('app', 'Продажа государственного имущества закрепленного за государственными учреждениями')?></li>
                        <li><?=Yii::t('app', 'Продажа земли и нематериальных активов')?></li>
                        <li><?=Yii::t('app', 'Поступления от продажи квартир гражданам')?></li>
                    </ul>
                </div>
            </div>
            <!-- <div class="col-sm-3">
                <div class="schema-container">
                    <div class="schema">
                        <div class="image-container type1"><img class="img img-responsive" src="/images/money-1.png" /></div>
                        <p class="i-heading"><?=Yii::t('app', 'ПОСТУПЛЕНИЯ ТРАНСФЕРТОВ')?></p>
                    </div>

                    <p class="example-header"><span><?=Yii::t('app', 'ПРИМЕРЫ')?></span></p>
                    <ul class="styled green">
                        <li><?=Yii::t('app', 'Трансферты из вышестоящих органов государственного управления')?></li>
                    </ul>
                </div>
            </div> -->
        </div>

    </div>

</div>
