<?php

/* @var $this yii\web\View */
use yii\helpers\Html ;

$this->title = 'Budget';

?>



<div class="site-index">

    <div class="block block-1">
        <div class="container">
            <h3 class="center"><?=Yii::t('app', 'ПОКАЗАТЕЛИ БЮДЖЕТА МАНГИСТАУСКОЙ ОБЛАСТИ')?></h3>

            <div class="text-block">
                <p><?= Yii::t('app', 'На странице приводятся основные характеристики формирования бюджета за прошлые два года, текущий финансовый год и плановый период. Динамика приводится по основным показателям «Доходы бюджета – всего», «Расходы бюджета – всего».') ?></p>
                <p><?= Yii::t('app', 'Для показателей бюджета доступен просмотр варианта бюджета: «Областной бюджет», «Бюджет области», «Бюджет районов и городов».') ?></p>
                <p><?= Yii::t('app', 'При выборке периода, показатели в аналитическом отчете изменяются на принятые показатели с учетом уточнений данного периода.') ?></p>
            </div>

            <?=$this->render('parts/_chart_index_1', ['model' => $chart1, 'cities' => $cities, 'years' => $years])?>
            <?=$this->render('parts/_chart_index_2')?>

        </div>
    </div>




    <div class="block block-analytics">
        <div class="container">
            <h3 class="center"><?=Yii::t('app', 'Аналитическая информация')?></h3>

            <div class="row">
                <div class="col-sm-4">
                    <div class="chart-button">
                        <a href="#">
                            <div class="image-container" style="background-image: url('/images/style/chart-1.png')"></div>
                            <div class="text"><?=Yii::t("app", "Налоговые и неналоговые доходы бюджета");?></div>
                        </a>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="chart-button">
                        <a href="#">
                            <div class="image-container" style="background-image: url('/images/style/chart-2.png')"></div>
                            <div class="text"><?=Yii::t("app", "Распределение расходов бюджета");?></div>
                        </a>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="chart-button">
                        <a href="#">
                            <div class="image-container" style="background-image: url('/images/style/chart-3.png')"></div>
                            <div class="text"><?=Yii::t("app", "Распределение доходов бюджета");?></div>
                        </a>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="chart-button">
                        <a href="#">
                            <div class="image-container" style="background-image: url('/images/style/chart-4.png')"></div>
                            <div class="text"><?=Yii::t("app", "Основные параметры бюджета");?></div>
                        </a>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="chart-button">
                        <a href="#">
                            <div class="image-container" style="background-image: url('/images/style/chart-5.png')"></div>
                            <div class="text"><?=Yii::t("app", "Структура доходов бюджета");?></div>
                        </a>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="chart-button">
                        <a href="#">
                            <div class="image-container" style="background-image: url('/images/style/chart-6.png')"></div>
                            <div class="text"><?=Yii::t("app", "Исполнение плановых показателей бюджета");?></div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="block container">
        <iframe id="iframe2020" class="material-card" width="100%" scrolling="no" height="300" style="border: medium none;margin-bottom: 20px;" src="http://strategy2050.kz/<?=Yii::$app->language?>/informer/vinformer"></iframe>
    </div>


</div>
