<div class="statistics-index" id="tables">

    <h3><?=Yii::t("app", "Структура исполнения поступлений Областного бюджета");?></h3>
    <div class="datetime">  <?=Yii::t("app", "Информация по состоянию на <span class='dtm'></span>");?> </div>
    <table class="table table-striped" id="t_income">
        <thead>
            <tr>

            </tr>
        </thead>

        <tbody>

        </tbody>

    </table>
</div>

<div class="statistics-index">

    <h3><?=Yii::t("app", "Структура исполнения расходов областного бюджета");?></h3>
    <div class="datetime">  <?=Yii::t("app", "Информация по состоянию на <span class='dtm'></span>");?> </div>
    <table class="table table-striped"  id="t_outcome">
        <thead>
            <tr>

            </tr>
        </thead>

        <tbody>

        </tbody>

    </table>
</div>
