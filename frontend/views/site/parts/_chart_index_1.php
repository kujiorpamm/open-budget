<?php

use yii\helpers\Html ;
use yii\widgets\ActiveForm ;

$this->registerJsFile('/js/helper.js', ['depends' => '\yii\web\JqueryAsset']);
$this->registerJsFile('/js/index/index.js', ['depends' => '\yii\web\JqueryAsset']);

?>

<?php $form = ActiveForm::begin([
    'id' => 'form-chart-1'
    ])?>

    <div class="row">
        <div class="col-sm-3">
            <?=$form->field($model, 'city')->dropDownList($cities)->label('Тип')?>
        </div>
        <div class="col-sm-4">
            <div class="row">
                <div class="col-xs-6">
                    <?=$form->field($model, 'year')->dropDownList($years)->label('Период')?>
                </div>
                <div class="col-xs-6">
                    <?=$form->field($model, 'month')->dropDownList([
                        '01' => Yii::t('app', 'Январь'),
                        '02' => Yii::t('app', 'Февраль'),
                        '03' => Yii::t('app', 'Март'),
                        '04' => Yii::t('app', 'Апрель'),
                        '05' => Yii::t('app', 'Май'),
                        '06' => Yii::t('app', 'Июнь'),
                        '07' => Yii::t('app', 'Июль'),
                        '08' => Yii::t('app', 'Август'),
                        '09' => Yii::t('app', 'Сентябрь'),
                        '10' => Yii::t('app', 'Октябрь'),
                        '11' => Yii::t('app', 'Ноябрь'),
                        '12' => Yii::t('app', 'Декабрь')
                    ])->label('&nbsp;')?>
                </div>
            </div>
        </div>
        <div class="col-sm-2">
            <?=$form->field($model, 'coeficient')->dropDownList(['1' => Yii::t('app', 'тыс.тг'), '0.001'  => Yii::t('app', 'млн.тг')])->label('&nbsp;')?>
        </div>
        <div class="col-sm-3">
            <label class="control-label">&nbsp;</label> <br />
            <button id="btn-chart-1" type="button" class="btn btn-success"><?=Yii::t("app", "Показать");?></button>
        </div>
    </div>
    <a id="link1" href="#" target="_blank"><?=Yii::t("app", "Отчет");?></a>

<?php ActiveForm::end()?>

<div class="row">
    <div id="chart-index">
        <div class="datetime"></div>
        <div class="labelset left">
            <div class="label1"><?=Yii::t("app", "Установлено")?></div>
            <div id="left-top" class="value1">22</div>
        </div>
        <div class="labelset bottom left">
            <div class="label1"><?=Yii::t("app", "Исполнено")?></div>
            <div id="left-bot" class="value1">22</div>
        </div>
        <div class="labelset right">
            <div class="label1"><?=Yii::t("app", "Установлено")?></div>
            <div id="right-top" class="value1">222</div>
        </div>
        <div class="labelset bottom right">
            <div class="label1"><?=Yii::t("app", "Исполнено")?></div>
            <div id="right-bot" class="value1">222</div>
        </div>
        <div class="bar left">
            <div class="name"><?=Yii::t('app', "Поступление")?></div>
            <div class="fill"></div>
            <div class="bg"></div>
        </div>
        <div class="bar right">
            <div class="name"><?=Yii::t('app', "Исполнение")?></div>
            <div class="fill"></div>
            <div class="bg"></div>
        </div>
    </div>
</div>
