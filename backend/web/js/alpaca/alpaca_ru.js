﻿(function($) {

	// german - germany

	var Alpaca = $.alpaca;

	Alpaca.registerView ({
		"id": "base",
		"messages": {
            "ru_RU": {
                required: "Обязательно",
                invalid: "Неверно",
                months: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
                timeUnits: {
                    SECOND: "Секунды",
                    MINUTE: "Минуты",
                    HOUR: "Часы",
                    DAY: "Дни",
                    MONTH: "Месяца",
                    YEAR: "Года"
                },
				"addItemButtonLabel" : "Добавить новый элемент",
				"addButtonLabel" : "Добавить",
				"removeButtonLabel" : "Удалить",
				"upButtonLabel" : "Выше",
				"downButtonLabel" : "Ниже",
                "notOptional": "Обязательно",
                "disallowValue": "Неверные значения: {0}",
                "invalidValueOfEnum": "Это поле должно содержать одно из следующих значений: {0}. [{1}]",
                "notEnoughItems": "Минимальное количество элементов {0}",
                "tooManyItems": "Максимальное количество элементов {0}",
                "valueNotUnique": "Значение уже есть",
                "notAnArray": "Keine Liste von Werten",
                "invalidDate": "Неверная дата: {0}",
                "invalidEmail": "Неправильный email",
                "stringNotAnInteger": "Должно быть часло",
                "invalidIPv4": "Ungültige IPv4 Adresse",
                "stringValueTooSmall": "Die kleinstmögliche Zahl ist {0}",
                "stringValueTooLarge": "Die grösstmögliche Zahl ist {0}",
                "stringValueTooSmallExclusive": "Die kleinstmögliche Zahl muss größer sein als {0}",
                "stringValueTooLargeExclusive": "Die grösstmögliche Zahl muss kleiner sein als {0}",
                "stringDivisibleBy": "Der Wert muss durch {0} dividierbar sein",
                "stringNotANumber": "Die Eingabe ist keine Zahl",
                "invalidPassword": "Ungültiges Passwort",
                "invalidPhone": "Ungültige Telefonnummer",
                "invalidPattern": "Diese Feld stimmt nicht mit folgender Vorgabe überein {0}",
                "stringTooShort": "Dieses Feld sollte mindestens {0} Zeichen enthalten",
                "stringTooLong": "Dieses Feld sollte höchstens {0} Zeichen enthalten"
            }
		}
	});

})(jQuery);
