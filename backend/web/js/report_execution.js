window.current_count = 0;

function executeReports() {
    $.ajax({
        url: '/admin/reports/ajax-process',
        type: 'POST',
        dataType: 'JSON',
        data: {
            current: window.current_count
        },
        beforeSend: function() {
            $('#btn-process').button('loading');
        },
        success: function(data) {
            window.current_count++;
            $('#cnt_current').html(window.current_count);
            $('#cnt_total').html(data.total);
            if(data.total > window.current_count) {
                executeReports();
            } else {
                window.current_count = 0;
                $('#btn-process').button('reset');
            }

            if(data.status == true) {
                $('#status_log').append("Отчет №" + data.report_id + " ОК <br/>");
            } else if (data.status == false) {
                $('#status_log').append("Отчет №" + data.report_id + " <b>ОШИБКА!</b>  <br/>");
            }

        }
    });

}
