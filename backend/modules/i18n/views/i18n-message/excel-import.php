<?php
    use yii\helpers\Html;
?>

<div class="excel-create">

    <?= Html::beginForm([''], 'post', ['enctype'=>'multipart/form-data']);?>
    <?=Html::dropDownList('type', '', ['general' => 'Общий перевод', 'reports'=> 'Типы отчетов'])?> <br /><br />
    <?=Html::fileInput("file")?> <br />
    <?=Html::submitButton("Отправить", ['class' => 'btn btn-success'])?>

    <?= Html::endForm(); ?>

</div>
