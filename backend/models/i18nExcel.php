<?php

namespace backend\models;

use Yii;
use backend\modules\i18n\models\I18nSourceMessage;
use backend\modules\i18n\models\I18nMessage;
use common\models\ReportItemCategories;

class i18nExcel extends \yii\base\Model {

    public static function processExcel($path) {

        set_time_limit ( 300 );

        $data = \moonland\phpexcel\Excel::import($path,
            [
               'setFirstRecordAsKeys' => false,
               'setIndexSheetByName' => false,
               'getOnlySheet' => 'Отчет'
            ]
        );

        $result = [
            'total' => 0,
            'success' => 0,
            'error' => 0
        ];

        foreach ($data as $row) {
            $source = I18nSourceMessage::find()->where(['message' => $row['A']])->one();
            $result['total'] += 1;
            if($source) {
                $source->setTranslation("kk", $row['B']) == true ? $result['success'] += 1 : $result['error'] += 1;;
            } else {
                var_dump("NOT FOUND: " . $row['A']);
            }
        }

        return $result;
    }

    public static function processExcelForReports($path) {
        set_time_limit ( 300 );

        $data = \moonland\phpexcel\Excel::import($path,
            [
               'setFirstRecordAsKeys' => false,
               'setIndexSheetByName' => false,
               'getOnlySheet' => 'Отчет'
            ]
        );

        $result = [
            'total' => 0,
            'success' => 0,
            'error' => 0
        ];

        foreach ($data as $row) {
            $report_category = ReportItemCategories::find()->where(['name_ru' => $row['A']])->one();
            $result['total'] += 1;
            if($report_category) {
                $report_category->name_kz =  $row['B'];
                $report_category->save();
            } else {
                var_dump("NOT FOUND: " . $row['A']);
            }
        }

        return $result;

    }

}
