<?php

namespace backend\controllers;

use Yii;
use common\models\Reports;
use common\models\ReportTypes;
use common\models\ReportItemCategories;
use common\models\Cities;
use common\models\search\ReportsSearch;
use common\models\ReportItems;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ReportsController implements the CRUD actions for Reports model.
 */
class ReportsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }



    public function actionInitCategories() {
        $main_types = [
                'ДОХОДЫ',
                'ЗАТРАТЫ',
                'ЧИСТОЕ БЮДЖЕТНОЕ КРЕДИТОВАНИЕ',
                'САЛЬДО ПО ОПЕРАЦИЯМ С ФИНАНСОВЫМИ АКТИВАМИ',
                'ДЕФИЦИТ (ПРОФИЦИТ) БЮДЖЕТА',
                'ФИНАНСИРОВАНИЕ ДЕФИЦИТА (ИСПОЛЬЗОВАНИЕ ПРОФИЦИТА) БЮДЖЕТА'
        ];

        foreach ($main_types as $key => $value) {
            $model = new ReportItemCategories();
            $model->parent_id = NULL;
            $model->code = NULL;
            $model->name_ru = $value;
            $model->fullname = $value;
            if(!$model->save()) {
                echo "<pre>"; print_r($model->getErrors()); echo "</pre>"; exit;
            }
        }

    }

    /**
     * Lists all Reports models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ReportsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $types = ReportTypes::find()->select('name_ru')->indexBy('id')->column();
        $cities = Cities::find()->select('name_ru')->indexBy('id')->column();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'types' => $types,
            'cities' => $cities
        ]);
    }

    /**
     * Displays a single Reports model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {

        $model = $this->findModel($id);
        if(Yii::$app->session->hasFlash('error')) {
            ReportItems::deleteAll(['report_id' => $model->id]);
            // echo "<pre>"; print_r(Yii::$app->session->getFlash('error')); echo "</pre>"; exit;
        }
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => ReportItems::find()->where(['report_id' => $model->id])
        ]);

        return $this->render('view', [
            'model' => $this->findModel($id),
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionProcess($id) {
        $model = $this->findModel($id);
        // Обработать файл
        $result = $model->processExcel();
        // Если есть ошибки, то добавить их в переменню сессии
        // echo "<pre>"; print_r( $result['error']->getMessage() ); echo "</pre>"; exit;
        if($result['status'] == false) {
            $model->status = false;
            $model->save();
            // echo "<pre>"; var_dump($result['error']); echo "</pre>"; exit;
            Yii::$app->session->setFlash('error', $result['error']->getMessage() . "(Строка в файле: {$result['row']})");
        }

        return $this->redirect(['view', 'id' => $id]);
    }

    public function beforeAction($action) {

            $this->enableCsrfValidation = false;
            return parent::beforeAction($action);
    }

    // Обрабатывает все отчеты по клику кнопки
    public function actionAjaxProcess() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $offset = Yii::$app->request->post('current');
        $model = Reports::find()->orderBy('id ASC')->offset($offset)->one();
        $total = Reports::find()->count();

        if($model) {
            $result = $model->processExcel();
        }

        if($result['status']  == false) {
            return [
                'total' => $total,
                'offset' => $offset,
                'report_id' => $model->id,
                'status' => false,
                'message' => $result['error']->getMessage()
            ];
        } else {
            return [
                'total' => $total,
                'offset' => $offset,
                'report_id' => $model->id,
                'status' => true
            ];
        }

    }

    public function actionCreate()
    {
        $model = new Reports();
        $model->scenario = 'create';

        $model->filename = "repo_" . rand();
        $types = ReportTypes::find()->select('name_ru')->indexBy('id')->column();
        $cities = Cities::find()->select('name_ru')->indexBy('id')->column();


        if ($model->load(Yii::$app->request->post())) {
            $model->file = UploadedFile::getInstance($model, 'file');
            $model->date = Yii::$app->formatter->asDate($model->date, 'php:Y-m-d');
            $model->date_fact = date('Y-m-d H:i:s', strtotime($model->date . ' -1 month'));
            if($model->validate()) {
                $model->filename = '/uploads/reports/' . $model->filename . '.' . $model->file->extension;
                $model->save();
                $model->file->saveAs( Yii::getAlias('@frontend') . '/web' . $model->filename);
            }
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'types' => $types,
            'cities' => $cities
        ]);
    }

    public function actionCreateMultiple() {

        $types = ReportTypes::find()->select('name_ru')->indexBy('id')->column();
        $cities = Cities::find()->select('name_ru')->indexBy('id')->column();

        if(Yii::$app->request->post()) {
            $item_name = "reports_";
            $items = [];
            foreach ($_POST as $key => $value) {
                unset($matches);
                if (preg_match('/reports_(.?)_(.{2,})/', $key, $matches)) {
                    $items[$matches[1]][$matches[2]] = $value;
                }
            }
            foreach ($_FILES as $key => $value) {
                unset($matches);
                if (preg_match('/reports_(.?)_(.{2,})/', $key, $matches)) {
                    $items[$matches[1]][$matches[2]] = $value['tmp_name'];
                }
            }

            foreach ($items as $key => $item) {
                $model = new Reports();
                $model->scenario = 'create';
                $model->filename = "repo_" . rand();
                $model->date = Yii::$app->formatter->asDate("01." . $item['date'], 'php:Y-m-d');
                $model->date_fact = date('Y-m-d H:i:s', strtotime($model->date . ' -1 month'));
                $model->type_id = array_search($item['type'], $types);
                $model->city_id = array_search($item['city'], $cities);
                // файл
                $model->file = UploadedFile::getInstanceByName("reports_{$key}_file");
                if($model->validate()) {
                    $model->filename = '/uploads/reports/' . $model->filename . '.' . $model->file->extension;
                    $model->save();
                    $model->file->saveAs( Yii::getAlias('@frontend') . '/web' . $model->filename);
                } else {
                    throw new \Exception("Произошла ошибка при загрузке отчета {$item['date']} : {$item['city']}, {$item['type']}", 1);
                }
            }

            return $this->refresh();
        }

        return $this->render('create-multiple', [
            'types' => $types,
            'cities' => $cities
        ]);
    }

    /**
     * Updates an existing Reports model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $types = ReportTypes::find()->select('name_ru')->indexBy('id')->column();
        $cities = Cities::find()->select('name_ru')->indexBy('id')->column();

        $model->date = Yii::$app->formatter->asDate($model->date, 'php:d.m.Y');

        if ($model->load(Yii::$app->request->post())) {
            $model->date = Yii::$app->formatter->asDate($model->date, 'php:Y-m-d');
            if($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'types' => $types,
            'cities' => $cities
        ]);
    }

    /**
     * Deletes an existing Reports model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionFixDates() {
        $models = Reports::find()->all();

        foreach ($models as $model) {
            $date = strtotime($model->date . ' -1 month');
            $model->date_fact = date('Y-m-d H:i:s', $date);

            if( !$model->save()) {
                echo "<pre>"; print_r($model->getErrors()); echo "</pre>"; exit;
            }
        }
    }

    /**
     * Finds the Reports model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Reports the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Reports::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
