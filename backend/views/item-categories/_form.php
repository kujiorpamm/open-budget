<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ReportItemCategories */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="report-item-categories-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name_kz')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
