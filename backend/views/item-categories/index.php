<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Категории индикаторов отчетов';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="report-item-categories-index">
    <!-- <p>
        <?= Html::a('Create Report Item Categories', ['create'], ['class' => 'btn btn-success']) ?>
    </p> -->

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            // 'parent_id',
            // 'code',
            'name_ru',
            'name_kz',
            //'fullname',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}'
            ],
        ],
    ]); ?>
</div>
