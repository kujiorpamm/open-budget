<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ReportItemCategories */

$this->title = 'Редактировать перевод для : ' . $model->name_ru;
$this->params['breadcrumbs'][] = ['label' => 'Report Item Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="report-item-categories-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
