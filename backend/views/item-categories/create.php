<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ReportItemCategories */

$this->title = 'Create Report Item Categories';
$this->params['breadcrumbs'][] = ['label' => 'Report Item Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="report-item-categories-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
