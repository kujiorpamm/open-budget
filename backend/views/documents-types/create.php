<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\DocumentsTypes */

$this->title = 'Новый тип документа';
$this->params['breadcrumbs'][] = ['label' => 'Documents Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="documents-types-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
