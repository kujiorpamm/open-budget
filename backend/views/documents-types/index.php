<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Типы документов';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="documents-types-index">

    <p>
        <?= Html::a('Добавить новый тип', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name_ru',
            'name_kz',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
