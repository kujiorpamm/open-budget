<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\DocumentsTypes */

$this->title = 'Редактирование';
$this->params['breadcrumbs'][] = ['label' => 'Documents Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="documents-types-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
