<?php

use \yii\helpers\Url;

?>

<aside class="main-sidebar">

    <section class="sidebar">

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Меню', 'options' => ['class' => 'header']],
                    // ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii']],
                    // ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug']],
                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                    ['label'=>'Отчеты', 'icon' => 'flag', 'url' => Url::to(['reports/index'])],
                    ['label'=>'Подотчеты', 'icon' => 'flag', 'url' => Url::to(['subreports/index'])],
                    ['label'=>'Документы', 'icon' => 'file', 'url' => Url::to(['documents/index'])],
                    ['label' => 'Справочники', 'icon' => 'share', 'url' => '#', 'items' =>
                        [
                            ['label'=>'Города', 'url' => Url::to(['cities/index'])],
                            ['label'=>'Типы отчетов', 'url' => Url::to(['report-types/index'])],
                            ['label'=>'Типы документов', 'url' => Url::to(['documents-types/index'])],
                        ]
                    ],
                    ['label' => 'Языки', 'items' =>
                        [
                            ['label'=>'Переводы общие', 'url' => Url::to(['/i18n/i18n-message/index'])],
                            ['label'=>'Переводы отчетов', 'url' => Url::to(['/item-categories/index'])],
                            ['label'=>'Эксель', 'url' => Url::to(['/i18n/i18n-message/excel-import'])],
                            ['label'=>'Словарь', 'url' => Url::to(['/i18n/i18n-source-message/index'])],
                        ]
                    ]
                ],
            ]
        ) ?>

    </section>

</aside>
