<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\ReportsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Отчеты');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reports-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="buttons-heading">
        <div class="clearfix">
            <div class="pull-left">
                <?= Html::a(Yii::t('app', 'Новый отчет'), ['create'], ['class' => 'btn btn-success']) ?>
                <?= Html::a(Yii::t('app', 'Множественная загрузка'), ['create-multiple'], ['class' => 'btn btn-success']) ?>
            </div>
            <div class="pull-right">
                <button onclick="$('#modal-reports').modal('show');" class="btn btn-success">Обработать отчеты</button>
            </div>
        </div>
    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'date',
                'format' => 'date',
                'filter' => kartik\date\DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'date',
                    'options' => ['placeholder' => 'Укажите дату'],
                    'language' => 'ru',
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'viewMode' => 'months',
                        'minViewMode' => 'months',
                    ]
                ])
            ],
            [
                'attribute' => 'type_id',
                'value' => 'type.name',
                'filter' => $types
            ],
            [
                'attribute' => 'city_id',
                'value' => 'city.name',
                'filter' => $cities
             ],
            [
                'attribute' => 'filename',
                'filter' => false
            ],
            [
                'attribute' => 'status',
                'label' => 'Статус обработки',
                'format' => 'raw',
                'value' => function ($model) {
                    if($model->status === 0) {
                        return "<i style='color:#ff0000' class='glyphicon glyphicon-exclamation-sign'></i>";
                    } else {
                        return "<i style='color:#00ff00' class='glyphicon glyphicon-ok'></i>";
                    }
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

<?php
    Modal::begin([
        'id' => 'modal-reports',
        'header' => 'Обработать все отчеты единовременно',
    ]);
?>

    <p>Пожалуйста, не закрывайте окно до тех пор, пока отчеты полностью не обработаются.</p> <br /> <br />
    <div class="row">

        <div class="col-xs-8">
            <p style="font-weight: 700">
                Обработано <span id="cnt_current">0</span> из <span id="cnt_total">??</span>
                <div id="status_log"></div>
            </p>
        </div>
        <div class="col-xs-4">
            <button onclick="executeReports();" id="btn-process" class="btn btn-block btn-warning" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Обработка" >Обработать</button>
        </div>
    </div>

<?php
    Modal::end();
?>
