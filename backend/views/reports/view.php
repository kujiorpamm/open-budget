<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Reports */

$this->title = "Отчет №{$model->id}";
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Reports'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reports-view">

    <p>
        <?=Html::a('Обработать', ['process', 'id'=> $model->id], ['class' => 'btn btn-primary'])?>
    </p>
    <!-- <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p> -->

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'date:date',
            ['attribute' => 'type.name_ru', 'label' => 'Тип отчета'],
            ['attribute' => 'city.name_ru', 'label' => 'Город'],
            [
                'attribute' =>'filename',
                'format' => 'raw',
                'value' => function($model) {
                    return Html::a("<i class='glyphicon glyphicon-link'>Скачать</i>", $model->filename);
                }
            ],
            [
                'label' => 'Статус обработки',
                'format' => 'raw',
                'value' => function($model) {
                    if ($model->status == 0) {
                        return "<div class='alert alert-danger'>Содержит ошибки. Обработайте отчет, чтобы получить информацию об ошибках.</div>";
                    } else {
                        return "Обработан";
                    }
                }
            ]
            // 'data:ntext',
        ],
    ]) ?>

    <?=GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'category.fullname',
            ['attribute' => 'category.name_ru', 'headerOptions' => ['style' => 'width: 20%;']],
            'budget_approved:decimal',
            'budget_refined:decimal',
            'budget_corrected:decimal',
            'plan_payments:decimal',
            'plan_obligations:decimal',
            'obligations_accepted:decimal',
            'obligations_unpaid:decimal',
            'budget_execution:decimal',
            'budget_execution_percent:decimal',
            'budget_execution_percent_total:decimal'
        ],
        'options' => [
            'class' => 'grid-view small-header grid-view-large',
        ]
    ])?>


</div>
