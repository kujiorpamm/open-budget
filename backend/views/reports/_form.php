<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model common\models\Reports */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="row">
    <div class="col-lg-6 col-md-12">

        <div class="reports-form">

            <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>

            <?= $form->field($model, 'date')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Укажите дату'],
                'language' => 'ru',
                'pluginOptions' => [
                    'autoclose'=>true,
                    'viewMode' => 'months',
                    'minViewMode' => 'months',
                ]
            ]); ?>

            <?= $form->field($model, 'city_id')->dropDownList($cities) ?>

            <?= $form->field($model, 'type_id')->dropDownList($types) ?>

            <?= $form->field($model, 'file')->widget(FileInput::classname(), [
                'language' => 'ru',
                'options' => ['accept' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'],
                'pluginOptions' => [
                    'showPreview' => false,
                    'showCaption' => true,
                    'showRemove' => true,
                    'showUpload' => false
                ]
            ]); ?>

            <?php if (!$model->isNewRecord) echo $form->field($model, 'data')->textarea(['rows' => 6]) ?>

            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>


    </div>
</div>
