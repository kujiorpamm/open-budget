<?php

use yii\web\View;
use yii\helpers\Html;

$this->title = "Множественная загрузка отчетов";

$this->registerCssFile('/admin/js/alpaca/bootstrap-datetimepicker.min.css');
$this->registerCssFile('/admin/js/alpaca/alpaca.min.css');
$this->registerJsFile('/admin/js/alpaca/moment.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('/admin/js/alpaca/bootstra-locale-ru.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('/admin/js/alpaca/bootstrap-datetimepicker.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('/admin/js/alpaca/handlebars.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('/admin/js/alpaca/alpaca.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('/admin/js/alpaca/alpaca_ru.js', ['depends' => 'yii\web\JqueryAsset']);

$types = JSON_ENCODE($types);
$cities = JSON_ENCODE($cities);

$js =<<<JS

    var schema = {
        "schema" : {

            "type" : "object",
            "properties" : {
                "reports" : {
                    "type" : "array",
                    "minItems": 1,
                    "maxItems": 10,
                    "items" : {
                        "type" : "object",
                        "properties" : {
                            "date" : {
                                "title" : "Дата",
                                "type" : "string",
                                "required" : true
                            },
                            "city" : {
                                "title" : "Город",
                                "type" : "string",
                                "enum": Object.values($cities),
                                "required" : true
                            },
                            "type" : {
                                "title" : "Тип отчета",
                                "type" : "string",
                                "enum": Object.values($types),
                                "required" : true
                            },
                            "file" : {
                                "title" : "Файл",
                                "type": "string",
                                "format": "uri"
                            }
                        }
                    }
                }
            }
        },
        "options": {
            "toolbarSticky": true,
            "toolbarPosition": "bottom",
            "form": {
                "attributes": {
                    "method": "post",
                    "action": "/admin/reports/create-multiple",
                    "enctype": "multipart/form-data"
                },
                "buttons": {
                    "submit": {
                        "title": "Загрузить!"
                    }
                }
            },
            "fields" : {
                "reports" : {
                    "items" : {
                        "fields" : {
                            "date" : {
                                "type" : "date",
                                "picker": {
                                    "format": "MM.YYYY",
                                    "locale": "ru"
                                }
                            },
                            "city" : {
                                "type" : "select"
                            },
                            "type" : {
                                "type" : "select"
                            },
                            "file" : {
                                "type" : "file"
                            }
                        }
                    }
                }
            }
        },
        "view": {
            "locale": "ru_RU"
        }
    }

    $('#reports').alpaca(schema);

JS;
$this->registerJs($js, View::POS_END);

?>


<?=Html::beginForm(['create-multiple'], 'post', ['enctype' => 'multipart/form-data'])?>

    <div class="row">
        <div class="col-sm-8"><div id="reports"></div></div>
    </div>

<!-- <?=Html::submitButton("Загрузить")?> -->
<?=Html::endForm()?>
