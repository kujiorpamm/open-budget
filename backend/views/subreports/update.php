<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Subreports */

$this->title = 'Update Subreports: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Subreports', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="subreports-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
