<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Subreports */

$this->title = 'Добавить подотчет';
$this->params['breadcrumbs'][] = ['label' => 'Subreports', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subreports-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
