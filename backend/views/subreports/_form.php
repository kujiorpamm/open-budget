<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model common\models\Subreports */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="row">
    <div class="col-sm-6">
        <div class="subreports-form">

            <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>

            <?= $form->field($model, 'date')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Укажите дату'],
                'language' => 'ru',
                'pluginOptions' => [
                    'autoclose'=>true,
                    'viewMode' => 'months',
                    'minViewMode' => 'months',
                ]
            ]); ?>

            <?= $form->field($model, 'file')->widget(FileInput::classname(), [
                'language' => 'ru',
                'options' => ['accept' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'],
                'pluginOptions' => [
                    'showPreview' => false,
                    'showCaption' => true,
                    'showRemove' => true,
                    'showUpload' => false
                ]
            ]); ?>

            <div class="form-group">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>

    </div>
</div>
