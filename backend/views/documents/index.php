<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Документы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="documents-index">

    <p>
        <?= Html::a('Добавить документ', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'type.name',
            'name',
            'language',
            [
                'attribute' => 'path',
                'format' => 'raw',
                'value' => function($model) {
                    return Html::a("<i class='glyphicon glyphicon-link'>Скачать</i>", $model->path, ['target' => '_blank']);
                }
            ],
            //'ext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
