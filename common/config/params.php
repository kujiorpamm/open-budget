<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'languages' => [
        'Рус' => 'ru',
        'Қаз' => 'kk'
    ]
];
