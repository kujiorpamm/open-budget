<?php

namespace common\models;

use Yii;
use common\models\helpers\ExcellImport;
/**
 * This is the model class for table "reports".
 *
 * @property int $id
 * @property string $date
 * @property int $type_id
 * @property int $city_id
 * @property string $filename
 * @property string $data
 */
class Reports extends \yii\db\ActiveRecord
{



    public $file;


    public static function tableName()
    {
        return 'reports';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date', 'date_fact', 'type_id', 'city_id', 'filename'], 'required'],
            [['date'], 'safe'],
            [['status'], 'boolean'],
            [['type_id', 'city_id'], 'integer'],
            [['data'], 'string'],
            [['filename'], 'string', 'max' => 128],
            [['file'], 'file', 'skipOnEmpty' => false, 'extensions' => ['xls', 'xlsx'], 'on' => 'create'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'date' => Yii::t('app', 'Дата'),
            'type_id' => Yii::t('app', 'Тип'),
            'city_id' => Yii::t('app', 'Город'),
            'filename' => Yii::t('app', 'Файл'),
            'data' => Yii::t('app', 'Информация'),
            'type.name' => Yii::t('app', 'Тип'),
            'city.name' => Yii::t('app', 'Город'),
        ];
    }

    public function getType() {
        return $this->hasOne(ReportTypes::className(), ['id'=>'type_id']);
    }

    public function getCity() {
        return $this->hasOne(Cities::className(), ['id'=>'city_id']);
    }

    public function getItems() {
        return $this->hasMany(ReportItems::className(), ['report_id' => 'id']);
    }

    public function getIncomes() {
        return ReportItems::find()->innerJoin('report_item_categories cat', 'cat.id = report_items.category_id')
            ->where(['=', 'report_id', $this->id])->andWhere(['=', 'cat.fullname', 'ДОХОДЫ'])->one();
    }

    public function getOutcomes() {
        return ReportItems::find()->innerJoin('report_item_categories cat', 'cat.id = report_items.category_id')
            ->where(['=', 'report_id', $this->id])->andWhere(['=', 'cat.fullname', 'ЗАТРАТЫ'])->one();
    }


    public function getTree() {
        // $models = self::find()->innerJoin('report_item_categories cats', ['report_items.category_id = cats.id'])
        //     ->innerJoin('report_item_categories subcats', ['subcats.parent_id = cats.id'])
        //     ->where('report_items.report_id = ')

        return self::nodetree();

    }

    public function shortTree() {
        if($this->data) return json_decode($this->data, true);
        $refs = array();
        $list = array();

        $nodes = Yii::$app->db->createCommand('select * from report_item_categories')->queryAll();

        foreach ($nodes as $data) {
            $thisref = &$refs[ $data['id'] ];
            $thisref['id'] = $data['id'];
            $thisref['parentid'] = $data['parent_id'];
            $thisref['name'] = $data['name_ru'];
            $model = ReportItems::find()->where(['=', 'category_id', $data['id']])->andWhere(['=', 'report_id', $this->id])->one();
            if($model) {
                $thisref['model'] = [
                    'budget_corrected' => $model->budget_corrected,
                    'budget_execution' => $model->budget_execution,
                    'budget_execution_fail' => ($model->budget_execution - $model->budget_corrected),
                    'budget_execution_percent' => $model->budget_execution_percent,
                    'budget_execution_percent_total' => $model->budget_execution_percent_total
                ];
            }
            if ($data['parent_id'] == NULL) {
                $list[ $data['id'] ] = &$thisref;
            } else if( isset($thisref['model']) && $thisref['model'] ){
                $refs[ $data['parent_id'] ]['children'][ $data['id'] ] = &$thisref;
            }
        }

        $this->data = json_encode($list);
        $this->save();
        return $list;
    }

    public function nodetree($param=array()) {
        $refs = array();
        $list = array();

        $nodes = Yii::$app->db->createCommand('select * from report_item_categories')->queryAll();

        foreach ($nodes as $data) {
            $thisref = &$refs[ $data['id'] ];
            $thisref['parentid'] = $data['parent_id'];
            $thisref['name'] = $data['name_ru'];
            $thisref['model'] = ReportItems::find()->where(['=', 'category_id', $data['id']])->andWhere(['=', 'report_id', $this->id])->all();
            if ($data['parent_id'] == NULL) {
                $list[ $data['id'] ] = &$thisref;
            } else if($thisref['model']){
                $refs[ $data['parent_id'] ]['children'][ $data['id'] ] = &$thisref;
            }
        }
        return $list;
    }

    public function nodetreeArr() {
        $refs = array();
        $list = array();

        $nodes = Yii::$app->db->createCommand('select * from report_item_categories')->queryAll();

        foreach ($nodes as $data) {
            $thisref = &$refs[ $data['id'] ];
            $thisref['parentid'] = $data['parent_id'];
            $thisref['name'] = $data['name_ru'];
            $thisref['model'] = ReportItems::find()->where(['=', 'category_id', $data['id']])->andWhere(['=', 'report_id', $this->id])->all();
            if ($data['parent_id'] == NULL) {
                $list[] = &$thisref;
            } else if($thisref['model']){
                $refs[ $data['parent_id'] ]['children'][  ] = &$thisref;
            }
        }
        return $list;
    }

    public function processExcel() {

        ExcellImport::prepareCategories();
        return ExcellImport::processFile($this);
    }

    public function beforeDelete() {
        if(parent::beforeDelete()) {
            $res = \yii\helpers\FileHelper::unlink( Yii::getAlias("@frontend") . '/web' . $this->filename );
            ReportItems::deleteAll(['report_id'=>$this->id]);
            return true;
        }
        return false;
    }

}
