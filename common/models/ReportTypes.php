<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "report_types".
 *
 * @property int $id
 * @property string $name_ru
 * @property string $name_kz
 */
class ReportTypes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'report_types';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name_ru', 'name_kz'], 'required'],
            [['name_ru', 'name_kz'], 'string', 'max' => 128],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name_ru' => Yii::t('app', 'Name Ru'),
            'name_kz' => Yii::t('app', 'Name Kz'),
        ];
    }

    public function getName() {
        return $this->name_ru;
    }

    static function getList() {
        return self::find()->indexBy('id')->select('name_ru')->column();
    }

}
