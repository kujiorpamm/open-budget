<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "cities".
 *
 * @property int $id
 * @property string $name_ru
 * @property string $name_en
 */
class Cities extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cities';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name_ru'], 'required'],
            [['name_ru', 'name_en'], 'string', 'max' => 128],
            [['data_ru', 'data_kz'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name_ru' => 'Name Ru',
            'name_en' => 'Name En',
            'data' => 'Информация о регионе',
        ];
    }

    public function getName() {
        return $this->name_ru;
    }

    static function getList() {
        return self::find()->indexBy('id')->select('name_ru')->column();
    }
}
