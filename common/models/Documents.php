<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "documents".
 *
 * @property int $id
 * @property int $type_id
 * @property string $name
 * @property string $language
 * @property string $path
 * @property string $ext
 *
 * @property Documentstype_ids $type_id0
 */
class Documents extends \yii\db\ActiveRecord
{

    public $file;

    public static function tableName()
    {
        return 'documents';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_id', 'name', 'language', 'path', 'ext'], 'required'],
            [['type_id'], 'integer'],
            [['name', 'path'], 'string', 'max' => 512],
            [['language'], 'string', 'max' => 2],
            [['ext'], 'string', 'max' => 16],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => DocumentsTypes::className(), 'targetAttribute' => ['type_id' => 'id']],
            [['file'], 'file', 'skipOnEmpty' => false, 'on' => 'create'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type_id' => Yii::t('app', 'Тип'),
            'name' => Yii::t('app', 'Наименование'),
            'language' => Yii::t('app', 'Язык'),
            'path' => Yii::t('app', 'Путь'),
            'ext' => Yii::t('app', 'Формат'),
            'file' => Yii::t('app', 'Файл'),
            'type.name' => Yii::t('app', 'Тип'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(DocumentsTypes::className(), ['id' => 'type_id']);
    }
}
