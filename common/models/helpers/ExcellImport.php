<?php
namespace common\models\helpers;

use Yii;
use yii\base\Model;
use common\models\Reports;
use common\models\ReportItems;
use common\models\ReportItemCategories;


class ExcellImport extends Model {

    const GENERAL = ['ДОХОДЫ', 'ЗАТРАТЫ', 'ЧИСТОЕ БЮДЖЕТНОЕ КРЕДИТОВАНИЕ', 'САЛЬДО ПО ОПЕРАЦИЯМ С ФИНАНСОВЫМИ АКТИВАМИ', 'ДЕФИЦИТ (ПРОФИЦИТ) БЮДЖЕТА', 'ФИНАНСИРОВАНИЕ ДЕФИЦИТА (ИСПОЛЬЗОВАНИЕ ПРОФИЦИТА) БЮДЖЕТА', 'БЮДЖЕТНЫЕ КРЕДИТЫ', 'ПОГАШЕНИЕ БЮДЖЕТНЫХ КРЕДИТОВ', 'ПРИОБРЕТЕНИЕ ФИНАНСОВЫХ АКТИВОВ'];
    const RESERVED = ['БЮДЖЕТНЫЕ КРЕДИТЫ', 'ПОГАШЕНИЕ БЮДЖЕТНЫХ КРЕДИТОВ', 'ПРИОБРЕТЕНИЕ ФИНАНСОВЫХ АКТИВОВ'];

    public $model;

    static function prepareCategories() {
        foreach (self::GENERAL as $cat_name) {
            $model = ReportItemCategories::findOne(['name_ru' => $cat_name]);
            if($model) continue;

            $model = new ReportItemCategories();
            $model->name_ru = $cat_name;
            $model->fullname = $cat_name;
            $model->save();
        }
    }

    static function processFile($model){

        $parser = new self();

        $parser->model = $model;
        return $parser->parse();

    }

    private function parse() {
        // try {
            $data = \moonland\phpexcel\Excel::import(Yii::getAlias('@frontend/web') . $this->model->filename,
                [
                   'setFirstRecordAsKeys' => false,
                   'setIndexSheetByName' => false,
                   'getOnlySheet' => 'Отчет'
                ]
            );

        // } catch (\Exception $e) {
        //     return ['status' => false, 'error' => $e];
        // }

        ReportItems::deleteAll(['report_id'=>$this->model->id]);

        $items = [];
        $struct = [];
        $count = 1; // Строка в excell
        $status = true; // Конечный результат обработки

        // try {

            foreach ($data as $row) {
                $struct = $this->process_struct($row, $struct, $count);
                $count ++;
            }

        // } catch (\Exception $e) {
        //     return ['status' => false, 'error' => $e , 'row' => $count];
        // }

        $this->model->status = 1;
        $this->model->save();

        return ['status' => true];
    }

    private function process_struct($row, $struct, $count) {
        $it_is = false;

        if( in_array($row['G'], [
            'I. ДОХОДЫ',
            'II. ЗАТРАТЫ',
            'III. ЧИСТОЕ БЮДЖЕТНОЕ КРЕДИТОВАНИЕ',
            'IV. САЛЬДО ПО ОПЕРАЦИЯМ С ФИНАНСОВЫМИ АКТИВАМИ',
            'V. ДЕФИЦИТ (ПРОФИЦИТ) БЮДЖЕТА',
            'VI. ФИНАНСИРОВАНИЕ ДЕФИЦИТА (ИСПОЛЬЗОВАНИЕ ПРОФИЦИТА) БЮДЖЕТА',
            // 'ПРИОБРЕТЕНИЕ ФИНАНСОВЫХ АКТИВОВ',
            // 'БЮДЖЕТНЫЕ КРЕДИТЫ',
            ]) || in_array($row['G'], self::RESERVED) ) {

                    if(in_array($row['G'], self::RESERVED)) { /////////////////
                        $struct = [$row['G']];
                    } else {
                        $struct = [ explode('. ', $row['G'])[1] ];
                    }
                    $it_is = true;
            }

        $_code = implode( [ $row['A'], $row['B'], $row['C'], $row['D'], $row['E'], $row['F'] ] );
        if (empty($struct)) return $struct;   // Если еще не указана главная категория

        if(!$it_is) {
            if(!$_code || !ctype_digit($_code)) return $struct;           // Если категория есть, но нет кода
        }

        if($row['A']) {
            $struct = [$struct[0], $row['A']];
        } else if ($row['B']) {
            $struct = [$struct[0], $struct[1], $row['B']];
        } else if ($row['C']) {
            $struct = [$struct[0], $struct[1],$struct[2], $row['C']];
        } else if ($row['D']) {
            $struct = [$struct[0], $struct[1],$struct[2], $struct[3], $row['D']];
        } else if(!$it_is) {
            return $struct;
        }

        // Если это строка 2го уровня (1й в базе вручную), нужно ее сохранить
        if ( count($struct) > 0 ) $this->process_data($struct, $count, $row);

        return $struct;
    }

    private function process_data($struct, $count, $row) {
        // Сначала проверить, есть ли такой тип в базе уже
        $item_category = ReportItemCategories::findOne(['fullname' => implode($struct, '-')]);
        // Если его нет, то создать
        if(!$item_category) {
            $name = array_pop($struct);
            $parent = ReportItemCategories::findOne(['fullname' => implode($struct, '-')]);
            array_push($struct, $name);
            if($parent) {

                if($row['G'] == "") return;                                     // Если нет имени, просто пропустить

                $item_category = new ReportItemCategories();
                $item_category->parent_id = $parent->id;
                $item_category->code = 'und';
                $item_category->name_ru = $row['G'];
                $item_category->fullname = implode($struct, '-');
                if(!$item_category->save()) {
                    throw new \Exception("Ошибка. Сохранение новой категории не происходит. Строка в файле отчета - $count .", 1);
                }
            } else {
                throw new \Exception("Ошибка. Не найдена родительская категория. Строка в файле отчета - $count .", 1);
            }
        }

        // Затем его использовать для сохранения
        $item_model = new ReportItems();
        $item_model->report_id = $this->model->id;
        $item_model->category_id = $item_category->id;
        $item_model->budget_approved = floatval(str_replace(' ', '', str_replace(',', '', $row['H']) ));
        $item_model->budget_refined = floatval(str_replace(' ', '', str_replace(',', '', $row['I'])));
        $item_model->budget_corrected = floatval(str_replace(' ', '', str_replace(',', '', $row['J'])));
        $item_model->plan_payments = floatval(str_replace(' ', '', str_replace(',', '', $row['K'])));
        $item_model->plan_obligations = floatval(str_replace(' ', '', str_replace(',', '', $row['L'])));
        $item_model->obligations_accepted = floatval(str_replace(' ', '', str_replace(',', '', $row['N'])));
        $item_model->obligations_unpaid = floatval(str_replace(' ', '', str_replace(',', '', $row['O'])));
        $item_model->budget_execution = floatval(str_replace(' ', '', str_replace(',', '', $row['P'])));
        $item_model->budget_execution_percent = floatval(str_replace(' ', '', str_replace(',', '', $row['Q'])));
        $item_model->budget_execution_percent_total = floatval(str_replace(' ', '', str_replace(',', '', $row['R'])));


        if( !$item_model->save() ) {
            throw new \Exception("Ошибка. Не сохраняется информация содержащаяся в строке. Строка в файле отчета - $count .", 1);
            // echo "<pre>"; print_r("Ошибка 3"); echo "</pre>"; exit;
        }

        return true;
    }

}
