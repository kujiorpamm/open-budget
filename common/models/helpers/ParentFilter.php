<?php
namespace common\models\helpers;

use Yii;
use yii\base\Model;
use common\models\Reports;
use common\models\ReportItems;
use common\models\ReportItemCategories;
use common\models\Settings;

class ParentFilter extends Model {

    public $city;
    public $coeficient = 1;
    // public $from;
    // public $to;
    public $year;
    public $month;

    public $settings;
    public $report;
    public $prevs;

    public function rules() {
        return [
            [['city', 'coeficient', 'year', 'month'], 'required']
        ];
    }

    public function getPieChartData($items, $category_id) {

        $report = $this->findModel();

        // Типы доходов, которые нужно найти

        $labels = [];
        $values = [];
        $data = [];

        if(!$report->id) {
            $data[] = [
                'name' => Yii::t('app', 'Нет данных'),
                'value' => '1'
            ];
        } else {
            foreach ($items as $key => $item) {
                $category = ReportItemCategories::findOne(['id' => $item['id']]);
                $ritem = ReportItems::find()->where(['report_id' => $report->id, 'category_id' => $category->id])->one();
                if($ritem) {
                    $data[] = [
                        'name' => $category->getName(),
                        'value' => number_format($ritem->budget_execution * $this->coeficient, 2, '.', '')
                    ];
                } else {
                    $data[] = [
                        'name' => $category->getName(),
                        'value' => "0"
                    ];
                }
            }
        }

        try {
            $total_income = ReportItems::find()->where(['report_id' => $report->id, 'category_id' => $category_id])->one();
            $total_income = number_format($total_income->budget_execution * $this->coeficient, 2, '.', '');
        } catch (\Exception $e) {
            $total_income = 0;
        }

        return ['data' => $data, 'total_income' => $total_income];

    }

    public function getStackedBarChart($income_subcategories, $is_main_cat = false) {
        $report = $this->findModel();
        $data = [
            'data' => [],
            'groups' => []
        ];

        // Всего 3 отчета в массива
        $total_reports = count($this->prevs);
        foreach ($this->prevs as $key => $current_report) {


            if($key == ($total_reports -1)) {
                $data['data'][$key]['date'] = Yii::t('app', '{0} план.', [Yii::$app->formatter->asDate($current_report->date, 'php:Y')]);
            } else {
                $data['data'][$key]['date'] = Yii::t('app', '{0} факт.', [Yii::$app->formatter->asDate($current_report->date, 'php:Y')]);
            }

            if($is_main_cat) {
                $subcategories = $income_subcategories;
                foreach ($subcategories as $kkkey => $subcat) {
                    $data['groups'][] = $subcat->getName();
                    $stats = ReportItems::find()->where(['report_id' => $current_report->id, 'category_id' => $subcat->id])->one();

                    if($key == ($total_reports - 1)) {
                        if($stats) {
                            $data['data'][$key][$subcat->getName()] = $stats->plan_payments * $this->coeficient;
                            $data['data'][$total_reports][$subcat->getName()] = $stats->budget_execution * $this->coeficient;
                        } else {
                            $data['data'][$key][$subcat->getName()] = 0;
                            $data['data'][$total_reports][$subcat->getName()] = 0;
                        }
                    } else {
                        if($stats) {
                            $data['data'][$key][$subcat->getName()] = $stats->budget_execution * $this->coeficient;
                        } else {
                            $data['data'][$key][$subcat->getName()] = 0;
                        }
                    }
                }
            } else {
                foreach ($income_subcategories as $kkey => $rc) {
                    $subcategories = ReportItemCategories::find()->where(['parent_id' => $rc->id])->all();
                    foreach ($subcategories as $kkkey => $subcat) {
                        $data['groups'][] = $subcat->getName();
                        $stats = ReportItems::find()->where(['report_id' => $current_report->id, 'category_id' => $subcat->id])->one();

                        if($key == ($total_reports - 1)) {
                            if($stats) {
                                $data['data'][$key][$subcat->getName()] = $stats->plan_payments * $this->coeficient;
                                $data['data'][$total_reports][$subcat->getName()] = $stats->budget_execution * $this->coeficient;
                            } else {
                                $data['data'][$key][$subcat->getName()] = 0;
                                $data['data'][$total_reports][$subcat->getName()] = 0;
                            }
                        } else {
                            if($stats) {
                                $data['data'][$key][$subcat->getName()] = $stats->budget_execution * $this->coeficient;
                            } else {
                                $data['data'][$key][$subcat->getName()] = 0;
                            }
                        }
                    }

                }
            }
        }

        $data['groups'] = array_unique($data['groups']);
        $data['data'][$total_reports]['date'] =  Yii::t('app', '{0} факт.', [Yii::$app->formatter->asDate($this->prevs[$total_reports - 1] ->date, 'php:Y')]);

        foreach ($this->prevs as $prev) {
            $data['labels'][] = Yii::$app->formatter->asDate($prev->date, 'php:Y');
        }

        $data['labels'][] = end($data['labels']);
        for($i = 0; $i < count($data['labels']); $i ++ ) {
            if($i == count($data['labels']) - 2) {
                $data['labels'][$i] =  Yii::t('app', '{0} план.', [ $data['labels'][$i]]);
            } else {
                $data['labels'][$i] =  Yii::t('app', '{0} факт.', [$data['labels'][$i]]);
            }
        }

        return $data;

    }

    public function getTableData($income_categories, $income_subcategories) {
        $data = [];
        // 1я строка
        $data[0]['data'] = [ Yii::t('app', 'Показатель')];
        foreach ($this->prevs as $kkey => $prev_report) {
            $data[0]['data'][] = Yii::$app->formatter->asDate($prev_report->date, 'php:Y');
        }
        // 2я строка
        $data[1]['data'] = [Yii::t('app', 'факт.'), Yii::t('app', 'план.'), Yii::t('app', 'факт.'), Yii::t('app', 'план.'), Yii::t('app', 'план.'), Yii::t('app', 'факт.'), Yii::t('app', '% исп.')];

        $total_reports = count($this->prevs);
        foreach ($income_subcategories as $key => $rc) {
            // НАЛОГОВЫЕ ПОСТУПЛЕНИЯ
            $_data = [
                'header' => true,
                'data' => [$rc->getName()]
            ];
            foreach ($this->prevs as $kkey => $prev_report) {
                $report_item = ReportItems::find()->where(['report_id' => $prev_report->id, 'category_id' => $rc->id])->one();

                if($kkey == ($total_reports -1)) {
                    try {
                        $_data['data'][] = $report_item->plan_payments * $this->coeficient;
                        $_data['data'][] = $report_item->budget_execution * $this->coeficient;
                        $_data['data'][] = $report_item->budget_execution_percent;
                    } catch (\Exception $e) {
                        $_data['data'][] = 0;
                        $_data['data'][] = 0;
                        $_data['data'][] = 0;
                    }
                } else {
                    try {
                        $_data['data'][] = $report_item->budget_execution * $this->coeficient;
                        $_data['data'][] = $report_item->plan_payments * $this->coeficient;
                    } catch (\Exception $e) {
                        $_data['data'][] = 0;
                        $_data['data'][] = 0;
                    }
                }
            }
            $data[] = $_data;


            $subcategories = ReportItemCategories::find()->where(['parent_id' => $rc->id])->all();
            foreach ($subcategories as $kkey => $subcat) {
                $_data = [
                    'header' => false,
                    'data' => [$subcat->getName()]
                ];
                foreach ($this->prevs as $kkey => $prev_report) {
                    $report_item = ReportItems::find()->where(['report_id' => $prev_report->id, 'category_id' => $subcat->id])->one();
                    if(!$report_item) {
                        if($kkey == ($total_reports -1)) {
                            $_data['data'][] = 0;
                            $_data['data'][] = 0;
                            $_data['data'][] = 0;
                        } else {
                            $_data['data'][] = 0;
                            $_data['data'][] = 0;
                        }
                    } else {
                        if($kkey == ($total_reports -1)) {
                            $_data['data'][] = $report_item->budget_execution * $this->coeficient;
                            $_data['data'][] = $report_item->plan_payments * $this->coeficient;
                            $_data['data'][] = $report_item->budget_execution_percent;
                        } else {
                            $_data['data'][] = $report_item->budget_execution * $this->coeficient;
                            $_data['data'][] = $report_item->plan_payments * $this->coeficient;
                        }
                    }

                }
                $data[] = $_data;
            }
        }
        // ВСЕГО
        $_data = [
            'header' => true,
            'data' => [
                Yii::t('app', 'ВСЕГО')
            ]
        ];
        $total_reports = count($this->prevs);
        foreach ($this->prevs as $kkey => $prev_report) {
            $r = ReportItems::find()->where(['report_id' => $prev_report->id, 'category_id' => $income_categories->id])->one();
            // $_data['data'][] =
            try {
                if($kkey == ($total_reports -1)) {
                    $_data['data'][] = $r->budget_execution * $this->coeficient;
                    $_data['data'][] = $r->plan_payments * $this->coeficient;
                    $_data['data'][] = $r->budget_execution_percent;
                } else {
                    $_data['data'][] = $r->budget_execution * $this->coeficient;
                    $_data['data'][] = $r->plan_payments * $this->coeficient;
                }
            } catch (\Exception $e) {
                if($kkey == ($total_reports -1)) {
                    $_data['data'][] = 0;
                    $_data['data'][] = 0;
                    $_data['data'][] = 0;
                } else {
                    $_data['data'][] = 0;
                    $_data['data'][] = 0;
                }
            }

        }

        $data[] = $_data;

        return $data;

    }

    public function findModel() {
        if(!$this->report) {
            $this->report = Reports::find()
                ->where(['type_id' => 2])                                           // Полный отчет
                ->andWhere(['city_id'=> $this->city])                                 // Город
                ->andWhere(['YEAR(date_fact)' => $this->year])                        // Год
                ->andWhere(['MONTH(date)' => $this->month])                       // Месяц
                ->orderBy("date DESC")
                ->one();
            if(!$this->report) $this->report = $this->getEmptyReport($this->year . "-" . $this->month . "-01");


            $year = Yii::$app->formatter->asDate($this->report->date, 'php:Y');
            for($i = 2; $i > 0; $i--) {
                $search_year = $year - $i;
                $prev_report = Reports::find()
                    ->where(['type_id' => 2])                                           // Полный отчет
                    ->andWhere(['city_id'=> $this->city])                                 // Город
                    ->andWhere(['YEAR(date)' => $search_year])                        // Год
                    ->andWhere(['MONTH(date)' => $this->month])                       // Месяц
                    ->orderBy("date DESC")
                    ->one();
                if($prev_report) {
                    $this->prevs[] = $prev_report;
                } else {
                    $this->prevs[] = $this->getEmptyReport("{$search_year}-{$this->month}-01");
                }
            }
            $this->prevs[] = $this->report;
        }
        return $this->report;
    }

    private function getEmptyReport($date) {
        $report = new Reports();
        $report->date = $date;
        $report->city_id = $this->city;
        return $report;
    }

    private function getFormatted($number) {
        return \common\components\Helper::formatNumber($number * $this->coeficient);
    }
    private function getMultiplied($item) {
        if($item) {
            return \common\components\Helper::formatNumber($item->budget_execution * $this->coeficient);
        } else {
            return 0;
        }
    }

    public function getYears() {
        $_years = array_column(Yii::$app->db->createCommand("SELECT YEAR(date) as year FROM reports GROUP BY YEAR(date) ORDER BY YEAR(date) DESC")->queryAll(), 'year');
        $years = [];
        $years[date('Y')] = date('Y');
        foreach ($_years as $value) {
            $years[$value] = $value;
        }
        return $years;
    }

}
