<?php
namespace common\models\helpers;

use Yii;
use yii\base\Model;
use common\models\helpers\ParentFilter;
use common\models\Reports;
use common\models\ReportItems;
use common\models\ReportItemCategories;
use common\models\Settings;


class FilterOutcome extends ParentFilter {

    public function getPieChart() {
        $settings = Settings::findOne(['name' => 'index_cats']);
        $settings = JSON_DECODE($settings->value, true);

        $report = $this->findModel();

        // Типы доходов, которые нужно найти
        $items = [
            ['id' => $settings['outcome_gos_obsh'][0]['id'], 'model' => []],
            ['id' => $settings['outcome_oborona'][0]['id'], 'model' => []],
            ['id' => $settings['outcome_oborona'][1]['id'], 'model' => []],
            ['id' => $settings['outcome_social'][0]['id'], 'model' => []],
            ['id' => $settings['outcome_social'][1]['id'], 'model' => []],
            ['id' => $settings['outcome_social'][2]['id'], 'model' => []],
            ['id' => $settings['outcome_social'][3]['id'], 'model' => []],
            ['id' => $settings['outcome_real'][0]['id'], 'model' => []],
            ['id' => $settings['outcome_real'][1]['id'], 'model' => []],
            ['id' => $settings['outcome_real'][2]['id'], 'model' => []],
            ['id' => $settings['outcome_real'][3]['id'], 'model' => []],
            ['id' => $settings['outcome_real'][4]['id'], 'model' => []],
            ['id' => $settings['outcome_transf'][0]['id'], 'model' => []],
            ['id' => $settings['outcome_zaim'][0]['id'], 'model' => []],
            ['id' => $settings['outcome_other'][0]['id'], 'model' => []],
            ['id' => $settings['outcome_credit'][0]['id'], 'model' => []],
            ['id' => $settings['outcome_fin_activ'][0]['id'], 'model' => []],
        ];

        $result = $this->getPieChartData($items, $settings['outcome_category_id'][0]['id']);
        $report = $this->findModel();
        $income_categories = ReportItemCategories::find()->where(['id' => $settings['outcome_category_id'][0]['id']])->one();
        $income_subcategories = ReportItemCategories::find()->where(['parent_id' => $income_categories->id])->all();

        return [
            'status' => true,
            'total_income' => Yii::t('app', 'Всего расходов: {0}', [$result['total_income']]),
            // 'type' => 'pie',
            'date' => Yii::t('app', 'По состоянию на {0}', [Yii::$app->formatter->asDate($report->date, 'php:d.m.Y')]),
            'data_pie' => $result['data'],
            'data_bars' => $this->getStackedBarChart($income_subcategories),
            'data_tables' => $this->getTableData($income_categories, $income_subcategories)
        ];
    }

}
