<?php
namespace common\models\helpers;

use Yii;
use yii\base\Model;
use common\models\helpers\ParentFilter;
use common\models\Reports;
use common\models\ReportItems;
use common\models\ReportItemCategories;
use common\models\Settings;


class FilterIncome extends ParentFilter {

    public function getPieChart() {
        $settings = Settings::findOne(['name' => 'index_cats']);
        $settings = JSON_DECODE($settings->value, true);
        $items = [
            ['id' => $settings['income_taxes'][0]['id'], 'model' => []],
            ['id' => $settings['income_nontaxes'][0]['id'], 'model' => []],
            ['id' => $settings['income_osn_kap'][0]['id'], 'model' => []],
            ['id' => $settings['income_transferts'][0]['id'], 'model' => []],
        ];

        $settings = Settings::findOne(['name' => 'index_cats']);
        $settings = JSON_DECODE($settings->value, true);
        $result = $this->getPieChartData($items, $settings['income_category_id'][0]['id']);
        $income_categories = ReportItemCategories::find()->where(['id' => $settings['income_category_id'][0]['id']])->one();
        $income_subcategories = ReportItemCategories::find()->where(['parent_id' => $income_categories->id])->all();


        return [
            'status' => true,
            'total_income' => Yii::t('app', 'Всего доходов: {0}', [$result['total_income']]),
            'date' => Yii::t('app', 'По состоянию на {0}', [Yii::$app->formatter->asDate($this->report->date, 'php:d.m.Y')]),
            'data_pie' => $result['data'],
            'data_bars' => $this->getStackedBarChart($income_subcategories),
            'data_tables' => $this->getTableData($income_categories, $income_subcategories)
        ];
    }


}
