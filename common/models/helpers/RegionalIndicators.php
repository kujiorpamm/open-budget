<?php
namespace common\models\helpers;

use Yii;
use yii\base\Model;
use common\models\Reports;
use common\models\Subreports;
use common\models\ReportItems;
use common\models\ReportItemCategories;
use common\models\Settings;
use common\models\Cities;

class RegionalIndicators extends Model {

    // public $city;
    public $coeficient = 1;
    public $from;
    public $to;
    public $category_id;

    public $reports;

    public function rules() {
        return [
            [['category_id', 'coeficient', 'from', 'to'], 'required'],
            [['reports'], 'safe']
        ];
    }

    public function getData() {


        $reports = $this->findModels();
        $subreport = Subreports::find()
            // ->where(['YEAR(date)' => date('Y')])
            // ->andWhere(['MONTH(date)' => date('m')])
            ->orderBy('date DESC')
            ->one();
        $data = [];

        foreach ($reports as $key => $report) {
            $data['datasets'][] = $this->getReportDataForMap($report);
        }

        $data['measure_x'] = ' тг.';
        $data['measure_y'] = ' тг.';

        $data['subreports'] = $subreport ? JSON_DECODE($subreport->data) : [];
        $data['date1'] = Yii::t('app', 'По состоянию на {0}', [ Yii::$app->formatter->asDate($reports[0]->date, 'php:d.m.Y') ]);
        $data['date2'] = Yii::t('app', 'По состоянию на {0}', [ Yii::$app->formatter->asDate(($subreport) ? $subreport->date : "", 'php:d.m.Y') ]);



        return $data;

        return [
            'datasets' => [
                [
                    'data' => [
                        [
                            'x' => 2000000, 'y' => '2018 г', 'z' => 'Тупкараганский район'
                        ]
                    ],
                    'label' => 'Тупкараганский'
                ]
            ],
            'table_data' => '',
            'measure_x' => "t",
            'measure_y' => "s",
        ];
    }

    private function getReportDataForMap($report) {
        try {
            $report_item = ReportItems::find()->where(['report_id' => $report->id, 'category_id' => $this->category_id])->one();
            return [
                'data' => [
                    [
                        'x' => $report_item->budget_execution * $this->coeficient,
                        'y' => Yii::t('app', 'Дата: {0}', [Yii::$app->formatter->asDate($report->date, 'php: m.Y')]),
                        'z' => $report->city->name_ru,
                        'n' => $report->city->data_ru
                    ]
                ],
                'label' => $report->city->name_ru
            ];
        } catch (\Exception $e) {
            return [
                'data' => [
                    [
                        'x' => 0,
                        'y' => Yii::$app->formatter->asDate($report->date, 'php: m.Y'),
                        'z' => $report->city->name_ru
                    ]
                ],
                'label' => $report->city->name_ru
            ];
        }

    }

    public function findModels() {
        $cities = Cities::find()->all();

        foreach ($cities as $city) {
            $report = Reports::find()
                ->where(['type_id' => 2])                                           // Полный отчет
                ->andWhere(['city_id'=> $city->id])                                 // Город
                ->andWhere(['MONTH(date)' => $this->to])                       // Месяц
                ->orderBy("date DESC")
                ->one();
            if($report) $this->reports[] = $report;
        }

        return $this->reports;

    }

}
