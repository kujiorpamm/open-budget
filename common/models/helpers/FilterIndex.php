<?php
namespace common\models\helpers;

use Yii;
use yii\base\Model;
use common\models\helpers\ParentFilter;
use common\models\Reports;
use common\models\ReportItems;
use common\models\ReportItemCategories;
use common\models\Settings;


class FilterIndex extends ParentFilter {



    public function getIndexChartData() {
        $settings = Settings::findOne(['name' => 'index_cats']);
        $settings = JSON_DECODE($settings->value, true);
        $items = [
            ['id' => $settings['income_category_id'][0]['id'], 'model' => []],
            ['id' => $settings['outcome_category_id'][0]['id'], 'model' => []]
        ];

        $report = $this->findModel();

        if($report->id) {
            $income = ReportItems::find()
                ->where(['report_id'=>$report->id])
                ->andWhere(['category_id' => $settings['income_category_id'][0]['id']])
                ->one();
            $outcome = ReportItems::find()
                ->where(['report_id'=>$report->id])
                ->andWhere(['category_id' => $settings['outcome_category_id'][0]['id']])
                ->one();
            return [
                "report" => $report->filename,
                "income" => $income,
                "outcome" => $outcome,
                "date" => Yii::t("app", "По состоянию на {0} года", [Yii::$app->formatter->asDate($report->date, "php:d.m.Y")]),
                "coeficient" => $this->coeficient
            ];
        } else {
            return [
                "income" => [
                    "budget_corrected" => 0,
                    "budget_execution" => 0,
                    "budget_execution_percent" => 0
                ],
                "outcome" => [
                    "budget_corrected" => 0,
                    "budget_execution" => 0,
                    "budget_execution_percent" => 0
                ],
                "date" => Yii::t("app", "По состоянию на {0} года", [date("d.m." . $this->year)])
            ];
        }
    }

    public function getResults() {
        return false;
        // Найти все отчеты по месяцу
        $reports = Reports::find()
            ->where(['type_id' => 2])                                           // Полный отчет
            ->andWhere(['city_id'=> $this->city])                                 // Город
            ->andWhere(['MONTH(date_fact)' => $this->to])                       // Месяц
            ->orderBy('date_fact')
            ->all();

        $report = Reports::find()
            ->where(['type_id' => 2])                                           // Полный отчет
            ->andWhere(['city_id'=> $this->city])                                 // Город
            ->andWhere(['YEAR(date_fact)' => date('Y')])                        // Год
            ->andWhere(['MONTH(date_fact)' => $this->to])                       // Месяц
            ->one();
        if($report) {
            $income = ReportItems::find()
                ->where(['report_id'=>$report->id])
                ->andWhere(['category_id' => Yii::$app->params['income_category_id']])
                ->one();
            $outcome = ReportItems::find()
                ->where(['report_id'=>$report->id])
                ->andWhere(['category_id' => Yii::$app->params['outcome_category_id']])
                ->one();
        }

        // ПОСТУПЛЕНИЯ
        $table_income = $this->getTable($reports, Yii::$app->params['incomes']);

        // РАСХОДЫ
        $table_outcome = $this->getTable($reports, Yii::$app->params['outcomes']);

        return [
            'income' => (isset($income)) ? \common\components\Helper::formatNumber($income->budget_execution * $this->coeficient) : 0,
            'outcome' => (isset($outcome)) ? \common\components\Helper::formatNumber($outcome->budget_execution * $this->coeficient) : 0,
            'income_table' => $table_income,
            'outcome_table' => $table_outcome
        ];

    }

    public function getTables() {
        $settings = Settings::findOne(['name' => 'index_cats']);
        $settings = JSON_DECODE($settings->value, true);
        $items = [
            ['id' => $settings['income_taxes'][0]['id'], 'model' => []],
            ['id' => $settings['income_nontaxes'][0]['id'], 'model' => []],
            ['id' => $settings['income_osn_kap'][0]['id'], 'model' => []],
            ['id' => $settings['income_transferts'][0]['id'], 'model' => []],
        ];

        $this->findModel();

        $table_income = [];
        $table_outcome = [];
        $table_income[] = [
            Yii::t("app", "Наименование"),
            Yii::t("app", "{0} факт", [Yii::$app->formatter->asDate($this->prevs[0]->date, "php:Y")]),
            Yii::t("app", "{0} факт", [Yii::$app->formatter->asDate($this->prevs[1]->date, "php:Y")]),
            Yii::t("app", "{0} план", [Yii::$app->formatter->asDate($this->prevs[2]->date, "php:Y")]),
            Yii::t("app", "{0} факт", [Yii::$app->formatter->asDate($this->prevs[2]->date, "php:Y")]),
        ];

        $table_income[] = $this->computeRow(Yii::t("app", "Доходы - всего, в том числе:"),  $settings['income_category_id']);
        $table_income[] = $this->computeRow(Yii::t("app", "Налоговые поступления:"), $settings['income_taxes']);
        $table_income[] = $this->computeRow(Yii::t("app", "Неналоговые поступления и поступления от продажи основного капитала:"), $settings['income_nontaxes']);
        $table_income[] = $this->computeRow(Yii::t("app", "Поступления трансфертов:"), $settings['income_transferts']);
        $table_income[] = $this->computeRow(Yii::t("app", "Погашение бюджетных кредитов:"), $settings['income_bud_cred']);
        $table_income[] = $this->computeRow(Yii::t("app", "Поступления займов:"), $settings['income_pog_zaim']);

        $table_outcome[] = [
            Yii::t("app", "Наименование"),
            Yii::t("app", "{0} факт", [Yii::$app->formatter->asDate($this->prevs[0]->date, "php:Y")]),
            Yii::t("app", "{0} факт", [Yii::$app->formatter->asDate($this->prevs[1]->date, "php:Y")]),
            Yii::t("app", "{0} план", [Yii::$app->formatter->asDate($this->prevs[2]->date, "php:Y")]),
            Yii::t("app", "{0} факт", [Yii::$app->formatter->asDate($this->prevs[2]->date, "php:Y")]),
        ];
        $table_outcome[] = $this->computeRow(Yii::t("app", "Расходы - всего, в том числе:"), $settings['outcome_category_id']);
        $table_outcome[] = $this->computeRow(Yii::t("app", "Государственные услуги общего характера:"), $settings['outcome_gos_obsh']);
        $table_outcome[] = $this->computeRow(Yii::t("app", "Оборона, общественный порядок и безопасность:"), $settings['outcome_oborona']);
        $table_outcome[] = $this->computeRow(Yii::t("app", "Социальная сфера:"), $settings['outcome_social']);
        $table_outcome[] = $this->computeRow(Yii::t("app", "Реальный сектор:"), $settings['outcome_real']);
        $table_outcome[] = $this->computeRow(Yii::t("app", "Трансферты:"), $settings['outcome_transf']);
        $table_outcome[] = $this->computeRow(Yii::t("app", "Погашение займов:"), $settings['outcome_zaim']);
        $table_outcome[] = $this->computeRow(Yii::t("app", "Прочие:"), $settings['outcome_other']);
        $table_outcome[] = $this->computeRow(Yii::t("app", "Бюджетные кредиты:"), $settings['outcome_credit']);
        $table_outcome[] = $this->computeRow(Yii::t("app", "Приобретение финансовых активов:"), $settings['outcome_fin_activ']);


        return [
            'income' => $table_income,
            'outcome' => $table_outcome,
            // 'date' => Yii::$app->formatter->asDate($report->date_fact, "php:d.m.Y")
        ];

    }

    public function computeRow($name, $category_id) {
        if(!is_array($category_id)) $category_id = [$category_id];
        $data = [0,0,0,0];

        foreach ($this->prevs as $key => $report) {
            foreach ($category_id as $_category_id) {
                $result = 0;
                if($report->id) {
                    $report_item = ReportItems::find()
                        ->where(['report_id'=>$report->id])
                        ->andWhere(['category_id' => $_category_id['id']])
                        ->one();
                    if($report_item) {
                        if($key < count($this->prevs) - 1) {
                            $data[$key] += $report_item->budget_execution * $this->coeficient;
                        } else {
                            $data[$key] += $report_item->plan_payments * $this->coeficient;
                            $data[$key + 1] += $report_item->budget_execution * $this->coeficient;
                        }
                    } else {
                        if($key < count($this->prevs) - 1) {
                            $data[$key] += 0;
                        } else {
                            $data[$key] += 0;
                            $data[$key + 1] += 0;
                        }
                    }
                } else {
                    if($key < count($this->prevs) - 1) {
                        $data[$key] = 0;
                    } else {
                        $data[$key] = 0;
                        $data[$key + 1] = 0;
                    }
                }
            }
        }
        array_unshift($data, $name);
        return $data;
    }

    static function getBasicCategories() {
        // index_cats
        return [
            "income_category_id" => [['id' => '','search' => 'ДОХОДЫ']],
            "outcome_category_id" => [['id' => '','search' => 'ЗАТРАТЫ']],

            "income_taxes" => [['id' => '','search' => 'ДОХОДЫ-1']],
            "income_nontaxes" => [['id' => '','search' => 'ДОХОДЫ-2'], ['id' => '','search' => 'ДОХОДЫ-3']],
            "income_transferts" => [['id' => '','search' => 'ДОХОДЫ-4']],
            "income_bud_cred" => [['id' => '','search' => 'ПОГАШЕНИЕ БЮДЖЕТНЫХ КРЕДИТОВ']],
            "income_pog_zaim" => [['id' => '','search' => 'ФИНАНСИРОВАНИЕ ДЕФИЦИТА (ИСПОЛЬЗОВАНИЕ ПРОФИЦИТА) БЮДЖЕТА-7']],
            "income_osn_kap" => [['id' => '','search' => 'ДОХОДЫ-3']],
            // "income_prod_activ" => ['id' => '','search' => 'ФИНАНСИРОВАНИЕ ДЕФИЦИТА (ИСПОЛЬЗОВАНИЕ ПРОФИЦИТА) БЮДЖЕТА-7'],

            "outcome_gos_obsh" => [['id' => '','search' => 'ЗАТРАТЫ-01']],
            "outcome_oborona" => [['id' => '','search' => 'ЗАТРАТЫ-02'], ['id' => '','search' => 'ЗАТРАТЫ-03']],
            "outcome_social" => [['id' => '','search' => 'ЗАТРАТЫ-04'], ['id' => '','search' => 'ЗАТРАТЫ-05'], ['id' => '','search' => 'ЗАТРАТЫ-06'], ['id' => '','search' => 'ЗАТРАТЫ-08'] ],
            "outcome_real" => [['id' => '','search' => 'ЗАТРАТЫ-07'], ['id' => '','search' => 'ЗАТРАТЫ-09'], ['id' => '','search' => 'ЗАТРАТЫ-10'], ['id' => '','search' => 'ЗАТРАТЫ-11'], ['id' => '','search' => 'ЗАТРАТЫ-12']  ],
            "outcome_transf" => [['id' => '','search' => 'ЗАТРАТЫ-15'] ],
            "outcome_zaim" => [['id' => '','search' => 'ФИНАНСИРОВАНИЕ ДЕФИЦИТА (ИСПОЛЬЗОВАНИЕ ПРОФИЦИТА) БЮДЖЕТА-16'], ['id' => '','search' => 'ЗАТРАТЫ-14'] ],
            "outcome_other" => [['id' => '','search' => 'ЗАТРАТЫ-13'] ],
            "outcome_credit" => [['id' => '','search' => 'БЮДЖЕТНЫЕ КРЕДИТЫ'] ],
            "outcome_fin_activ" => [['id' => '','search' => 'ПРИОБРЕТЕНИЕ ФИНАНСОВЫХ АКТИВОВ'] ],

            "transerts" => [['id' => '', 'search' => 'ЗАТРАТЫ-15']],
            "transerts_subv" => [['id' => '', 'search' => 'ЗАТРАТЫ-15-1-257-007']],
            "transerts_iz" => [['id' => '', 'search' => 'ЗАТРАТЫ-15-1-257-006']],
            "transerts_vozvr" => [['id' => '', 'search' => 'ЗАТРАТЫ-15-1-257-011']],
            "transerts_tsel" => [['id' => '', 'search' => 'ЗАТРАТЫ-15-1-257-024']],


        ];

    }

    static function setBasicCategories() {

        $settings = Settings::findOne(['name' => 'index_cats']);

        // Проверить есть ли они
        if(!$settings) $settings = new Settings();

        $__settings = self::getBasicCategories();
        $data = [];
        foreach ($__settings as $name => $_set) {
            foreach ($_set as $kkey => $item) {
                $cat = ReportItemCategories::find()->where(['fullname' => $item["search"]])->one();
                $data[$name][$kkey] = [
                    'id' => ($cat) ? $cat->id : NULL,
                    'search' => $item["search"]
                ];
            }
        }
        $settings->name = "index_cats";
        $settings->value = JSON_ENCODE($data);
        $settings->save();
    }

    public function getYears() {
        $_years = array_column(Yii::$app->db->createCommand("SELECT YEAR(date) as year FROM reports GROUP BY YEAR(date) ORDER BY YEAR(date) DESC")->queryAll(), 'year');
        $years[date('Y')] = date('Y');
        foreach ($_years as $value) {
            $years[$value] = $value;
        }
        return $years;
    }

    private function getEmptyReport($date) {
        $report = new Reports();
        $report->date = $date;
        $report->city_id = $this->city;
        return $report;
    }

    public function findModel() {
        if(!$this->report) {
            $this->report = Reports::find()
                ->where(['type_id' => 2])                                           // Полный отчет
                ->andWhere(['city_id'=> $this->city])                                 // Город
                ->andWhere(['YEAR(date_fact)' => $this->year])                        // Год
                ->andWhere(['MONTH(date)' => $this->month])                       // Месяц
                ->orderBy("date DESC")
                ->one();
            if(!$this->report) $this->report = $this->getEmptyReport($this->year . "-" . $this->month . "-01");


            $year = Yii::$app->formatter->asDate($this->report->date, 'php:Y');
            for($i = 2; $i > 0; $i--) {
                $search_year = $year - $i;
                $prev_report = Reports::find()
                    ->where(['type_id' => 2])                                           // Полный отчет
                    ->andWhere(['city_id'=> $this->city])                                 // Город
                    ->andWhere(['YEAR(date)' => $search_year])                        // Год
                    ->andWhere(['MONTH(date)' => $this->month])                       // Месяц
                    ->orderBy("date DESC")
                    ->one();
                if($prev_report) {
                    $this->prevs[] = $prev_report;
                } else {
                    $this->prevs[] = $this->getEmptyReport("{$search_year}-{$this->month}-01");
                }
            }
            $this->prevs[] = $this->report;
        }
        return $this->report;
    }

}
