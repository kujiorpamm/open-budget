<?php
namespace common\models\helpers;

use Yii;
use yii\base\Model;
use common\models\helpers\ParentFilter;
use common\models\Reports;
use common\models\ReportItems;
use common\models\ReportItemCategories;
use common\models\Settings;


class FilterTransferts extends ParentFilter {

    public function getPieChart() {
        $settings = Settings::findOne(['name' => 'index_cats']);
        $settings = JSON_DECODE($settings->value, true);

        $report = $this->findModel();

        $items = [
            ['id' => $settings['transerts_subv'][0]['id'], 'model' => []],
            ['id' => $settings['transerts_iz'][0]['id'], 'model' => []],
            ['id' => $settings['transerts_vozvr'][0]['id'], 'model' => []],
            ['id' => $settings['transerts_tsel'][0]['id'], 'model' => []],
        ];

        $result = $this->getPieChartData($items, $settings['transerts'][0]['id']);
        $income_categories = ReportItemCategories::find()->where(['id' => $settings['transerts'][0]['id']])->one();
        $income_subcategories = [];
        foreach ($items as $item) {
            $income_subcategories[] = ReportItemCategories::find()->where(['id' => $item['id']])->one();
        }


        return [
            'status' => true,
            'total_income' => Yii::t('app', 'ИТОГО ТРАНСФЕРТЫ: {0}', [$result['total_income']]),
            // 'type' => 'pie',
            'date' => Yii::t('app', 'По состоянию на {0}', [Yii::$app->formatter->asDate($report->date, 'php:d.m.Y')]),
            'data_pie' =>  $result['data'],
            'data_bars' => $this->getStackedBarChart($income_subcategories, true),
            'data_tables' => $this->getTableData($income_categories, $income_subcategories),
        ];
    }

    // public function getTableData() {
    //     $settings = Settings::findOne(['name' => 'index_cats']);
    //     $settings = JSON_DECODE($settings->value, true);
    //
    //     $report = $this->findModel();
    //     $income_categories = ReportItemCategories::find()->where(['id' => $settings['transerts'][0]['id']])->one();
    //     $income_subcategories = [];
    //
    //     $items = [
    //         ['id' => $settings['transerts_subv'][0]['id'], 'model' => []],
    //         ['id' => $settings['transerts_iz'][0]['id'], 'model' => []],
    //         ['id' => $settings['transerts_vozvr'][0]['id'], 'model' => []],
    //         ['id' => $settings['transerts_tsel'][0]['id'], 'model' => []],
    //     ];
    //
    //     foreach ($items as $item) {
    //         $income_subcategories[] = ReportItemCategories::find()->where(['id' => $item['id']])->one();
    //     }
    //
    //     $data = [];
    //     // 1я строка
    //     $data[0]['data'] = [ Yii::t('app', 'Показатель')];
    //     foreach ($this->prevs as $kkey => $prev_report) {
    //         $data[0]['data'][] = Yii::$app->formatter->asDate($prev_report->date, 'php:Y');
    //     }
    //     // 2я строка
    //     $data[1]['data'] = [Yii::t('app', 'факт.'), Yii::t('app', 'план.'), Yii::t('app', 'факт.'), Yii::t('app', 'план.'), Yii::t('app', 'факт.'),  Yii::t('app', 'план.'), Yii::t('app', '% исп.')];
    //
    //     $total_reports = count($this->prevs);
    //     foreach ($income_subcategories as $key => $rc) {
    //         // НАЛОГОВЫЕ ПОСТУПЛЕНИЯ
    //         $_data = [
    //             'header' => false,
    //             'data' => [$rc->name_ru]
    //         ];
    //         foreach ($this->prevs as $kkey => $prev_report) {
    //             $report_item = ReportItems::find()->where(['report_id' => $prev_report->id, 'category_id' => $rc->id])->one();
    //
    //             if($kkey == ($total_reports -1)) {
    //                 try {
    //                     $_data['data'][] = $report_item->budget_execution * $this->coeficient;
    //                     $_data['data'][] = $report_item->plan_payments * $this->coeficient;
    //                     $_data['data'][] = $report_item->budget_execution_percent;
    //                 } catch (\Exception $e) {
    //                     $_data['data'][] = 0;
    //                     $_data['data'][] = 0;
    //                     $_data['data'][] = 0;
    //                     // echo "<pre>"; var_dump($rc->name_ru, $rc->id); echo "</pre>"; exit;
    //                 }
    //             } else {
    //                 try {
    //                     $_data['data'][] = $report_item->budget_execution * $this->coeficient;
    //                     $_data['data'][] = $report_item->plan_payments * $this->coeficient;
    //                 } catch (\Exception $e) {
    //                     $_data['data'][] = 0;
    //                     $_data['data'][] = 0;
    //                 }
    //             }
    //         }
    //         $data[] = $_data;
    //
    //
    //         $subcategories = ReportItemCategories::find()->where(['parent_id' => $rc->id])->all();
    //         foreach ($subcategories as $kkey => $subcat) {
    //             $_data = [
    //                 'header' => false,
    //                 'data' => [$subcat->name_ru]
    //             ];
    //             foreach ($this->prevs as $kkey => $prev_report) {
    //                 $report_item = ReportItems::find()->where(['report_id' => $prev_report->id, 'category_id' => $subcat->id])->one();
    //                 if(!$report_item) {
    //                     if($kkey == ($total_reports -1)) {
    //                         $_data['data'][] = 0;
    //                         $_data['data'][] = 0;
    //                         $_data['data'][] = 0;
    //                     } else {
    //                         $_data['data'][] = 0;
    //                         $_data['data'][] = 0;
    //                     }
    //                 } else {
    //                     if($kkey == ($total_reports -1)) {
    //                         $_data['data'][] = $report_item->budget_execution * $this->coeficient;
    //                         $_data['data'][] = $report_item->plan_payments * $this->coeficient;
    //                         $_data['data'][] = $report_item->budget_execution_percent;
    //                     } else {
    //                         $_data['data'][] = $report_item->budget_execution * $this->coeficient;
    //                         $_data['data'][] = $report_item->plan_payments * $this->coeficient;
    //                     }
    //                 }
    //
    //             }
    //             $data[] = $_data;
    //         }
    //     }
    //     // ВСЕГО
    //     $_data = [
    //         'header' => true,
    //         'data' => [
    //             Yii::t('app', 'ВСЕГО')
    //         ]
    //     ];
    //     $total_reports = count($this->prevs);
    //     foreach ($this->prevs as $kkey => $prev_report) {
    //         $r = ReportItems::find()->where(['report_id' => $prev_report->id, 'category_id' => $income_categories->id])->one();
    //         // $_data['data'][] =
    //         if($kkey == ($total_reports -1)) {
    //             $_data['data'][] = $r->budget_execution * $this->coeficient;
    //             $_data['data'][] = $r->plan_payments * $this->coeficient;
    //             $_data['data'][] = $r->budget_execution_percent;
    //         } else {
    //             $_data['data'][] = $r->budget_execution * $this->coeficient;
    //             $_data['data'][] = $r->plan_payments * $this->coeficient;
    //         }
    //     }
    //
    //     $data[] = $_data;
    //
    //     return $data;
    //
    // }
    //
    // public function getStackedBarChart() {
    //
    //     $settings = Settings::findOne(['name' => 'index_cats']);
    //     $settings = JSON_DECODE($settings->value, true);
    //
    //     $report = $this->findModel();
    //     $income_categories = ReportItemCategories::find()->where(['id' => $settings['outcome_category_id'][0]['id']])->one();
    //     $income_subcategories = ReportItemCategories::find()->where(['parent_id' => $income_categories->id])->all();
    //     // echo "<pre>"; var_dump($income_categories); echo "</pre>"; exit;
    //     $data = [
    //         'data' => [],
    //         'groups' => [],
    //         'labels' => [],
    //     ];
    //
    //     // Всего 3 отчета в массива
    //     $total_reports = count($this->prevs);
    //
    //     $items = [
    //         ['id' => $settings['transerts_subv'][0]['id'], 'model' => []],
    //         ['id' => $settings['transerts_iz'][0]['id'], 'model' => []],
    //         ['id' => $settings['transerts_vozvr'][0]['id'], 'model' => []],
    //         ['id' => $settings['transerts_tsel'][0]['id'], 'model' => []],
    //     ];
    //     // Для каждого отчета
    //     foreach ($this->prevs as $key => $report) {
    //         // Для каждой метрики
    //         $_date = Yii::t('app', '{0} факт.', [Yii::$app->formatter->asDate($report->date, 'php:Y')]);
    //         $_data = [
    //             'date' => $_date
    //         ];
    //         if($key == count($this->prevs) - 1) {
    //             $_date2 = Yii::t('app', '{0} план.', [Yii::$app->formatter->asDate($report->date, 'php:Y')]);
    //             $_data2 = [
    //                 'date' => $_date2
    //             ];
    //         } else {
    //             unset($_data2, $_date2);
    //         }
    //         foreach ($items as $key => $item) {
    //             $category = ReportItemCategories::findOne(['id' => $item['id']]);
    //             if( !in_array($category->name_ru, $data['groups'])) $data['groups'][] = $category->name_ru;
    //             $ritem = ReportItems::find()->where(['report_id' => $report->id, 'category_id' => $category->id])->one();
    //             if($ritem) {
    //                 $_data[$category->name_ru] = number_format($ritem->budget_execution * $this->coeficient, 2, '.', '');
    //                 if(isset($_data2)) {
    //                     $_data2[$category->name_ru] = number_format($ritem->plan_payments * $this->coeficient, 2, '.', '');
    //                 }
    //             } else {
    //                 $_data[$category->name_ru] = 0;
    //                 if(isset($_data2)) {
    //                     $_data2[$category->name_ru] = 0;
    //                 }
    //             }
    //
    //         }
    //
    //         if(isset($_data2)) {
    //             $data['data'][] = $_data2;
    //             $data['labels'][] = $_date2;
    //         }
    //
    //         $data['data'][] = $_data;
    //         $data['labels'][] = $_date;
    //
    //
    //
    //     }
    //
    //     return $data;
    // }
    //
    //
    // public function findModel() {
    //     if(!$this->report) {
    //         $this->report = Reports::find()
    //             ->where(['type_id' => 2])                                           // Полный отчет
    //             ->andWhere(['city_id'=> $this->city])                                 // Город
    //             // ->andWhere(['YEAR(date_fact)' => date('Y')])                        // Год
    //             ->andWhere(['MONTH(date)' => $this->to])                       // Месяц
    //             ->orderBy("date DESC")
    //             ->one();
    //
    //
    //         $year = Yii::$app->formatter->asDate($this->report->date, 'php:Y');
    //         for($i = 2; $i > 0; $i--) {
    //             $search_year = $year - $i;
    //             $prev_report = Reports::find()
    //                 ->where(['type_id' => 2])                                           // Полный отчет
    //                 ->andWhere(['city_id'=> $this->city])                                 // Город
    //                 ->andWhere(['YEAR(date)' => $search_year])                        // Год
    //                 ->andWhere(['MONTH(date)' => $this->to])                       // Месяц
    //                 ->orderBy("date DESC")
    //                 ->one();
    //             if($prev_report) {
    //                 $this->prevs[] = $prev_report;
    //             }
    //         }
    //         $this->prevs[] = $this->report;
    //
    //     }
    //
    //     return $this->report;
    // }
    //
    // private function getFormatted($number) {
    //     return \common\components\Helper::formatNumber($number * $this->coeficient);
    // }
    // private function getMultiplied($item) {
    //     if($item) {
    //         return \common\components\Helper::formatNumber($item->budget_execution * $this->coeficient);
    //     } else {
    //         return 0;
    //     }
    // }

}
