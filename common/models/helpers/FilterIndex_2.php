<?php
namespace common\models\helpers;

use Yii;
use yii\base\Model;
use common\models\Reports;
use common\models\ReportItems;
use common\models\ReportItemCategories;
use common\models\Settings;


class FilterIndex extends Model {

    public $city;
    public $coeficient = 1;
    public $from;
    public $to;

    public function rules() {
        return [
            [['city', 'coeficient', 'from', 'to'], 'required']
        ];
    }

    public function getIndexChartData() {


        $settings = Settings::findOne(['name' => 'index_cats']);
        $settings = JSON_DECODE($settings->value, true);

        $report = Reports::find()
            ->where(['type_id' => 2])                                           // Полный отчет
            ->andWhere(['city_id'=> $this->city])                                 // Город
            ->andWhere(['YEAR(date)' => date('Y')])                        // Год
            // ->andWhere(['YEAR(date_fact)' => "2017"])                        // Год
            ->andWhere(['MONTH(date)' => $this->to])                       // Месяц
            ->one();
        if(!$report) {
            $report = Reports::find()
                ->where(['type_id' => 2])                                           // Полный отчет
                ->andWhere(['city_id'=> $this->city])                                 // Город
                ->andWhere(['MONTH(date)' => $this->to])                       // Месяц
                ->orderBy("date DESC")
                ->one();
        }

        if($report) {
            $income = ReportItems::find()
                ->where(['report_id'=>$report->id])
                ->andWhere(['category_id' => $settings['income_category_id'][0]['id']])
                ->one();
            $outcome = ReportItems::find()
                ->where(['report_id'=>$report->id])
                ->andWhere(['category_id' => $settings['outcome_category_id'][0]['id']])
                ->one();
            return [
                "report" => $report->filename,
                "income" => $income,
                "outcome" => $outcome,
                "date" => Yii::t("app", "По состоянию на {0} года", [Yii::$app->formatter->asDate($report->date, "php:d.m.Y")]),
                "coeficient" => $this->coeficient
            ];
        } else {
            return [
                "income" => [
                    "budget_corrected" => 0,
                    "budget_execution" => 0,
                    "budget_execution_percent" => 0
                ],
                "outcome" => [
                    "budget_corrected" => 0,
                    "budget_execution" => 0,
                    "budget_execution_percent" => 0
                ],
                "date" => Yii::t("app", "По состоянию на {0} года", [date("d.m.Y")])
            ];
        }
    }

    public function getTables() {

        $settings = Settings::findOne(['name' => 'index_cats']);
        $settings = JSON_DECODE($settings->value, true);
        // echo "<pre>"; var_dump($settings); echo "</pre>"; exit;
        // echo "<pre>"; var_dump($settings); echo "</pre>"; exit;
        // echo "<pre>"; var_dump($settings); echo "</pre>"; exit;

        $report = Reports::find()
            ->where(['type_id' => 2])                                           // Полный отчет
            ->andWhere(['city_id'=> $this->city])                                 // Город
            ->andWhere(['MONTH(date)' => $this->to])                       // Месяц
            ->orderBy("date DESC")
            ->one();
        if(!$report) return [
            'income' => [['Отчет не найден']],
            'outcome' => [['Отчет не найден']],
            'date' => date('d.m.Y'),
            'report1' => '',
            'report2' => '',
            'report3' => ''
        ];
        // найти отчеты за 2 предыдущих года
        $year = Yii::$app->formatter->asDate($report->date_fact, "php:Y");
        $reports = [];
        for($i = 2, $j = 0;  $i >= 1; $i--, $j++) {
            $reports[$j] = Reports::find()
                ->where(['type_id' => 2])                                           // Полный отчет
                ->andWhere(['city_id'=> $this->city])                                 // Город
                ->andWhere(['MONTH(date)' => $this->to])
                ->andWhere(['YEAR(date)' => $year - $i])
                ->andWhere(['!=', 'id', $report->id])                       // Месяц
                ->orderBy("date DESC")
                ->one();
        }
        $reports[2] = $report;

        // echo "<pre>"; var_dump($reports); echo "</pre>"; exit;

        // $report_prev = Reports::find()
        //     ->where(['type_id' => 2])                                           // Полный отчет
        //     ->andWhere(['city_id'=> $this->city])                                 // Город
        //     ->andWhere(['MONTH(date_fact)' => $this->to])
        //     ->andWhere(['!=', 'id', $report->id])                       // Месяц
        //     ->orderBy("date_fact DESC")
        //     ->one();
        //
        // if(!$report_prev) $report_prev = $report;

        $table_income = [];
        $table_outcome = [];

        // Доходы
        $table_income[] = [
            Yii::t("app", "Наименование"),
            Yii::t("app", "{0} факт", [Yii::$app->formatter->asDate($reports[0]->date_fact, "php:Y")]),
            Yii::t("app", "{0} факт", [Yii::$app->formatter->asDate($reports[1]->date_fact, "php:Y")]),
            Yii::t("app", "{0} план", [Yii::$app->formatter->asDate($reports[2]->date_fact, "php:Y")]),
            Yii::t("app", "{0} факт", [Yii::$app->formatter->asDate($reports[2]->date_fact, "php:Y")]),
        ];
        $table_income[] = $this->computeRow(Yii::t("app", "Доходы - всего, в том числе:"), $reports, $settings['income_category_id']);
        $table_income[] = $this->computeRow(Yii::t("app", "Налоговые поступления:"), $reports, $settings['income_taxes']);
        $table_income[] = $this->computeRow(Yii::t("app", "Неналоговые поступления и поступления от продажи основного капитала:"), $reports, $settings['income_nontaxes']);
        // $table_income[] = $this->computeRow(Yii::t("app", "Поступления от продажи основного капитала:"), $reports, $settings['income_osn_kap']);
        $table_income[] = $this->computeRow(Yii::t("app", "Поступления трансфертов:"), $reports, $settings['income_transferts']);
        $table_income[] = $this->computeRow(Yii::t("app", "Погашение бюджетных кредитов:"), $reports, $settings['income_bud_cred']);
        $table_income[] = $this->computeRow(Yii::t("app", "Поступления займов:"), $reports, $settings['income_pog_zaim']);
        // $table_income[] = $this->computeRow(Yii::t("app", "Поступления от продажи финансовых активов:"), $reports, $settings['income_prod_activ']);

        // Расходы
        $table_outcome[] = [
            Yii::t("app", "Наименование"),
            Yii::t("app", "{0} факт", [Yii::$app->formatter->asDate($reports[0]->date_fact, "php:Y")]),
            Yii::t("app", "{0} факт", [Yii::$app->formatter->asDate($reports[1]->date_fact, "php:Y")]),
            Yii::t("app", "{0} план", [Yii::$app->formatter->asDate($reports[2]->date_fact, "php:Y")]),
            Yii::t("app", "{0} факт", [Yii::$app->formatter->asDate($reports[2]->date_fact, "php:Y")]),
        ];
        $table_outcome[] = $this->computeRow(Yii::t("app", "Расходы - всего, в том числе:"), $reports, $settings['outcome_category_id']);
        $table_outcome[] = $this->computeRow(Yii::t("app", "Государственные услуги общего характера:"), $reports, $settings['outcome_gos_obsh']);
        $table_outcome[] = $this->computeRow(Yii::t("app", "Оборона, общественный порядок и безопасность:"), $reports, $settings['outcome_oborona']);
        $table_outcome[] = $this->computeRow(Yii::t("app", "Социальная сфера:"), $reports, $settings['outcome_social']);
        $table_outcome[] = $this->computeRow(Yii::t("app", "Реальный сектор:"), $reports, $settings['outcome_real']);
        $table_outcome[] = $this->computeRow(Yii::t("app", "Трансферты:"), $reports, $settings['outcome_transf']);
        $table_outcome[] = $this->computeRow(Yii::t("app", "Погашение займов:"), $reports, $settings['outcome_zaim']);
        $table_outcome[] = $this->computeRow(Yii::t("app", "Прочие:"), $reports, $settings['outcome_other']);
        $table_outcome[] = $this->computeRow(Yii::t("app", "Бюджетные кредиты:"), $reports, $settings['outcome_credit']);
        $table_outcome[] = $this->computeRow(Yii::t("app", "Приобретение финансовых активов:"), $reports, $settings['outcome_fin_activ']);

        return [
            'income' => $table_income,
            'outcome' => $table_outcome,
            'date' => Yii::$app->formatter->asDate($report->date_fact, "php:d.m.Y"),
            'report1' => $reports[0]->filename,
            'report2' => $reports[1]->filename,
            'report3' => $reports[2]->filename
        ];
    }

    private function computeRow($name, $reports, $category_id) {
        if(!is_array($category_id)) $category_id = [$category_id];

        try {
            $data = [$name, "0", "0", "0", "0"];

            foreach ($category_id as $_category_id) {
                $row1 = ReportItems::find()
                    ->where(['report_id'=>$reports[0]->id])
                    ->andWhere(['category_id' => $_category_id['id']])
                    ->one();
                $data[1] += $this->getValue($row1, "budget_execution");
                $row2 = ReportItems::find()
                    ->where(['report_id'=>$reports[1]->id])
                    ->andWhere(['category_id' => $_category_id['id']])
                    ->one();
                $data[2] += $this->getValue($row2, "budget_execution");
                $row3 = ReportItems::find()
                    ->where(['report_id'=>$reports[2]->id])
                    ->andWhere(['category_id' => $_category_id['id']])
                    ->one();
                $data[3] += $this->getValue($row3, "plan_payments");
                $data[4] += $this->getValue($row3, "budget_execution");
            }
        } catch (\Exception $e) {
            return [$name, "0", "0", "0", "0"];
        }




        // if(!$row1 || !$row2) {
        //     echo "<pre>"; var_dump($category_id); echo "</pre>"; exit;
        // }

        return $data;
    }

    private function getValue($obj, $param) {
        if(isset($obj->{$param})) {
            return $obj->{$param} * $this->coeficient;
        };
        return "0";
    }

    public function getResults() {
        return false;
        // Найти все отчеты по месяцу
        $reports = Reports::find()
            ->where(['type_id' => 2])                                           // Полный отчет
            ->andWhere(['city_id'=> $this->city])                                 // Город
            ->andWhere(['MONTH(date_fact)' => $this->to])                       // Месяц
            ->orderBy('date_fact')
            ->all();

        $report = Reports::find()
            ->where(['type_id' => 2])                                           // Полный отчет
            ->andWhere(['city_id'=> $this->city])                                 // Город
            ->andWhere(['YEAR(date_fact)' => date('Y')])                        // Год
            ->andWhere(['MONTH(date_fact)' => $this->to])                       // Месяц
            ->one();
        if($report) {
            $income = ReportItems::find()
                ->where(['report_id'=>$report->id])
                ->andWhere(['category_id' => Yii::$app->params['income_category_id']])
                ->one();
            $outcome = ReportItems::find()
                ->where(['report_id'=>$report->id])
                ->andWhere(['category_id' => Yii::$app->params['outcome_category_id']])
                ->one();
        }

        // ПОСТУПЛЕНИЯ
        $table_income = $this->getTable($reports, Yii::$app->params['incomes']);

        // РАСХОДЫ
        $table_outcome = $this->getTable($reports, Yii::$app->params['outcomes']);

        return [
            'income' => (isset($income)) ? \common\components\Helper::formatNumber($income->budget_execution * $this->coeficient) : 0,
            'outcome' => (isset($outcome)) ? \common\components\Helper::formatNumber($outcome->budget_execution * $this->coeficient) : 0,
            'income_table' => $table_income,
            'outcome_table' => $table_outcome
        ];

    }

    public function getTable($reports, $report_items) {
        $table = [
            ['Наименование']
        ];
        foreach ($reports as $_report) {
            $year = Yii::$app->formatter->asDate($_report->date_fact, 'php:Y');
            if($year == date('Y')) {
                $table[0][] = $year . ' план.';
                $table[0][] = $year . ' факт.';
            } else {
                $table[0][] = $year . ' факт.';
            }
        }
        foreach ($report_items as $name => $income_id) {
            $row = [$name];
            foreach ($reports as $_report) {
                $year = Yii::$app->formatter->asDate($_report->date_fact, 'php:Y');
                $item = ReportItems::find()
                    ->where(['report_id'=>$_report->id])
                    ->andWhere(['category_id' => $income_id])
                    ->one();
                if($year == date('Y')) {
                    if($item) {
                        $row[] = Yii::$app->formatter->asDecimal( \common\components\Helper::formatNumber($item->plan_payments * $this->coeficient));
                        $row[] = Yii::$app->formatter->asDecimal( \common\components\Helper::formatNumber($item->budget_execution * $this->coeficient));
                    } else {
                        $row[] = 'x';
                        $row[] = 'x';
                    }
                } else {
                    if($item) {
                        $row[] = Yii::$app->formatter->asDecimal( \common\components\Helper::formatNumber($item->budget_execution * $this->coeficient));
                    } else {
                        $row[] = 'x';
                    }
                }
            }

            $table[] = $row;
        }
        return $table;
    }

    static function getBasicCategories() {
        // index_cats
        return [
            "income_category_id" => [['id' => '','search' => 'ДОХОДЫ']],
            "outcome_category_id" => [['id' => '','search' => 'ЗАТРАТЫ']],

            "income_taxes" => [['id' => '','search' => 'ДОХОДЫ-1']],
            "income_nontaxes" => [['id' => '','search' => 'ДОХОДЫ-2'], ['id' => '','search' => 'ДОХОДЫ-3']],
            "income_transferts" => [['id' => '','search' => 'ДОХОДЫ-4']],
            "income_bud_cred" => [['id' => '','search' => 'ПОГАШЕНИЕ БЮДЖЕТНЫХ КРЕДИТОВ']],
            "income_pog_zaim" => [['id' => '','search' => 'ФИНАНСИРОВАНИЕ ДЕФИЦИТА (ИСПОЛЬЗОВАНИЕ ПРОФИЦИТА) БЮДЖЕТА-7']],
            "income_osn_kap" => [['id' => '','search' => 'ДОХОДЫ-3']],
            // "income_prod_activ" => ['id' => '','search' => 'ФИНАНСИРОВАНИЕ ДЕФИЦИТА (ИСПОЛЬЗОВАНИЕ ПРОФИЦИТА) БЮДЖЕТА-7'],

            "outcome_gos_obsh" => [['id' => '','search' => 'ЗАТРАТЫ-01']],
            "outcome_oborona" => [['id' => '','search' => 'ЗАТРАТЫ-02'], ['id' => '','search' => 'ЗАТРАТЫ-03']],
            "outcome_social" => [['id' => '','search' => 'ЗАТРАТЫ-04'], ['id' => '','search' => 'ЗАТРАТЫ-05'], ['id' => '','search' => 'ЗАТРАТЫ-06'], ['id' => '','search' => 'ЗАТРАТЫ-08'] ],
            "outcome_real" => [['id' => '','search' => 'ЗАТРАТЫ-07'], ['id' => '','search' => 'ЗАТРАТЫ-09'], ['id' => '','search' => 'ЗАТРАТЫ-10'], ['id' => '','search' => 'ЗАТРАТЫ-11'], ['id' => '','search' => 'ЗАТРАТЫ-12']  ],
            "outcome_transf" => [['id' => '','search' => 'ЗАТРАТЫ-15'] ],
            "outcome_zaim" => [['id' => '','search' => 'ФИНАНСИРОВАНИЕ ДЕФИЦИТА (ИСПОЛЬЗОВАНИЕ ПРОФИЦИТА) БЮДЖЕТА-16'], ['id' => '','search' => 'ЗАТРАТЫ-14'] ],
            "outcome_other" => [['id' => '','search' => 'ЗАТРАТЫ-13'] ],
            "outcome_credit" => [['id' => '','search' => 'БЮДЖЕТНЫЕ КРЕДИТЫ'] ],
            "outcome_fin_activ" => [['id' => '','search' => 'ПРИОБРЕТЕНИЕ ФИНАНСОВЫХ АКТИВОВ'] ],

            "transerts" => [['id' => '', 'search' => 'ЗАТРАТЫ-15']],
            "transerts_subv" => [['id' => '', 'search' => 'ЗАТРАТЫ-15-1-257-007']],
            "transerts_iz" => [['id' => '', 'search' => 'ЗАТРАТЫ-15-1-257-006']],
            "transerts_vozvr" => [['id' => '', 'search' => 'ЗАТРАТЫ-15-1-257-011']],
            "transerts_tsel" => [['id' => '', 'search' => 'ЗАТРАТЫ-15-1-257-024']],


        ];

    }

    static function setBasicCategories() {

        $settings = Settings::findOne(['name' => 'index_cats']);

        // Проверить есть ли они
        if(!$settings) $settings = new Settings();

        $__settings = self::getBasicCategories();
        $data = [];
        foreach ($__settings as $name => $_set) {
            foreach ($_set as $kkey => $item) {
                $cat = ReportItemCategories::find()->where(['fullname' => $item["search"]])->one();
                $data[$name][$kkey] = [
                    'id' => ($cat) ? $cat->id : NULL,
                    'search' => $item["search"]
                ];
            }
        }
        $settings->name = "index_cats";
        $settings->value = JSON_ENCODE($data);
        $settings->save();

    }

}
