<?php

namespace common\models;

use Yii;
use common\models\Cities;

/**
 * This is the model class for table "subreports".
 *
 * @property int $id
 * @property int $city_id
 * @property string $date
 * @property string $data
 */
class Subreports extends \yii\db\ActiveRecord
{

    public $file;

    public $cities;

    public static function tableName()
    {
        return 'subreports';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date', 'data'], 'required'],
            [['date'], 'safe'],
            [['data'], 'string'],
            [['date'], 'unique'],
            [['file'], 'file', 'skipOnEmpty' => false, 'extensions' => ['xls', 'xlsx'], 'on' => 'create'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'file' => 'Файл',
            'date' => 'Дата отчета',
            'data' => 'Данные',
        ];
    }

    public function processReport() {

        $_data = [];
        $cities = [
            'г. Актау' => NULL, 'г. Жанаозен' => NULL,
            'Бейнеуский район' => NULL, 'Каракиянский район' => NULL,
            'Мангистауский район' => NULL, 'Тупкараганский район' => NULL,
            'Мунайлинский район' => NULL
        ];

        // Сначала найти id городов для этих отчетов
        foreach ($cities as $key => $value) {
            $city = Cities::find()->where(['like', 'name_ru', str_replace('г. ', '', "$key")])->one();
            $cities[$key] = $city;
        }
        $this->cities = $cities;
        $data = \moonland\phpexcel\Excel::import($this->file->tempName,
            [
               'setFirstRecordAsKeys' => false,
               'setIndexSheetByName' => false
            ]
        );

        $current_city = NULL;
        foreach ($data as $key => $row) {
            // Сначала проверить, определен ли город
            //     Если да, то проверить текущая строка принадлежит ли главным городам
            //         Если да, то поменять город
            //         Если нет, то добавить его
            //     Если нет, то попытаться определить его в текущей строке, получилось?
            //         Нет - следующая строка
            //         Да - ок, добавить город

            if( $cities[$row['A']] ) {
                $current_city = $cities[$row['A']]->name_ru;
                $result = $this->getProcessedRow($row);
                if($result) $_data[$current_city][] = $result;
                continue;
            }

            if($current_city) {
                $result = $this->getProcessedRow($row);
                if($result) $_data[$current_city][] = $result;
            } else {
                continue;
            }

        }

        $this->data = JSON_ENCODE($_data);
        return true;
    }

    private function getProcessedRow($row) {
        $name = $this->cities[$row['A']] ? $row['A'] : $row['B'];
        if(!$name) return NULL;

        return [
            'name' => $name,

            'income_year' => $this->parse_number($row['C']),
            'income_month' => $this->parse_number($row['D']),
            'income_actual' => $this->parse_number($row['E']),
            'income_actual_percent' => $this->parse_number($row['F']),

            'sub_year' => $this->parse_number($row['G']),
            'sub_month' => $this->parse_number($row['H']),
            'sub_kasse' => $this->parse_number($row['I']),

            'outcome_year' => $this->parse_number($row['J']),
            'outcome_month' => $this->parse_number($row['K']),
            'outcome_actual' => $this->parse_number($row['L']),
            'outcome_actual_percent' => $this->parse_number($row['M']),
        ];
    }

    private function parse_number($number) {
        return floatval(str_replace(' ', '', str_replace(',', '', $number) ));
    }

}
