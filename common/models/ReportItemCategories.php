<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "report_item_categories".
 *
 * @property int $id
 * @property int $parent_id
 * @property string $code
 * @property string $name_ru
 * @property string $name_kz
 * @property string $fullname
 */
class ReportItemCategories extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'report_item_categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id'], 'integer'],
            [['name_ru', 'fullname'], 'required'],
            [['code'], 'string', 'max' => 16],
            [['name_ru', 'name_kz'], 'string', 'max' => 1024],
            [['fullname'], 'string', 'max' => 1024],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'parent_id' => Yii::t('app', 'Parent ID'),
            'code' => Yii::t('app', 'Code'),
            'name_ru' => Yii::t('app', 'Название (рус)'),
            'name_kz' => Yii::t('app', 'Название (каз)'),
            'fullname' => Yii::t('app', 'Fullname'),
        ];
    }

    public function getName() {
        $name = "";
        switch (Yii::$app->language) {
            case 'ru':
                $name =  $this->name_ru;
                break;
            case 'kk':
                $name =  $this->name_kz;
                if($name) break;
            default:
                $name = $this->name_ru;
                break;
        }
        return $name;
    }

    public function getParent() {
        return $this->hasOne(self::className(), ['id'=>'parent_id']);
    }

    public function getChilds() {
        return $this->hasMany(self::className(), ['parent_id'=>'id']);
    }

}
