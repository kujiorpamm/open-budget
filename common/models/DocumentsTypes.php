<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "documents_types".
 *
 * @property int $id
 * @property string $name_ru
 * @property string $name_kz
 *
 * @property Documents[] $documents
 */
class DocumentsTypes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'documents_types';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name_ru', 'name_kz'], 'required'],
            [['name_ru', 'name_kz'], 'string', 'max' => 512],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name_ru' => Yii::t('app', 'Наименование (рус)'),
            'name_kz' => Yii::t('app', 'Наименование (каз)'),
        ];
    }

    public function getName() {
        return $this->name_ru;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocuments()
    {
        return $this->hasMany(Documents::className(), ['type_id' => 'id']);
    }

    public function getDocumentsByLanguage()
    {
        return $this->hasMany(Documents::className(), ['type_id' => 'id'])->andOnCondition(['language'=>Yii::$app->language]);
    }
}
