<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "report_items".
 *
 * @property int $id
 * @property int $report_id
 * @property double $budget_approved
 * @property double $budget_refined
 * @property double $budget_corrected
 * @property double $plan_payments
 * @property double $plan_obligations
 * @property double $obligations_accepted
 * @property double $obligations_unpaid
 * @property double $budget_execution
 * @property double $budget_execution_percent
 * @property double $budget_execution_percent_total
 */
class ReportItems extends \yii\db\ActiveRecord
{

    public $name_ru;

    public static function tableName()
    {
        return 'report_items';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['report_id', 'category_id'], 'required'],
            [['report_id', 'category_id'], 'integer'],
            [['budget_approved', 'budget_refined', 'budget_corrected', 'plan_payments', 'plan_obligations', 'obligations_accepted', 'obligations_unpaid', 'budget_execution', 'budget_execution_percent', 'budget_execution_percent_total'], 'number'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'report_id' => Yii::t('app', 'Report ID'),
            'budget_approved' => Yii::t('app', 'Утвержденный бюджет на отчетный финансовый год'),
            'budget_refined' => Yii::t('app', 'Уточненный бюджет на отчетный финансовый год'),
            'budget_corrected' => Yii::t('app', 'Скорректированный бюджет на отчетный финансовый год'),
            'plan_payments' => Yii::t('app', 'Сводный планпоступлений по платежам'),
            'plan_obligations' => Yii::t('app', 'Сводный планпоступлений по обязательствам'),
            'obligations_accepted' => Yii::t('app', 'Принятые обязательства'),
            'obligations_unpaid' => Yii::t('app', 'Неоплаченные обязательства'),
            'budget_execution' => Yii::t('app', 'Исполнение поступлениий бюджета и/или оплаченных обязательств по бюджетным программам (подпрограммам)'),
            'budget_execution_percent' => Yii::t('app', 'Исп-е поступ-ий бюджета и/или оплач. обяз-в по бюдж. прогр. (подпрогр.)  к свод. плану  поступ-ий и финанс-ия  на отчет. период, % '),
            'budget_execution_percent_total' => Yii::t('app', 'Исп-е поступ-ий бюджета и/или оплач. обяз-ва по бюдж. прогр. (подпрогр.) к исполняемому бюджету, %'),
        ];
    }

    public function getCategory() {
        return $this->hasOne(ReportItemCategories::className(), ['id'=>'category_id']);
    }

    public static function nodetree($param=array()) {
        $refs = array();
        $list = array();

        $nodes = Yii::$app->db->createCommand('select * from report_item_categories')->queryAll();

        foreach ($nodes as $data) {
            $thisref = &$refs[ $data['id'] ];
            $thisref['parentid'] = $data['parent_id'];
            $thisref['text'] = $data['name_ru'];
            if ($data['parent_id'] == 0) {
                $list[ $data['id'] ] = &$thisref;
            } else {
                $refs[ $data['parent_id'] ]['children'][ $data['id'] ] = &$thisref;
            }
        }
        return $list;
    }

}
